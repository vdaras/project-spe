player = nil;
dead = false;
camera = nil;
guiContents = nil;
change = false;

font = nil

function Create()

    this.renderer:AddLayer( 'Map' );
    this.renderer:AddLayer( 'Entities' );
    this:LoadMap( 'topDown.tmx', 'Map' );
		
    camera = this.renderer:GetCamera( );
    
    player = core.FindEntity( 'player' );
    checkPoint = player:GetPosition();
	
	audio.
    audio.PlayMusic('a1.ogg');
end

function OnEntityMessage(sender,msg)

	if sender:GetID() == 'player' and msg == 'death' then
		dead = true;
	elseif msg == 'changeLVL' then
		change = true;
	end
	
end

function Update()
    
	if change then 
		this:UnLoadMap();
		this:LoadMap( 'map2.tmx', 'Map' );
		player = core.FindEntity( 'player' );
		change = false;
	end
	
	if not dead then
		camera:SetFocus( player:GetPositionX() - 500, player:GetPositionY() - 250 );
	else
		player = core.SpawnEntity('player','player',checkPoint:GetX(),checkPoint:GetY(),'Entities');
		dead = false;
	end
end

function OnKeyboardEvent(event)
	
	key = event:GetKey()

	if key == input.SDLK_F1 then
		this.audioMan:PauseAll();
	elseif key == input.SDLK_F2 then
		this.audioMan:PlayAll();
	elseif key == input.SDLK_F3 then
		audio.PlayMusic('arpa.ogg');
    end
end


function Cleanup()
   if font ~= nil then
      font:DecreaseReference( );
   end
end