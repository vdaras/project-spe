player = nil;
dead = false;
camera = nil;
guiContents = nil;
change = false;
font = nil


monsters = nil;
monsterCounter = 0;

function Create()

    this.renderer:AddLayer('Back');
    this.renderer:AddLayer('GUI');
	
	root = GuiContents();
	CreatePicture(root,"back",0,0,"/Resources/Images/menu.jpg");
	
    camera = this.renderer:GetCamera();

	monsters = 
	{
		core.SpawnEntity('mob1','ghost',100,50,'Entities'),
		core.SpawnEntity('mob2','ghost',75,150,'Entities'),
		core.SpawnEntity('mob3','ghost',75,250,'Entities'),
		core.SpawnEntity('mob4','ghost',100,350,'Entities')
	};
	
	monsterCounter = 4;
	
    --audio.PlayMusic('a1.ogg');
end

function OnEntityMessage(sender,msg)

	if sender:GetCategory() == 'ghost' and msg == 'death' then
		monsterCounter--;		
	end
	
end

function Update()
    
	if monsterCounter == 0 then
		--VICTORY
	end
	
end

function OnKeyboardEvent(event)
	
	key = event:GetKey()

	if key == input.SDLK_F1 then
		this.audioMan:PauseAll();
	elseif key == input.SDLK_F2 then
		this.audioMan:PlayAll();
	elseif key == input.SDLK_F3 then
		audio.PlayMusic('arpa.ogg');
    end
end


function Cleanup()
   if font ~= nil then
      font:DecreaseReference( );
   end
end