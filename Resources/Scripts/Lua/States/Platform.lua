player = nil;
dead = false;
camera = nil;
guiContents = nil;
change = false;

font = nil

rainSound = nil;

function Create()

	this.renderer:AddLayer('partB');
    this.renderer:AddLayer('Map');
    this.renderer:AddLayer('Entities');
    this:LoadMap( 'platform.tmx', 'Map' );
	
    camera = this.renderer:GetCamera( );
    
    player = core.FindEntity('player');
    checkPoint = player:GetPosition();
	

	CreateTreeLayers();
	CreateCloudLayers();
	
	core.SpawnParticles('rain',vect.Vector2F(0,0),vect.Vector2F(0,1),false,'Entities');
	
    audio.PlayMusic('rain.ogg');
end

function CreateTreeLayers()

	this.renderer:AddLayer('l1',1.2);
	this.renderer:AddLayer('l2',1.6);
	this.renderer:AddLayer('l3',1.7);
	this.renderer:AddLayer('l4',2.3);
	this.renderer:AddLayer('l5',2.6);

	core.AddDrawable('back/tree.png',100 ,300,'l1');
	core.AddDrawable('back/tree1.png',2500,210,'l2');
	core.AddDrawable('back/tree2.png',4950,320,'l3');
	core.AddDrawable('back/tree1.png',1500,240,'l4');
	core.AddDrawable('back/tree.png',5550,220,'l5');
	core.AddDrawable('back/tree2.png',8500,410,'l1');
	core.AddDrawable('back/tree.png',4840,220,'l1');
	core.AddDrawable('back/tree.png',2300,200,'l2');
	core.AddDrawable('back/tree1.png',1500,200,'l4');
	core.AddDrawable('back/tree.png',3000,200,'l3');
	core.AddDrawable('back/tree1.png',3400,200,'l1');
	core.AddDrawable('back/tree2.png',10500,400,'l2');
	core.AddDrawable('back/tree1.png',8000,200,'l2');
	core.AddDrawable('back/tree1.png',12000,200,'l2');
	core.AddDrawable('back/tree1.png',9000,200,'l2');
	core.AddDrawable('back/tree.png',20000,200,'l1');
	core.AddDrawable('back/tree2.png',14010,320,'l4');
	
	
	core.AddDrawable('back/bush3.png',100 ,900,'l1');
	core.AddDrawable('back/bush.png',450,850,'l2');
	core.AddDrawable('back/bush3.png',670,800,'l3');
	core.AddDrawable('back/bush.png',1100,850,'l4');
	core.AddDrawable('back/bush.png',1250,800,'l5');
	core.AddDrawable('back/bush3.png',1200,800,'l1');
	core.AddDrawable('back/bush.png',1250,800,'l2');
	core.AddDrawable('back/bush.png',1200,800,'l3');
	
	core.AddDrawable('back/bush3.png',1640,720,'l1');
	core.AddDrawable('back/bush.png',2200,700,'l2');
	core.AddDrawable('back/bush.png',2800,700,'l4');
	core.AddDrawable('back/bush2.png',2900,800,'l3');
	core.AddDrawable('back/bush.png',3400,800,'l1');
	core.AddDrawable('back/bush2.png',37500,750,'l2');
	core.AddDrawable('back/bush.png',3900,700,'l2');
	core.AddDrawable('back/bush3.png',3000,800,'l2');
	core.AddDrawable('back/bush.png',4400,790,'l2');
	core.AddDrawable('back/bush3.png',4100,760,'l1');
	core.AddDrawable('back/bush2.png',4900,810,'l4');
	
	core.AddDrawable('back/bush3.png',5700,800,'l2');
	core.AddDrawable('back/bush.png',5200,800,'l1');
	core.AddDrawable('back/bush2.png',5500,810,'l4');
	
	core.AddDrawable('back/bush.png',6800,800,'l1');
	core.AddDrawable('back/bush3.png',6200,800,'l3');
	core.AddDrawable('back/bush.png',6500,810,'l4');
	
	core.AddDrawable('back/bush.png',7600,800,'l1');
	core.AddDrawable('back/bush3.png',7200,800,'l3');
	core.AddDrawable('back/bush.png',7500,810,'l4');
	
	core.AddDrawable('back/bush.png',8600,800,'l1');
	core.AddDrawable('back/bush3.png',8200,800,'l3');
	core.AddDrawable('back/bush.png',8888,810,'l4');
	
end

function CreateCloudLayers()

	this.renderer:AddLayer('c1',0.1);
	this.renderer:AddLayer('c2',0.28);
	this.renderer:AddLayer('c3',0.37);
	this.renderer:AddLayer('c4',0.67);

	core.AddDrawable('back/cloud1.png',100 ,-40,'c4');
	core.AddDrawable('back/cloud1.png',600,-40,'c4');
	core.AddDrawable('back/cloud2.png',1250,-40,'c4');
	core.AddDrawable('back/cloud3.png',2500 ,-40,'c4');
	core.AddDrawable('back/cloud1.png',3100,-40,'c4');
	core.AddDrawable('back/cloud2.png',3850,-40,'c4');
	
	
	core.AddDrawable('back/cloud3.png',100 ,0,'c3');
	core.AddDrawable('back/cloud1.png',900,10,'c2');
	core.AddDrawable('back/cloud2.png',4950,20,'c3');
	
	core.AddDrawable('back/cloud1.png',1600 ,0,'c1');
	core.AddDrawable('back/cloud1.png',3000,0,'c2');
	core.AddDrawable('back/cloud2.png',2550,0,'c3');
	
	core.AddDrawable('back/cloud2.png',3400 ,18,'c1');
	core.AddDrawable('back/cloud3.png',4200,0,'c2');
	core.AddDrawable('back/cloud2.png',2250,11,'c3');
	
	core.AddDrawable('back/cloud3.png',4400 ,-98,'c1');
	core.AddDrawable('back/cloud1.png',1700,-47,'c1');
	core.AddDrawable('back/cloud3.png',2950,-10,'c3');
	
	core.AddDrawable('back/cloud3.png',1200 ,28,'c1');
	core.AddDrawable('back/cloud1.png',3300,25,'c2');
	core.AddDrawable('back/cloud2.png',2050,69,'c3');
	
		core.AddDrawable('back/cloud1.png',100 ,0,'c1');
	core.AddDrawable('back/cloud1.png',900,-10,'c2');
	core.AddDrawable('back/cloud2.png',4950,20,'c3');
	
	
		core.AddDrawable('back/cloud1.png',200 ,0,'c1');
	core.AddDrawable('back/cloud1.png',1222,10,'c2');
	core.AddDrawable('back/cloud2.png',2350,20,'c2');
	
	--core.AddDrawable('back/tree1.png',10000,200,'l5');
	
end

function OnEntityMessage(sender,msg)

	if sender:GetID() == 'player' and msg == 'changeLVL' then
		change = true;
	elseif sender:GetID() == 'player' and msg == 'death' then
		dead = true;
	end	
	
end

function Update()
    
	if change then 
		--core.PopStateStack();
		core.PushState('Resources/Scripts/Lua/States/TopDownState.lua');
	end
	
	if not dead then
		camera:SetFocus( player:GetPositionX() - 500, 400);
	else
		player = core.SpawnEntity('player','player',checkPoint:GetX(),checkPoint:GetY(),'Entities');
		dead = false;
	end
end

function OnKeyboardEvent(event)
	
	key = event:GetKey()

	if key == input.SDLK_F1 then
		this.audioMan:PauseAll();
	elseif key == input.SDLK_F2 then
		this.audioMan:PlayAll();
	elseif key == input.SDLK_F3 then
		audio.PlayMusic('arpa.ogg');
    end
end

function Cleanup()
   if font ~= nil then
      font:DecreaseReference( );
   end
end