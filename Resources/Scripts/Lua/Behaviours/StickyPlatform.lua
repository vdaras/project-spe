
--Main functions called directly from engine 

--Globals const (like) used by this script
velX = vect.Vector2F(200,0);
velY = vect.Vector2F(0,300);
--Attributes
velocity = vect.Vector2F(-150,0);

function Initialize()
	physics = this:GetPhysicsLogic();
end

function Update()
	--local v = physics:GetVelocity() + velocity;
	physics:SetVelocity(velocity);
end

function OnTileCollision(tile,normal)
	
	if tile == "Solid" and normal:GetY() == 0 then
		velocity =  velocity * -1;
	end
	
end 

function OnEntityCollision(entity,normal)

	if entity:GetCategory() == "Player" and normal:GetY() == 1 then
		local v = physics:GetVelocity() + velocity;
		entity:GetPhysicsLogic():SetVelocity(v);
	else
		velocity =  velocity * -1;
	end
	
end
