
--Main functions called directly from engine 

function OnTileCollision(tile,normal)

	if tile == 'Solid' then
		Explode(normal);
	end
end

function OnEntityCollision(entity,normal)
	
	if entity:GetCategory() == "Player" then
		entity:Kill();
		core.SpawnParticles("fire",entity:GetPosition(),normal * -1,true,"partB");
	else
		Explode(normal);
	end
end

function Explode(normal)
	this:Kill();
	core.SpawnParticles("explosion",this:GetPosition(),normal,true,"partB");
end