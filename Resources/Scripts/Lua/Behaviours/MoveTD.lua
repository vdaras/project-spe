
--Main functions called directly from engine 

--Globals const (like) used by this script
velX = vect.Vector2F(250,0);
velY = vect.Vector2F(0,250);
speedMod = 1;
jumpMod = 1;
moved= 0;
--Attributes
onJump = false;
upDir = false;
downDir = false;
side = 'L';
velocity = vect.Vector2F(0,0);

function Initialize()
	animation = this:GetAnimationLogic();
	physics = this:GetPhysicsLogic();
	PlayAnimation();
end

function Update()

	local v = physics:GetVelocity() + NormalizeVelocity();
	physics:SetVelocity( v );
	PlayAnimation();
	UpdateStateValues();
end

function OnTileCollision(tile,normal)
	
	if tile == "Death" then
		this:Kill();
	elseif tile == "Jump" then
		jumpMod = 1.5;
	elseif tile == "Stair" then
		onStair = true;
	end
	
end

function OnEntityCollision(entity,normal)

end

function OnKeyButtonDown(key,kmod,association)
	
	if association == "left" then
		velocity = velocity - velX;
		moved = moved +1;
	elseif association == "right" then
		velocity = velocity + velX;
		moved = moved +1;
	elseif association == "up" then
		velocity = velocity - velY;
		moved = moved +1;
	elseif association == "down" then
		velocity = velocity + velY;
		moved = moved +1;
	end

end

function OnKeyButtonUp(key,kmod,association)

	if association == "left" then
		velocity = velocity + velX;
		moved = moved -1;
	elseif association == "right" then
		velocity = velocity - velX;
		moved = moved -1;
	elseif association == "up" then
		velocity = velocity + velY;
		moved = moved -1;
	elseif association == "down" then
		velocity = velocity - velY;
		moved = moved -1;
	end
	
end

--Auxilary functions

function UpdateStateValues()

	speedMod = 1;
	
end

function NormalizeVelocity()
	return velocity;
end

function PlayAnimation()

	local v = physics:GetVelocity();
	
	if v:GetX() > 0 then
		side = 'R';
	elseif v:GetX() < 0 then
		side = 'L';
	elseif v:GetY() < 0 then
		side = 'U';
	elseif v:GetY() > 0 then
		side = 'D';
	end
	
	if moved == 0 then
		action = "Stand";
	else
		action = "Walk";
	end
	
	animation:Play(action..side,core.AnimationLogic_ANIM_LOOPING);

end
