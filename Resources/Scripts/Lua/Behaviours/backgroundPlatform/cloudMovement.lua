
--Main functions called directly from engine 
--Globals const (like) used by this script
velX = vect.Vector2F(math.random(-2,2),0);
velY = vect.Vector2F(0,150);
--Attributes
velocity = vect.Vector2F(-5,0);
timer = 0;
side = 'L';

function Initialize()

	--math.randomseed(os.time());

	animation = this:GetAnimationLogic();
	
	scale = math.random(0.2,1.5);
	animation:SetScale(vect.Vector2F(scale,scale));
	--animation:SetColor(color.Color(230,10,10,10));
	
	physics = this:GetPhysicsLogic();
	physics:SetVelocity(velX);
end

function Update()
	
end

function OnTileCollision(tile,normal)

end

function NewVelocity()

end

function OnEntityCollision(entity,normal)

end
