
--Main functions called directly from engine 
--Attributes
side = 'L';

function Initialize()
	animation = this:GetAnimationLogic();
	physics = this:GetPhysicsLogic();
	PlayAnimation();
end

function Update()
	PlayAnimation();
end

function OnEntityCollision(entity,normal)

end

--Auxilary functions

function PlayAnimation()

	local v = physics:GetVelocity();
	
	if v:GetX() > 0 then
		side = 'R';
		action = 'Walk';
	elseif v:GetX() < 0 then
		side = 'L';
		action = 'Walk';
	else
		action = 'Stand'
	end
	
	if v:GetY() > 0 then
		action = 'Fall';
	elseif v:GetY() < 0 then
		action = 'Jump';
	end
	
	animation:Play(action..side,core.AnimationLogic_ANIM_LOOPING);

end