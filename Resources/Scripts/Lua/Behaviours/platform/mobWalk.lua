
--Main functions called directly from engine 

--Globals const (like) used by this script
velX = math.Vector2F(200,0);
velY = math.Vector2F(0,300);
--Attributes
velocity = math.Vector2F(-150,0);

function Initialize()
	physics = this:GetPhysicsLogic();
end

function Update()
	local v = physics:GetVelocity() + velocity;
	physics:SetVelocity(v);
end

function OnTileCollision(tile,normal)
	
	if tile == "Solid" and normal:GetY() == 0 then
		velocity =  velocity * -1;
	end
	
end

