
--Main functions called directly from engine 

hasGreenKey = false;
hasRedKey = false;
hasBlueKey = false;

function Initialize()

end

function Update()

end

function OnTileCollision(tile,normal)

end

function OnEntityCollision(entity,normal)
	
	printing.Print(entity:GetCategory());
	printing.Print(entity:GetType());
	--printing.Print(entity:GetName());
	
	if entity:GetCategory() == "Collectible" then
		entity:Kill();
		core.SpawnParticles("fire",entity:GetPosition(),normal * -1,true,"parts");
		audio.PlaySound("cc.wav");
	elseif entity:GetType() == "fake" or entity:GetType() == "fake2"  then
		entity:Kill();
		core.SpawnParticles("rocks",entity:GetPosition(),vect.Vector2F(0,-1),true,"parts");
		audio.PlaySound("smash.wav");
	elseif entity:GetType() == "sign" then
		this:NotifyState("changeLVL");
	elseif entity:GetCategory() == "Key" then
	
		keyColor = entity:GetProperty("color"):AsString();		
		OnKeyAquire(keyColor);
		entity:Kill();
	elseif entity:GetCategory() == "lock" then
	
		keyColor = entity:GetProperty("color"):AsString();

		--if OnKeyUse(keyColor) then
			entity:Kill();
		--else 
		
		--end
	end
end


function OnKeyAquire(keyColor)
	if keyColor == "green" then
		hasGreenKey = true;
	elseif keyColor == "red" then
		hasRedKey = true;
	elseif keyColor == "blue" then
		hasBlueKey = true;
	end

end

function OnKeyUse(keyColor)
	if keyColor == "green" then
		return hasGreenKey;
	elseif keyColor == "red" then
		return hasRedKey;
	elseif keyColor == "blue" then
		return hasBlueKey;
	end
	return false;
end

