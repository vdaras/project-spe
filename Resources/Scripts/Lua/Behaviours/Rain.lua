
--Main functions called directly from engine 

activated = false;

function Initialize()

end

function Update()

end

function OnTileCollision(tile,normal)

end

function OnEntityCollision(entity,normal)
	
	if entity:GetCategory() == "Player" and not activated then
		core.SpawnParticles("rain",vect.Vector2F(0,0),normal * -1,false,"parts");
		activated = true;
	end
end
