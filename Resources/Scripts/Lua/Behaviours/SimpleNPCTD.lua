
--Main functions called directly from engine 
--Globals const (like) used by this script
velX = vect.Vector2F(150,0);
velY = vect.Vector2F(0,150);
--Attributes
velocity = vect.Vector2F(-150,0);
timer = 0;
side = 'L';

function Initialize()
	animation = this:GetAnimationLogic();
	physics = this:GetPhysicsLogic();
	PlayAnimation();
end

function Update()
	physics:SetVelocity(NewVelocity());
	PlayAnimation();
end

function OnTileCollision(tile,normal)
	
	if tile == "Solid" then
		velocity = velocity * -1;
	end
end

function NewVelocity()

	timer = timer +1;
	if timer > 170 then
		local x = math.random(3) -2;
		local y = math.random(3) -2;
		velocity = velX * x + velY * y;
		timer = 0;
	end
	
	return velocity;
	
end

function OnEntityCollision(entity,normal)
	if entity == "Player" then
	
	end
end

--Auxilary functions

function PlayAnimation()

	local v = physics:GetVelocity();
	
	if v:GetX() > 0 then
		side = 'R';
	elseif v:GetX() < 0 then
		side = 'L';
	elseif v:GetY() < 0 then
		side = 'U';
	elseif v:GetY() > 0 then
		side = 'D';
	end
	
	if moved == 0 then
		action = "Stand";
	else
		action = "Walk";
	end
	
	animation:Play(action..side,core.AnimationLogic_ANIM_LOOPING);

end