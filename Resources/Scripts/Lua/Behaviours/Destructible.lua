
--Main functions called directly from engine 


function OnEntityCollision(entity,normal)
	
	if entity:GetCategory() == "Player" then
		Explode(normal);
	end
end

function Explode(normal)
	this:Kill();
	core.SpawnParticles("rocks",this:GetPosition(),normal * -1,true,"parts");
	audio.PlaySound("smash.wav");
end