
--Main functions called directly from engine 

--Globals const (like) used by this script
velX = vect.Vector2F(200,0);
velY = vect.Vector2F(0,300);
speedMod = 1;
jumpMod = 1;
moved= 0;
--Attributes
onJump = false;
upDir = false;
downDir = false;
side = 'L';
velocity = vect.Vector2F(0,0);

function Initialize()
	animation = this:GetAnimationLogic();
	physics = this:GetPhysicsLogic();
	--PlayAnimation();
end

function Update()

	local v = physics:GetVelocity() + NormalizeVelocity();
	physics:SetVelocity(v );
	PlayAnimation();
end

function OnTileCollision(tile,normal)
	
	if tile == "Death" then
		this:Kill();
	elseif tile == "Jump" then
		jumpMod = 1.5;
	elseif tile == "Stair" then
		onStair = true;
	end
	
end

function OnEntityCollision(entity,normal)

end

function OnKeyButtonDown(key,kmod,association)
	
	if association == "left" then
		velocity = velocity - velX;
		moved = moved +1;
	elseif association == "right" then
		velocity = velocity + velX;
		moved = moved +1;
	elseif association == "jump" then
		onJump = true;
	elseif association == "up" then
		upDir = true;
	elseif association == "down" then
		downDir = true;
		this:NotifyState("down");
	end

end

function OnKeyButtonUp(key,kmod,association)

	if association == "left" then
		velocity = velocity + velX;
		moved = moved -1;
	elseif association == "right" then
		velocity = velocity - velX;
		moved = moved -1;
	elseif association == "up" then
		upDir = false;
	elseif association == "down" then
		downDir = false;
	end
	
end

--Auxilary functions

function UpdateStateValues()
	
	jumpMod = 1;
	speedMod = 1;
	onStair = false;

end

function NormalizeVelocity()

	local onSolid = physics:HasSolidContactOn(vect.Vector2F(0,1));
	
	--enable doubleJump in case of fall without a first jump
	if onSolid then 
		doubleJump = true;
	end
	
	if onJump and onSolid then --simple jump
		--consume jump event and enable doubleJump
		onJump = false;
		doubleJump = true;
		return velocity - velY*jumpMod;	
	elseif onJump and doubleJump and not onSolid then--double jump
		doubleJump = false
		--remove the Y velocity of the entity to make this a clear jump
		local vel = physics:GetVelocity();
		vel:SetY(0);
		physics:SetVelocity(vel);
		--spawn particles to indicate the double jump on air
		core.SpawnParticles("jSmoke",this:GetPosition() + vect.Vector2F(15,30),vect.Vector2F(0,1),true,"parts");
		--make the second jump less effective than the first one
		return velocity - velY*jumpMod*0.8;
	end
	
	if onStair then
		local vel = physics:GetVelocity();
		vel:SetY(0);
		physics:SetVelocity(vel);
		if upDir then
			return velocity - velY;
		elseif downDir then
			return velocity + velY;
		end
	end

	onJump = false;
	return velocity;
end

function PlayAnimation()

	local onSolid = physics:HasSolidContactOn(vect.Vector2F(0,1));
	local v = physics:GetVelocity();
	
	if v:GetX() > 0 and moved ~= 0 then
		side = 'R';
		action = 'Walk';
	elseif v:GetX() < 0 and moved ~= 0 then
		side = 'L';
		action = 'Walk';
	else
		action = 'Stand'
	end
	
	if not onSolid then
		if v:GetY() > 0 then
			action = 'Fall';
		elseif v:GetY() < 0 then
			action = 'Jump';
		end
	end
	
	animation:Play(action..side,core.AnimationLogic_ANIM_LOOPING);

end
