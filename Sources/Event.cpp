/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "Event.h"

namespace spe
{

Event::Event()
    :
    m_consumed(false),
    m_timeOccurred(SDL_GetTicks())
{
}


Event::~Event()
{
}


/**
 * Marks this event as consumed.
 */

void Event::Consume()
{
    m_consumed = true;
}


/**
 * Indicates if the event is consumed or not.
 *
 * @return true: if event is consumed
 *         false: otherwise
 */

bool Event::IsConsumed() const
{
    return m_consumed;
}


/**
 * Returns ticks of when the event occurred
 *
 * @return total ticks since engine start.
 */

Uint32 Event::GetTimeOccurred() const
{
    return m_timeOccurred;
}

}
