/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef ABSTRACTLOGIC_H
#define	ABSTRACTLOGIC_H

namespace spe
{

namespace EntitySystem
{

    class Entity;
    union GameEvent;

    /**
     * The abstract base class of the four main logic components of entities.
     * This difines the required interface.
     * note: that ComitEvent is abstract but has one default implementation.
     */
    class AbstractLogic
    {
    public:

        AbstractLogic() {}

        virtual ~AbstractLogic() {}
        inline virtual void SetParent(Entity* parent)
        {
            this->parent = parent;
        }

        inline virtual Entity* GetParent() const
        {
            return parent;
        }
    protected:
        Entity* parent;
    };

};

};

#endif	/* ABSTRACTLOGIC_H */

