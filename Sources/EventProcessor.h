/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef EVENT_GENERATOR_H
#define EVENT_GENERATOR_H


#include "KeyboardEvent.h"
#include "MouseEvent.h"
#include <list>

namespace spe
{

class EventProcessor
{
    std::list< Event* > m_eventGarbage;

public:

    EventProcessor();

    ~EventProcessor();


    /**
     * Notifies the Event Generator of a new event occurred.
     *
     * @param event: an SDL event.
     */

    Event* Process(const SDL_Event& event);


    /**
     * Disposes all produced events.
     */

    void DisposeEvents();

private:


    /**
     * Creates a mouse event based on an SDL_Event and enqueues it.
     */

    MouseEvent* ProcessMouseEvent(const SDL_Event& event);


    /**
     * Creates a keyboard event based on an SDL_Event and enqueues it.
     */

    KeyboardEvent* ProcessKeyboardEvent(const SDL_Event& event);
};

};

#endif