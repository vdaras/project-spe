/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "LogFileStream.h"
#include <ctime>
#include <sstream>
#include "Vector2.h"
#include "Rect.h"
#include "Circle.h"

namespace spe
{
	const char* htmlHead = "<head> <link rel=\"stylesheet\""" type=\"text/css\" href=\"log.css\" />  </head>\n"
		"<div class=\"links\">"
		"<a href=\"main.html\">Main</a> "
		"<a href=\"graphics.html\">Graphics</a> "
		"<a href=\"loading.html\">Loading</a> "
		"<a href=\"physics.html\">Physics</a> "
		"<a href=\"scripting.html\">Scripting</a> "
		"</div>";

	LogFileStream& LogFileStream::Log(int level /* = LEVEL_NOTE*/)
	{
		/*if( m_terminated )
		{
		//write time
		*this
		DeclareAsUnterminated( );
		}
		//return this stream to write external values*/
		return *this;
	}

	void LogFileStream::Initialize(const char* channelName, bool html)
	{
		if (html)
		{
			*this << htmlHead;
			*this << "<h1>" << channelName << "</h1>" << std::endl;
		}
		else
		{
			*this << channelName << std::endl;
		}
	}

	bool LogFileStream::IsTerminated()
	{
		return m_terminated;
	}

	void LogFileStream::DeclareAsTerminated()
	{
		m_terminated = true;
	}

	void LogFileStream::DeclareAsUnterminated()
	{
		m_terminated = false;
	}

	bool LogFileStream::IsChainWrite()
	{
		return m_chainWrite;
	}

	void LogFileStream::SetChanWrite(bool flag)
	{
		m_chainWrite = flag;
	}
}

std::ostream& Now(std::ostream& stream)
{
	//init time
	time_t rawtime;
	time(&rawtime);

	struct tm* timeinfo;
	timeinfo = localtime(&rawtime);

	//format time and write to file
	char timeStr[50];
	strftime(timeStr, 50, "%c", timeinfo);

	stream << timeStr;
	return stream;
}

std::ostream& NowTaged(std::ostream& stream)
{
	//init time
	time_t rawtime;
	time(&rawtime);

	struct tm* timeinfo;
	timeinfo = localtime(&rawtime);

	//format time and write to file
	char timeStr[50];
	strftime(timeStr, 50, "%c", timeinfo);

	stream << "<div class=\"date\">" << timeStr << "</div>";
	return stream;
}

std::ostream& CloseTag(std::ostream& stream)
{
	stream << "</div>";
	return stream;
}

//i could overload the << operator in order to obtain the data written to any channel written in CHANNEL_MAIN.(alla upotheto variosoun xD)

std::ostream& operator <<(std::ostream& stream, const spe::Math::Vector2I& vector)
{
	stream << "(" << vector.GetX() << "," << vector.GetY() << ")";
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const spe::Math::Vector2F& vector)
{
	stream << "(" << vector.GetX() << "," << vector.GetY() << ")";
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const spe::Math::Vector2D& vector)
{
	stream << "(" << vector.GetX() << "," << vector.GetY() << ")";
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const spe::Math::Circle& circle)
{
	stream << "( C: " << circle.GetCenter() << " ,R:" << circle.GetRadious() << ")";
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const spe::Math::Rect& rect)
{
	stream << "( T: " << rect.GetTop() << ", L:" << rect.GetLeft() << ", H:" << rect.GetHeight() << ", W:" << rect.GetWidth() << ")";
	return stream;
}

std::ostream& operator <<(std::ostream& stream, const std::list<std::string>& list)
{
	std::list<std::string>::const_iterator it = list.begin();
	std::list<std::string>::const_iterator end = list.end();

	for (; it != end; ++it)
	{
		stream << (*it) << "\n";
	}
	return stream;
}