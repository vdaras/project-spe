/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Camera.h"
#include "physics/EntityContactInfo.h"

namespace spe
{

Camera::Camera(int x, int y, int width, int height)
{
    //    m_previousFocus = Math::Vector2I(0, 0);

    //setup Rect
    m_clip.Set(y, x, height, width);

    m_xBorder = width;
    m_yBorder = height;
}

Camera::Camera(const Camera& c)
{
    //    m_previousFocus = Math::Vector2I(0, 0);
    m_clip = c.m_clip;

    m_xBorder = c.m_xBorder;
    m_yBorder = c.m_yBorder;
}

Camera::~Camera()
{
}

Math::Vector2F Camera::Translate(const Math::Vector2F& origin)
{
    return (origin - this->m_clip.GetOrigin().ConvertTo<float>());
}

/*
 * Focuses camera on x,y
 */

void Camera::SetFocus(int x, int y)
{
    //    m_previousFocus = m_clip.GetOrigin();
    m_clip.SetTop(y);
    m_clip.SetLeft(x);

    if(m_clip.GetLeft() < 0)
    {
        m_clip.SetLeft(0);
    }
    if(m_clip.GetTotalWidth() > m_xBorder)
    {
        m_clip.SetLeft(m_xBorder - m_clip.GetWidth());
    }
    if(m_clip.GetTop() < 0)
    {
        m_clip.SetTop(0);
    }
    if(m_clip.GetTotalHeight() > m_yBorder)
    {
        m_clip.SetTop(m_yBorder - m_clip.GetHeight());
    }
}

/*
 * Set's maximum camera edges. camera.x + camera.width must always be below maxX.
 * The same applies for maxY.
 */

void Camera::SetBorders(int maxX, int maxY)
{
    m_xBorder = maxX;
    m_yBorder = maxY;
}

/*
 * Moves camera
 */

void Camera::Move(int x, int y) //moves camera , if it hits a border , it retains it's position
{
    //    m_previousFocus = m_clip.GetOrigin();

    m_clip.SetTop(m_clip.GetTop() + y);
    m_clip.SetLeft(m_clip.GetLeft() + x);

    if(m_clip.GetLeft() < 0)
    {
        m_clip.SetLeft(0);
    }
    if(m_clip.GetTotalWidth() > m_xBorder)
    {
        m_clip.SetLeft(m_xBorder - m_clip.GetWidth());
    }
    if(m_clip.GetTop() < 0)
    {
        m_clip.SetTop(0);
    }
    if(m_clip.GetTotalHeight() > m_yBorder)
    {
        m_clip.SetTop(m_yBorder - m_clip.GetHeight());
    }
}

const Math::Vector2F Camera::GetFocus()
{
    return m_clip.GetOrigin().ConvertTo<float>();
}

const Math::Rect* Camera::GetRect() const
{
    return &m_clip;
}

}

