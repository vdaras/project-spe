/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef SERVICELOCATOR_H
#define	SERVICELOCATOR_H

namespace spe
{

template <class ServiceType>
class ServiceLocator
{
private:

    static ServiceType* m_service;

public:

    /**
     * Sets the object that implements the Service.
     * @param service
     */

    static void SetService(ServiceType* service)
    {
        m_service = service;
    }


    /**
     * Gets a handle to the service provider.
     *
     * @return
     */

    static ServiceType* GetService()
    {
        return m_service;
    }
};

template <class ServiceType>
ServiceType* ServiceLocator<ServiceType>::m_service = 0;

};

#endif	/* SERVICELOCATOR_H */

