/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <algorithm>

#include "Contact.h"
#include "EntityContactInfo.h"
#include "../Logger.h"
#include "TileContactInfo.h"

namespace spe
{
	namespace physicsSystem
	{
		namespace PhysicsInternals
		{
			Contact::Contact(EntitySystem::PhysicsLogic* first, EntitySystem::PhysicsLogic* second, const Math::Vector2F& info, int groupIndex) :m_infSecond(nullptr)
			{
				if (first->HasInfiniteMass())
				{
					std::swap(first, second);
				}

				m_group = groupIndex;
				this->m_first = first;
				this->m_second = second;

				if (!first->IsAwake() && second->IsAwake())
				{
					first->Awake();
				}

				if (first->IsAwake() && !second->IsAwake())
				{
					second->Awake();
				}

				m_ghost = first->IsGhost() || second->IsGhost();

				//calculate contact normal
				Math::Rect pollingRect1(*(first->GetRect()));
				pollingRect1.SetOrigin(first->GetPosition().ConvertTo<int>());

				Math::Rect pollingRect2(*(second->GetRect()));
				pollingRect2.SetOrigin(second->GetPosition().ConvertTo<int>());

				Math::Vector2I commonPoints[2];

				if (!Math::CommonPoints(pollingRect1, pollingRect2, commonPoints))
				{
					Math::CommonPoints(pollingRect2, pollingRect1, commonPoints);
				}

				m_contactNormal = (commonPoints[0] - commonPoints[1]).ConvertTo<float>();

				//flip members and get their absolute values
				m_contactNormal.Absolute();
				//and flip the members
				m_contactNormal.Flip();

				//nulify the lesser one
				if (m_contactNormal.GetX() > m_contactNormal.GetY())
				{
					m_contactNormal.Set(1.0f, 0.0f);
				}
				else if (m_contactNormal.GetX() < m_contactNormal.GetY())
				{
					m_contactNormal.Set(0.0f, 1.0f);
				}
				else
				{
					m_contactNormal.Set(0.5f, 0.5f);
				}

				Math::Vector2F pen = (info * m_contactNormal);
				m_penetration = pen.Length();

				Math::Vector2F dC = pollingRect1.GetCenter() - pollingRect2.GetCenter();

				dC *= m_contactNormal;

				dC.Normalize();

				m_contactNormal *= dC;

				m_restitution = first->GetMaterial().GetBounceModifier() * second->GetMaterial().GetBounceModifier();

#ifdef DEBUG_DRAW
				//RenderingManager::GetInstance()->RenderLine(pollingRect1.GetCenter(), pollingRect1.GetCenter() + m_contactNormal * 50, 0, 255, 0);
				//RenderingManager::GetInstance()->RenderLine(pollingRect1.GetCenter(), pollingRect1.GetCenter() + pen * 50, 255, 0, 0);
#endif
				AddEvents();
			}

			void Contact::AddEvents()
			{
				//contact information into contact list

				if (m_second->HasInfiniteMass())
				{
					m_first->AddEntityContact(new EntityContactInfo(m_second->GetParent(), m_contactNormal, m_first->GetVelocity() - m_second->GetVelocity()));
					m_second->AddEntityContact(new EntityContactInfo(m_first->GetParent(), m_contactNormal * -1, m_second->GetVelocity() - m_first->GetVelocity()));
					m_infSecond = m_second;
					m_second = nullptr;
				}
				else
				{
					m_first->AddEntityContact(new EntityContactInfo(m_second->GetParent(), m_contactNormal, m_first->GetVelocity() - m_second->GetVelocity()));
					m_second->AddEntityContact(new EntityContactInfo(m_first->GetParent(), m_contactNormal * -1, m_second->GetVelocity() - m_first->GetVelocity()));
				}
			}

			Contact::Contact(EntitySystem::PhysicsLogic* first, const Math::Vector2F& info, const Math::Vector2F& contactNormal_) : m_contactNormal(contactNormal_), m_infSecond(nullptr)
			{
				m_group = first->GetGroupIndex();
				this->m_first = first;
				this->m_second = nullptr;

				m_ghost = false;

				//    if( !first->IsAwake( ) )
				//    {
				//        first->Awake( );
				//    }

				Math::Vector2F pen = (info * m_contactNormal.GetAbsolute());
				m_penetration = pen.Length();

				m_restitution = first->GetMaterial().GetBounceModifier();

				first->AddTileContact(new TileContactInfo(m_contactNormal, 1));
			}

			void Contact::Resolve(float duration)
			{
				ResolveInterpenetration(duration);
				ResolveVelocity(duration);
			}

			float Contact::GetSeparatingVelocity()
			{
				if (m_ghost)
					return 0;

				Math::Vector2F relVel = m_first->GetVelocity();
				if (m_second)
					relVel -= m_second->GetVelocity();

				return relVel.DotProduct(m_contactNormal);
			}

			void Contact::ReportScripts()
			{
				if (m_infSecond)
				{
					m_second = m_infSecond;
				}

				if (!m_second)
				{
					return;
				}

				m_first->OnEntityCollision(m_second->GetParent(), m_contactNormal);
				m_second->OnEntityCollision(m_first->GetParent(), m_contactNormal * -1);
			}

			void Contact::ResolveInterpenetration(float duration)
			{
				//if (m_ghost)
				//{
				//	return;
				//}

				if (m_penetration <= 0)
				{
					//nullptrify movement
					m_movement[0] = Math::Vector2F::Zero;
					m_movement[1] = Math::Vector2F::Zero;
					return;
				}

				float totalImass = m_first->GetInvertedMass();
				if (m_second)
					totalImass += m_second->GetInvertedMass();

				if (totalImass <= 0)
				{
					//nullptrify movement
					m_movement[0] = Math::Vector2F::Zero;
					m_movement[1] = Math::Vector2F::Zero;
					return;
				}

				//calculate the percentage of movement in contact direction.
				Math::Vector2F movePerIMass = m_contactNormal * (m_penetration / totalImass);

				m_first->AdjustPositionPerImass(movePerIMass);
				m_movement[0] = movePerIMass * m_first->GetInvertedMass();

				if (m_second)
				{
					m_movement[1] = movePerIMass * -m_second->GetInvertedMass();
					m_second->AdjustPositionPerImass(movePerIMass * -1);
				}
				else
				{
					m_movement[1] = Math::Vector2F::Zero;
				}
			}

			void Contact::ResolveVelocity(float duration)
			{
				//if (m_ghost)
				//{
				//	return;
				//}

				float sepVelocity = GetSeparatingVelocity();
				if (sepVelocity >= 0)
				{
					return;
				}

				float newSepVel = -sepVelocity * m_restitution;

				Math::Vector2F accCausedVelocity = m_first->GetGeneralAcceleration();

				if (m_second)
					accCausedVelocity -= m_second->GetGeneralAcceleration();

				float accCausedSepVelocity = accCausedVelocity.DotProduct(m_contactNormal) * duration;

				// If we've got a closing velocity due to acceleration build-up,
				// remove it from the new separating velocity
				if (accCausedSepVelocity < 0)
				{
					newSepVel += m_restitution * accCausedSepVelocity;

					// Make sure we haven't removed more than was
					// there to remove.
					if (newSepVel < 0) newSepVel = 0;
				}

				float deltaVelocity;
				if (sepVelocity > -0.1)
					deltaVelocity = -sepVelocity;
				else
					deltaVelocity = newSepVel - sepVelocity;

				float totalImass = m_first->GetInvertedMass();
				if (m_second)
					totalImass += m_second->GetInvertedMass();

				if (totalImass <= 0)
					return;

				Math::Vector2F impulse = m_contactNormal * (deltaVelocity / totalImass);

				m_first->AdjustVelocityPerImass(impulse);

				if (m_second)
					m_second->AdjustVelocityPerImass(impulse * -1);

				//this is usefull for reducing the jiger effect
				m_restitution *= 0.5;
			}
		};
	};
};