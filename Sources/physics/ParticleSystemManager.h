/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PARTICLESYSTEMMANAGER_H
#define	PARTICLESYSTEMMANAGER_H

#include <string>

#include "../RenderingManager.h"
#include "Force.h"
#include "ParticleSystem.h"

namespace spe
{
	namespace physicsSystem
	{
		namespace particleSystem
		{
			/**
			 * This class stores all required data for particles and provides an interface to
			 * manipulate Particles Systems.
			 *
			 * PhysicsManager encapsulates this class and this should not be used alone.
			 */
			class ParticleSystemManager
			{
			public:
				ParticleSystemManager();
				~ParticleSystemManager();

				/**
				 * Loads requested particle system.
				 * @param name the name of the system. (filename excluding the .xml extension)
				 * @param active specify if the new system should be active or not.
				 * @return A pointer to the drawable interface of new system in order to register it
				 *         into the appropriate rendering layer.
				 */
				Drawable* LoadParticleSystem(const std::string& name, bool active = true);

				/**
				 * Returns true if specified particle system is loaded.
				 * @param name - the name of the system
				 * @return
				 */
				bool IsParticleSystemLoaded(const std::string& name);

				/**
				 * Removes the specified system from the manager.
				 * @param name The system name.
				 */
				void DeleteParticleSystem(const std::string& name);

				/**
				 * The specified system will stop render and update itself until it is activated again.
				 * @param name The system name.
				 */
				void HideParticleSystem(const std::string& name);

				/**
				* Activates the specified system making it updatable and visible.
				* @param name The system name.
				*/
				void ActivateParticleSystem(const std::string& name);

				/**
				 * Returns the specified particle system.
				 * @param name The system to return.
				 * @return The system.
				 */
				ParticleSystem* GetParticleSystem(const std::string& name);

				/**
				 * Removes all systems.
				 */
				void ClearParticleSystems();

				void SetParticleUpdateTime(int updateTime);
				int GetParticleUpdateTime() const;

			protected:
				/**
				 * The main method which invokes all per frame particle logic.
				 * @param deltaTime The time passed since last update.
				 */
				void UpdateParticles(unsigned long  deltaTime, LevelMap* map, const Math::Vector2F& cameraOffset);
				int particlesUpdateTime;
				typedef std::map<std::string, ParticleSystem*> ParticleSystemPool;
				ParticleSystemPool systems;
			};
		};
	};
};

#endif	/* PARTICLESYSTEMMANAGER_H */
