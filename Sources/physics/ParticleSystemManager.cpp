/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ParticleSystemManager.h"

#include "../parsers/ParticleSystemParser.h"
#include "../Globals.h"
#include "../RenderingManager.h"
#include "../ResourceManager.h"

namespace spe
{
	namespace physicsSystem
	{
		namespace particleSystem
		{
			ParticleSystemManager::ParticleSystemManager()
			{
			}

			ParticleSystemManager::~ParticleSystemManager()
			{
				this->ClearParticleSystems();
			}

			void ParticleSystemManager::UpdateParticles(unsigned long deltaTime, LevelMap* map, const Math::Vector2F& cameraOffset)
			{
				std::map<std::string, ParticleSystem*>::iterator it = systems.begin();
				const std::map<std::string, ParticleSystem*>::iterator end = systems.end();
				ParticleSystem* curSystem;
				for (; it != end; ++it)
				{
					curSystem = it->second;

					//if system is not hidden or inactive
					if (curSystem->IsActive())
					{
						//add new particles to the system's emitters
						curSystem->AddParticles();

						//update particles
						curSystem->UpdateParticles(deltaTime);

						//and if collisions are enabled calculate them.
						if (curSystem->IsMapCollisions())
						{
							curSystem->TileCollision(map, cameraOffset.x, cameraOffset.y);
						}
					}
				}
			}

			Drawable* ParticleSystemManager::LoadParticleSystem(const std::string &name, bool active)
			{
				ParticleSystem* system = new ParticleSystem;
				FileParser::ParticleSystemParser parser((Globals::GetInstance()->GetPath("Particles") + name + ".xml").c_str(), system);
				parser.ParseSystem();
				systems[name] = system;
				system->SetDisplay(ResourceManager::GetInstance()->GetTexture(system->GetResource()));
				system->SetActive(active);
				return system;
			}

			bool ParticleSystemManager::IsParticleSystemLoaded(const std::string &name)
			{
				return (systems.find(name) != systems.end());
			}

			void ParticleSystemManager::DeleteParticleSystem(const std::string &name)
			{
				ParticleSystem* system = systems[name];
				if (system != nullptr)
				{
					delete system;
				}
			}

			void ParticleSystemManager::HideParticleSystem(const std::string &name)
			{
				systems[name]->SetActive(false);
			}

			void ParticleSystemManager::ActivateParticleSystem(const std::string &name)
			{
				systems[name]->SetActive(true);
			}

			ParticleSystem* ParticleSystemManager::GetParticleSystem(const std::string &name)
			{
				return systems[name];
			}

			void ParticleSystemManager::ClearParticleSystems()
			{
				SafeRelease<std::string, ParticleSystem*>(systems);
			}

			void ParticleSystemManager::SetParticleUpdateTime(int updateTime)
			{
				this->particlesUpdateTime = updateTime;
			}

			int ParticleSystemManager::GetParticleUpdateTime() const
			{
				return particlesUpdateTime;
			}
		}
	}
}