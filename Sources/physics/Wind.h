/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef WIND_H
#define	WIND_H

#include "Force.h"

#include "../Vector2.h"

namespace spe
{
    namespace physicsSystem
    {
        namespace forces
        {

            class Wind : public TimedForce
            {
            public:

                Wind(float minIntensity, float maxIntesity, int minRad, int maxRad, int radStep = 1);
                virtual void Prepare(float seconds);
                virtual void Affect(PhysicalObject* object);
            private:
                void CalculateForce();
                Math::Vector2F force;
                float minIntensity;
                float maxIntesity;
                int minRad;
                int maxRad;
                int radStep;
                int angle;
                bool direction;
            };

        };
    };

};
#endif	/* WIND_H */

