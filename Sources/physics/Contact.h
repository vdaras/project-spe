/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef CONTACT_H
#define	CONTACT_H

#include "../Mathematics.h"
#include "../Utilities.h"
#include "../PhysicsLogic.h"
#include "../Entity.h"
#include "../Vector2.h"

namespace spe
{
	namespace physicsSystem
	{
	
		namespace PhysicsInternals
		{

			/*
			 * This class represents a contact between two physical objects or between
			 * an object and tile map.
			 *
			 * Note that contactNormal points from first towards the opposite direction of second.
			 *
			 * ex.
			 *        *----*   *----*
			 *  <-CN--| F  |   | S  |
			 *        *----*   *----*
			 */
			class Contact
			{
			public:

				/**
				 * Used when this Contact is shared between two physical objects
				 * @param first The first physical object.
				 * @param second The second physical object
				 * @param info The info vector calculated by collision detection step.
				 */
				Contact(EntitySystem::PhysicsLogic* first, EntitySystem::PhysicsLogic* second, const Math::Vector2F& info, int groupIndex);

				/**
				 * Used when this Contact is shared between a physical object and a tile.
				 * @param object
				 * @param info
				 * @param contactNormal
				 */
				Contact(EntitySystem::PhysicsLogic* object, const Math::Vector2F& info, const Math::Vector2F& contactNormal);

				~Contact()
				{
				}

				/**
				 * Tries to resolve this contact.
				 * @param duration
				 */
				void Resolve(float duration);

				/**
				 * Calculate separation velocity
				 * Separation velocity show the relative velocity/position between two object.
				 * If sepVelocity is above 0 it means that the two object are moving towards each other
				 * and their velocities should change.
				 * Otherwise the two object move away from each other and so only penetration must
				 * be resolved.
				 */
				float GetSeparatingVelocity();

				/**
				 * Call all appropriate script methods.
				 */
				void ReportScripts();

				/*
				 * Inlined setters and getters
				 */
				inline float GetPenetration()
				{
					return m_penetration;
				}

				inline void AdjustPenetration(float value)
				{
					m_penetration += value;
				}

				inline Math::Vector2F* const GetMovementArray()
				{
					return m_movement;
				}

				inline EntitySystem::PhysicsLogic* GetFirst()
				{
					return m_first;
				}

				inline EntitySystem::PhysicsLogic* GetSecond()
				{
					return m_second;
				}

				inline Math::Vector2F GetContactNormal()
				{
					return m_contactNormal;
				}

				inline int GetGroup() const
				{
					return m_group;
				}

			private:

				void AddEvents();

				/**
				 * Resolves any interpenetration between the two objects.
				 * @param duration
				 */
				void ResolveInterpenetration(float duration);

				/**
				 * Calculates and sets the new velocities of the two objects.
				 * @param duration
				 */
				void ResolveVelocity(float duration);

				/**
				 * The first object of the contact. From this starts the contactNormal.
				 */
				EntitySystem::PhysicsLogic* m_first;

				/**
				 * The second object. If this is a Contact with a tile second will be nullptr.
				 */
				EntitySystem::PhysicsLogic* m_second;

				/**
				 * If the second object has infinite mass then m_second will be nullptr.
				 * Thos is used to report the event corectly to the scripting system.
				 */
				EntitySystem::PhysicsLogic* m_infSecond;

				/**
				 * The combined bounce factor.
				 */
				float m_restitution;

				/**
				 * The amount the objects of this have moved.
				 */
				Math::Vector2F m_movement[2];

				/**
				 * The amount the two objects overlap each other on contactNormal.
				 */
				float m_penetration;

				/**
				 * A vector pointing towards where second pushes first.
				 */
				Math::Vector2F m_contactNormal;

				/**
				 * The ID of the group this contact belongs in.
				 */
				int m_group;


				bool m_ghost;
			};

		};
	};

};
#endif	/* CONTACT_H */

