/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ENTITYCONTACTINFO_H
#define	ENTITYCONTACTINFO_H

#include "../Vector2.h"

namespace spe
{
    namespace EntitySystem
    {
        class Entity;
    };

    namespace physicsSystem
    {
        namespace PhysicsInternals
        {

            class EntityContactInfo
            {
            public:
                EntityContactInfo(EntitySystem::Entity* collidedWith_
                                  , const Math::Vector2F& contactNormal_
                                  , const Math::Vector2F& relativeVelocity_);
            private:
                /**
                 * The other entity of the entity pair that collided with the owner of this.
                 */
                EntitySystem::Entity* collidedWith;

                /**
                 * The contact normal describing the contact from the owners perspective.
                 */
                Math::Vector2F contactNormal;

                /**
                 * The relative velocity between the two objects.
                 */
                Math::Vector2F relativeVelocity;

            public:

                void SetContactNormal(const Math::Vector2F& contactNormal);

                Math::Vector2F GetContactNormal();

                void SetCollidedWith(EntitySystem::Entity* collidedWith);

                EntitySystem::Entity* GetCollidedWith();

                Math::Vector2F GetRelativeVelocities() const;
            private:
                /**
                 * Like the normal geter of collidedWith property but used only by the scripts.
                 * @return
                 */
                EntitySystem::Entity& scGetCollidedWith() const;
            };

        };
    };

};

#endif	/* ENTITYCONTACTINFO_H */

