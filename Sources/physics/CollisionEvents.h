/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef COLLISIONEVENTS_H
#define	COLLISIONEVENTS_H

#include "../Entity.h"
#include "../Mathematics.h"
#include "../PhysicsLogic.h"
#include "../Utilities.h"
#include "../Vector2.h"
#include "Contact.h"

namespace spe
{

    namespace physicsSystem
    {

        namespace PhysicsInternals
        {

            enum Axis
            {
                AXIS_X,
                AXIS_Y,
                AXIS_INVALID
            };

            /**
             * A struct to represent a boundary entry.
             */
            struct RDCBoundaryEntry
            {
                /**
                 * The holding object.
                 */
                EntitySystem::PhysicsLogic* object;

                /**
                 * The position of the entry on the specified axis. (no need to hold the axis here)
                 */
                int posInAxis;

                /**
                 * Indicates if this entry is where the object starts or ends.
                 */
                bool open;
            };

            /**
             * A Comparator to use with std::sort.
             * Default () operator returns true if i should go before j.
             */
            struct ComparePhysics
            {
                bool operator()(EntitySystem::PhysicsLogic* i, EntitySystem::PhysicsLogic* j)
                {
                    return (i->GetParent()->GetPositionX() < j->GetParent()->GetPositionX());
                }
            };

            /**
             * A Comparator to use with std::sort.
             * Default () operator returns true if i should go before j.
             */
            struct CompareBoundaryEntries
            {
                bool operator()(const RDCBoundaryEntry& i,const RDCBoundaryEntry& j)
                {
                    return (i.posInAxis < j.posInAxis);
                }
            };

            struct CompareContacts
            {
                //returns true if the first argument is to be considered less than the second argument, and false otherwise.
                bool operator()(const Contact& a,const Contact& b)
                {
                    return (a.GetGroup() < b.GetGroup());
                }
            };


            struct TileColEvent
            {
                TileColEvent()
                {
                    tile = 0;
                }
                unsigned long tile;
                Math::Rect region;
            };

        };

    };

};
#endif	/* COLLISIONEVENTS_H */

