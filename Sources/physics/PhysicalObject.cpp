/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "PhysicalObject.h"

namespace spe
{

namespace physicsSystem
{

PhysicalObject::PhysicalObject(float velocityX, float velocityY, float accelarationX, float accelarationY) :
velocity(velocityX, velocityY), generalAcceleration(accelarationX, accelarationY)
{
}

PhysicalObject::PhysicalObject(const PhysicalObject& org) : velocity(org.velocity), generalAcceleration(org.generalAcceleration), forces(org.forces)
{
    damping = org.damping;
    ghost = org.ghost;
    invertedMass = org.invertedMass;
}

void PhysicalObject::SetForces(Math::Vector2F forces)
{
    this->forces = forces;
}

Math::Vector2F PhysicalObject::GetForces() const
{
    return forces;
}

void PhysicalObject::SetGhost(bool ghost)
{
    this->ghost = ghost;
}

bool PhysicalObject::IsGhost() const
{
    return ghost;
}

PhysicalObject::PhysicalObject(const Math::Vector2F& velocity_, const Math::Vector2F& acceleration_) :
velocity(velocity_), generalAcceleration(acceleration_)
{

}

void PhysicalObject::ResetForces()
{
    forces.Set( 0.0f, 0.0f );
}

void PhysicalObject::AddForce(const Math::Vector2F& forceToAdd)
{
    forces += forceToAdd;
}

void PhysicalObject::SetDamping(float damping)
{
    this->damping = damping;
}

float PhysicalObject::GetDamping() const
{
    return damping;
}

void PhysicalObject::SetInvertedMass(float invertedMass)
{
    this->invertedMass = invertedMass;
}

float PhysicalObject::GetInvertedMass() const
{
    return invertedMass;
}

void PhysicalObject::SetMass(float mass)
{
    if( mass <= 0.00001 )
        invertedMass = 0;
    else
        invertedMass = 1 / mass;
}

float PhysicalObject::GetMass() const
{
    return 1 / invertedMass;
}

bool PhysicalObject::HasInfiniteMass()
{
    return invertedMass <= 0.00001;
}

void PhysicalObject::SetGeneralAcceleration(const Math::Vector2F& acceleration)
{
    this->generalAcceleration = acceleration;
}

Math::Vector2F PhysicalObject::GetGeneralAcceleration() const
{
    return generalAcceleration;
}

void PhysicalObject::SetVelocity(const Math::Vector2F& velocity)
{
    this->velocity = velocity;
}

};

};
