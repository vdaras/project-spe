/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Wind.h"

#include "../RandLib.h"
#include "../TimedUpdater.h"

namespace spe
{

    namespace physicsSystem
    {

        namespace forces
        {

            Wind::Wind(float minIntensity, float maxIntesity, int minRad, int maxRad, int radStep/*=1*/)
            {
                direction = true;
                this->minIntensity = minIntensity;
                this->maxIntesity = maxIntesity;
                this->maxRad = maxRad;
                this->minRad = minRad;
                this->radStep = radStep;
                angle = 90;
            }

            void Wind::Prepare(float seconds)
            {
                CalculateForce();
            }

            void Wind::Affect(PhysicalObject* object)
            {

            }

            void Wind::CalculateForce()
            {
                float intensity = Math::Random(minIntensity, maxIntesity);
                static TimedUpdater tU;
                if(tU.Update(10))
                    direction = Math::EventCheck(50);

                if(direction)
                {
                    angle += radStep;
                    if(angle > maxRad)
                    {
                        angle = maxRad;
                    }
                }
                else
                {
                    angle -= radStep;
                    if(angle < minRad)
                    {
                        angle = minRad;
                    }
                }

                force.Set(1, 0);
                force.RotateVector(angle);
                force *= intensity;
            }

        };

    };

};
