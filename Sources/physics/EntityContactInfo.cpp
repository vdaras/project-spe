/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#include "EntityContactInfo.h"

namespace spe
{
    namespace physicsSystem
    {
        namespace PhysicsInternals
        {

            EntityContactInfo::EntityContactInfo(EntitySystem::Entity* collidedWith_
                                                 , const Math::Vector2F& contactNormal_
                                                 , const Math::Vector2F& relativeVelocity_)
                : collidedWith(collidedWith_), contactNormal(contactNormal_)
                ,relativeVelocity(relativeVelocity_)
            {
            }

            void EntityContactInfo::SetContactNormal(const Math::Vector2F& contactNormal)
            {
                this->contactNormal = contactNormal;
            }

            Math::Vector2F EntityContactInfo::GetContactNormal()
            {
                return contactNormal;
            }

            void EntityContactInfo::SetCollidedWith(EntitySystem::Entity* collidedWith)
            {
                this->collidedWith = collidedWith;
            }

            EntitySystem::Entity* EntityContactInfo::GetCollidedWith()
            {
                return collidedWith;
            }

            EntitySystem::Entity& EntityContactInfo::scGetCollidedWith() const
            {
                return *collidedWith;
            }

            Math::Vector2F EntityContactInfo::GetRelativeVelocities() const
            {
                return relativeVelocity;
            }

        };
    };

};
