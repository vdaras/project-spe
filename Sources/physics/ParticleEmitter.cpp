/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ParticleEmitter.h"

#include <list>

#include "../sdl/GraphicsCore.h"
#include "../Utilities.h"
#include "../LevelMap.h"
#include "Particle.h"

using namespace spe::Math;

namespace spe
{
	namespace physicsSystem
	{
		namespace particleSystem
		{
			ParticleEmiter::ParticleEmiter(ParticleEmiterPrototype* prot, const Math::Vector2F& pos, const Math::Vector2F& dir, int id) : position(pos + prot->defaultRelPosition)
				, direction(dir)
			{
				alive = true;
				active = true;
				currentPart = 0;
				this->id = id;
				prototype = prot;
				recycler = false;
				current = particles.end();
				boundingShape = nullptr;
			}

			ParticleEmiter::~ParticleEmiter()
			{
				SafeRelease<Particle*>(particles);
			}

			void ParticleEmiter::AddParticles(int threashold)
			{
				if (currentPart + threashold < prototype->maxParts)
				{
					for (int i = 0; i < threashold; i++)
					{
						Particle* part = new Particle;
						InitializeParticle(part);
						particles.push_back(part);
					}
					currentPart += threashold;
					return;
				}
				else if (currentPart < prototype->maxParts)
				{
					//more particles fit in but they
					//are less than creationThreashold

					for (int i = 0; i < prototype->maxParts - currentPart; i++)
					{
						Particle* part = new Particle;
						InitializeParticle(part);
						particles.push_back(part);
					}
					currentPart = prototype->maxParts;
					return;
				}
			}

			void ParticleEmiter::AddParticlesViaRecycle(int threashold)
			{
				if (current == particles.end())
				{
					current = particles.begin();
				}

				if (currentPart + threashold < prototype->maxParts)
				{
					for (int i = 0; i < threashold && current != particles.end(); i++, ++current)
					{
						Particle* part = *current;
						InitializeParticle(part);
					}
					currentPart += threashold;
					return;
				}
				else if (currentPart < prototype->maxParts)  //more particles fit in but they are less than creationThreashold
				{
					for (int i = 0; i < (prototype->maxParts - currentPart) && current != particles.end(); i++, ++current)
					{
						Particle* part = *current;
						InitializeParticle(part);
					}
					currentPart = prototype->maxParts;
					return;
				}
			}

			void ParticleEmiter::Render(sdl::GraphicsCore* gCore, const gl::Texture& rsc, const Math::Rect& clip, const Math::Rect& camera, gl::BlendingMethodName blending)
			{
				bool flip[2] = { false, false };
				if (!active)  //if emitter is deactivated extra measurements must be taken for dead particles
				{
					for (auto& particle : particles)
					{
						if (!particle->IsAlive(prototype->lifetime)) continue;       //if dead do not blit it
						//if (camera.Intersect((*it)->GetPosition()))//if it is inside camera blit it.
						//rsc->Blit(pos.GetX() - camera.GetLeft(), pos.GetY() - camera.GetTop(), target);

						Math::Vector2F pos(particle->GetPosition().GetX() - camera.GetLeft(), particle->GetPosition().GetY() - camera.GetTop());
						gCore->Render(rsc, clip, particle->GetColor(), pos, particle->GetScale(), particle->GetAngle(), flip, blending);
					}
				}
				else
				{
					for (auto& particle : particles)
					{
						Math::Vector2F pos(particle->GetPosition().GetX() - camera.GetLeft(), particle->GetPosition().GetY() - camera.GetTop());
						gCore->Render(rsc, clip, particle->GetColor(), pos, particle->GetScale(), particle->GetAngle(), flip, blending);
					}
				}
			}

			void ParticleEmiter::RenderWrapped(sdl::GraphicsCore* gCore, const gl::Texture& rsc, const Math::Rect& clip, const Math::Rect& camera, gl::BlendingMethodName blending)
			{
				bool flip[2] = { false, false };

				int left = camera.GetWidth() == 0 ? 1 : camera.GetWidth();
				int top = camera.GetHeight() == 0 ? 1 : camera.GetHeight();

				if (!active)  //if emitter is deactivated extra measurements must be taken for dead particles
				{
					for (auto& particle : particles)
					{
						if (!particle->IsAlive(prototype->lifetime)) continue;       //if dead do not blit it
						Math::Vector2F pos((int)particle->GetPosition().GetX() % left, (int)particle->GetPosition().GetY() % top);
						gCore->Render(rsc, clip, particle->GetColor(), pos, particle->GetScale(), particle->GetAngle(), flip, blending);
					}
				}
				else
				{
					for (auto& particle : particles)
					{
						Math::Vector2F pos((int)particle->GetPosition().GetX() % left, (int)particle->GetPosition().GetY() % top);
						gCore->Render(rsc, clip, particle->GetColor(), pos, particle->GetScale(), particle->GetAngle(), flip, blending);
					}
				}
			}

			void ParticleEmiter::ApplyForce(forces::Force* force)
			{
				if (prototype->ignoreForces)
				{
					return;
				}

				for (auto& particle : particles)
				{
					force->Affect(particle);
				}
			}

			bool ParticleEmiter::Update(float seconds, Rect* clip)
			{
				/*
				 * if deactivated instead of recycling dead particles we count how many of them
				 * are dead. If all particles are dead then its time this emitter let it go
				 * and kill itself and return false.
				 */

				// determine the method by which the alive check will be made.
				AbstractAliveCheck* checker = nullptr;

				if (boundingShape != nullptr)
				{
					//calibrate shapes position
					boundingShape->SetCenter(position);
					//check if timed updates is required also
					if (prototype->updateStyle)
					{
						checker = new TimedBoundingShapeAliveCheck(prototype->lifetime, boundingShape);
					}
					else
					{
						checker = new BoundingShapeAliveCheck(boundingShape);
					}
				}
				else
				{
					checker = new TimedAliveCheck(prototype->lifetime);
				}

				if (!active)
				{
					int toRecycle = 0;

					for (auto& particle : particles)
					{
						if (!(*checker)(particle))//if dead
						{
							toRecycle++;
						}
						else
						{
							particle->Update(seconds);
							particle->UpdateColor(prototype->birthColor, prototype->deathColor, prototype->lifetime);
							particle->UpdateScale(prototype->birthScale, prototype->deathScale, prototype->lifetime);
						}
					}

					//if all particles are dead kill emitter
					alive = toRecycle < prototype->maxParts;
				}
				else
				{
					for (auto& particle : particles)
					{
						if (!(*checker)(particle))//if dead time to recycle
						{
							particle->SetCurrentLife(0);
							this->InitializeParticle(particle);
						}
						else
						{
							particle->Update(seconds);
							particle->UpdateColor(prototype->birthColor, prototype->deathColor, prototype->lifetime);
							particle->UpdateScale(prototype->birthScale, prototype->deathScale, prototype->lifetime);
						}
					}
				}

				delete checker;
				return alive;
			}

			void ParticleEmiter::TileCollisionCheck(const LevelMap* map, float mapOffsetX, float mapOffsetY)
			{
				TimedAliveCheck checker(prototype->lifetime);

				int height = map->GetHeight() * map->GetTileHeight();
				int width = map->GetWidth() * map->GetTileWidth();

				int x,y;

				for (auto& particle : particles)
				{
					if (checker(particle))//if particle is alive
					{
						x = particle->GetPosition().GetX() + mapOffsetX;
						y = particle->GetPosition().GetY() + mapOffsetY;

						if (x < width && y < height && y > 0 && x > 0)
						{
							if (map->GetTilelistPropsFromRPos(x + 7, y + 7) & 1)
							{
								//if this emitter is active recycle particle
								if (this->active)
								{
									particle->SetCurrentLife(0);
									InitializeParticle(particle);
								}
								else//kill the particle
								{
									//HACK the easiest way to kill a particle is
									//by setting a very big number as lifetime
									particle->SetCurrentLife(99999);
								}
							}
						}
					}
				}
			}

			void ParticleEmiter::InitializeParticle(Particle* part)
			{
				part->SetMass(Random(prototype->minMass, prototype->maxMass));
				part->SetColor(prototype->birthColor);
				part->SetScale(prototype->birthScale);
				part->SetScaleVariation(Random(0.0f, prototype->scaleVariation));
				/*part->SetAcceleration(0, 0);*/

				{
					//Initialize force (set initial acceleration based on mass)
					float forceMagnitude = Random(prototype->minInitialForceMagnitude, prototype->maxInitialForceMagnitude);
					int forceDirection = Random(prototype->minInitialForceAngle, prototype->maxInitialForceAngle);
					if (Math::EventCheck(50))
					{
						//forceDirection* -1;
					}

					part->AddForce(direction.GetRotatedVector(forceDirection) * forceMagnitude);
				}

				{
					//Initialize Velocity
					float velocityMagnitude = Random(prototype->minInitialVelocityMagnitude, prototype->maxInitialVelocityMagnitude);
					int velocityDirection = Random(prototype->minInitialVelocityAngle, prototype->maxInitialVelocityAngle);
					if (Math::EventCheck(50))
					{
						//velocityDirection* -1;
					}
					part->SetVelocity(direction.GetRotatedVector(velocityDirection) * velocityMagnitude);
				}

				{
					//Initialize position
					float x = Random(position.GetX() - prototype->spawnRadius.GetX(), position.GetX() + prototype->spawnRadius.GetX());
					float y = Random(position.GetY() - prototype->spawnRadius.GetY(), position.GetY() + prototype->spawnRadius.GetY());
					part->SetPosition(x, y);
				}

				part->SetCurrentLife(0);
			}

			void ParticleEmiter::Resurect(const Vector2F& pos, const Vector2F& dir, int id, bool wipe/* = false*/)
			{
				this->id = id;
				alive = true;
				active = true;
				//TODO:activate emitter
				position = pos;
				direction = dir;
				if (wipe)
				{
					WipeData();
				}
				else
				{
					recycler = true;
					currentPart = 0;
					current = particles.begin();
				}
			}

			void ParticleEmiter::WipeData()
			{
				SafeRelease<Particle*>(particles);
				currentPart = 0;
			}

			void ParticleEmiter::Kill()
			{
				active = false;
			}

			bool ParticleEmiter::IsAlive()
			{
				return alive;
			}

			int ParticleEmiter::getId()
			{
				return id;
			}

			void ParticleEmiter::setId(int id)
			{
				this->id = id;
			}

			void ParticleEmiter::Move(float x, float y)
			{
				position.Move(x, y);
			}

			void ParticleEmiter::SetActive(bool active)
			{
				this->active = active;
			}

			bool ParticleEmiter::IsActive() const
			{
				return active;
			}

			void ParticleEmiter::SetAlive(bool alive)
			{
				this->alive = alive;
			}

			void ParticleEmiter::SetAsRecycler(bool create)
			{
				this->recycler = create;
			}

			bool ParticleEmiter::IsRecycler() const
			{
				return recycler;
			}

			void ParticleEmiter::SetDirection(Math::Vector2F direction)
			{
				this->direction = direction;
			}

			Math::Vector2F ParticleEmiter::GetDirection() const
			{
				return direction;
			}

			void ParticleEmiter::SetPosition(Vector2F position)
			{
				this->position = position;
			}

			Vector2F ParticleEmiter::GetPosition() const
			{
				return position;
			}
		};
	};
};