/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PHYSICALOBJECT_H
#define	PHYSICALOBJECT_H


#include "../Vector2.h"

namespace spe
{

    namespace physicsSystem
    {

        /**
         * This class represents the basic concept of physical simulation. Represents a
         * physical particle (the implementation of position is left to the children of this class.)
         * capable of moving and have forces applied on it.
         */
        class PhysicalObject
        {
        public:
            PhysicalObject(float velocityX, float velocityY, float accelarationX, float accelarationY);
            PhysicalObject(const Math::Vector2F& velocity, const Math::Vector2F& acceleration);
            PhysicalObject(const PhysicalObject& org);

            PhysicalObject() : velocity(0, 0), generalAcceleration(0, 0), forces(0, 0)
            {
            }

            virtual ~PhysicalObject()
            {
            }

            /**
             * Forwards this physical object in time, calculating new position and new
             * velocity. Furthermore after this call ΣF (forces) is reset.
             * @param seconds the delta between current and new moment.
             */
            virtual void Update(float seconds) = 0;

            /**
             * Returns if mass is infinite.
             * @return true if the object's mass should be considered infinite.
             */
            bool HasInfiniteMass();

            /**
             * Adds the result of a force into ΣF (forces) of this object.
             * @forceToAdd
             */
            void AddForce(const Math::Vector2F& forceToAdd);

            /**
             * Sets ΣF (forces) back to 0.
             */
            void ResetForces();

            /*
             * Setters and getters.
             */

            //get set damping factor
            void SetDamping(float damping);
            float GetDamping() const;

            //get set invertedMass
            void SetInvertedMass(float invertedMass);
            float GetInvertedMass() const;

            //get set mass
            void SetMass(float mass);
            float GetMass() const;

            //get set acceleration
            void SetGeneralAcceleration(const Math::Vector2F& acceleration);
            Math::Vector2F GetGeneralAcceleration() const;

            //get set velocity
            virtual void SetVelocity(const Math::Vector2F& velocity);

            inline const Math::Vector2F& GetVelocity() const
            {
                return velocity;
            }
            //
            virtual Math::Vector2F GetPosition() const = 0;
            void SetForces(Math::Vector2F forces);
            Math::Vector2F GetForces() const;

            void SetGhost(bool ghost);
            bool IsGhost() const;
        protected:

            /**
             * The linear velocity this object has.
             */
            Math::Vector2F velocity;

            /**
             * The general acceleration this object has build to some constant forces such as gravity.
             */
            Math::Vector2F generalAcceleration;

            /**
             * The accumulated force vector(Σf).
             */
            Math::Vector2F forces;

            /**
             * stores the 1/mass (used as this for optimization).
             */
            float invertedMass;

            /**
             * used to remove move energy in each update(due to air resistance f.i.).
             */
            float damping;

            /**
             * If true other entities can pass through this. Ghost objects still generate collision events
             * but they lack the solidCollision resolution.
             */
            bool ghost;

        };
    };

};

#endif	/* PHYSICALOBJECT_H */

