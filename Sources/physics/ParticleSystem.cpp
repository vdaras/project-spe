/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ParticleSystem.h"

#include <list>

#include "../Camera.h"
#include "../Logger.h"
#include "../Rect.h"
#include "../Utilities.h"
#include "ParticleEmitter.h"

namespace spe
{
	namespace physicsSystem
	{
		namespace particleSystem
		{
			ParticleSystem::ParticleSystem()
			{
				m_visible = true;
				lastInUseId = -1;
				firstRemovedId = -1;
				maxCachedEmiters = 20;
				SetAlwaysVisible(true);
			}

			void ParticleSystem::SetName(const std::string& name)
			{
				this->name = name;
			}

			std::string ParticleSystem::GetName() const
			{
				return name;
			}

			void ParticleSystem::SetFirstRemovedId(int firstRemovedId)
			{
				this->firstRemovedId = firstRemovedId;
			}

			int ParticleSystem::GetFirstRemovedId() const
			{
				return firstRemovedId;
			}

			void ParticleSystem::SetLastInUseId(int lastInUseId)
			{
				this->lastInUseId = lastInUseId;
			}

			int ParticleSystem::GetLastInUseId() const
			{
				return lastInUseId;
			}

			void ParticleSystem::SetCreationThreashold(int creationThreashold)
			{
				this->creationThreashold = creationThreashold;
			}

			int ParticleSystem::GetCreationThreashold() const
			{
				return creationThreashold;
			}

			void ParticleSystem::SetPartsPerEmiter(unsigned long int partsPerEmiter)
			{
				this->partsPerEmiter = partsPerEmiter;
			}

			unsigned long int ParticleSystem::GetPartsPerEmiter() const
			{
				return partsPerEmiter;
			}

			void ParticleSystem::SetPrototype(ParticleEmiterPrototype prototype)
			{
				this->prototype = prototype;
			}

			ParticleEmiterPrototype ParticleSystem::GetPrototype() const
			{
				return prototype;
			}

			void ParticleSystem::SetActive(bool active)
			{
				this->active = active;
			}

			bool ParticleSystem::IsActive() const
			{
				return active;
			}

			void ParticleSystem::SetResource(std::string& resource)
			{
				this->resource = resource;
			}

			const char* ParticleSystem::GetResource() const
			{
				return resource.c_str();
			}

			int ParticleSystem::AddPrototypedEmiterAt(const Math::Vector2F& pos, const Math::Vector2F& dir, bool oneTime /*=false*/)
			{
				int id;
				//RemoveLastEmiters();
				//check if we need allocation or we can replace a dead emitter
				if (GetEmiterId(&id))
				{
					AddNewEmiter(new ParticleEmiter(&prototype, pos, dir, id));
				}
				else
				{
					ReplaceEmiter(pos, dir, id, false);
				}

				if (oneTime) KillEmiter(id);
				return id;
			}

			bool ParticleSystem::GetEmiterId(int* id)
			{
				if (firstRemovedId == -1)
				{
					*id = ++lastInUseId;
					return true;
				}
				else
				{
					*id = firstRemovedId;
					firstRemovedId = FindNextRemoved(firstRemovedId);
					return false;
				}
			}

			void ParticleSystem::SetMaxCachedEmiters(int maxCachedEmiters)
			{
				this->maxCachedEmiters = maxCachedEmiters;
			}

			int ParticleSystem::GetMaxCachedEmiters() const
			{
				return maxCachedEmiters;
			}

			void ParticleSystem::SetMapCollisions(bool mapCollisions)
			{
				this->mapCollisions = mapCollisions;
			}

			bool ParticleSystem::IsMapCollisions() const
			{
				return mapCollisions;
			}

			void ParticleSystem::SetCameraBeh(CameraBehaviour cameraBeh)
			{
				this->cameraBeh = cameraBeh;
			}

			ParticleSystem::CameraBehaviour ParticleSystem::GetCameraBeh() const
			{
				return cameraBeh;
			}

			int ParticleSystem::GetEmiterId()
			{
				return ++lastInUseId;
			}

			int ParticleSystem::FindNextRemoved(int from)
			{
				int result = -1;
				int end;
				unsigned uMaxCachedEmiters = static_cast <unsigned>(maxCachedEmiters);

				if (emiters.size() > uMaxCachedEmiters)
				{
					end = maxCachedEmiters;
				}
				else
				{
					end = emiters.size();
				}

				for (int i = from + 1; i < end; ++i)
				{
					if (!emiters[i]->IsAlive())
					{
						result = i;
						break;
					}
				}
				return result;
			}

			bool ParticleSystem::KillEmiter(int id)
			{
				unsigned uID = static_cast <unsigned>(id);

				if (uID >= emiters.size())
				{
					//g++ is complaining about this, thus I disable it for the moment
					LOGERROR << "Emitters vector out of bounds " << uID << " " << emiters.size() << " " << SOURCE;
					return false;
				}
				emiters[id]->Kill();

				return true;
			}

			void ParticleSystem::Draw(sdl::GraphicsCore* gCore, Camera* camera)
			{
				if (m_display != 0 && m_visible)
				{
					const gl::Texture& rsc = *m_display->GetRawData();
					const Rect* rect = camera->GetRect();

					if (cameraBeh == CB_FOLLOW)
					{
						for (unsigned i = 0; i < emiters.size(); i++)
						{
							if (emiters[i]->IsAlive())
							{
								emiters[i]->RenderWrapped(gCore, rsc, m_clip, *rect, m_blending);
							}
						}
					}
					else
					{
						for (unsigned i = 0; i < emiters.size(); i++)
						{
							if (emiters[i]->IsAlive())
							{
								emiters[i]->Render(gCore, rsc, m_clip, *rect, m_blending);
							}
						}						
					}
				}
			}

			ParticleSystem::~ParticleSystem()
			{
				SafeRelease<ParticleEmiter*>(emiters);
			}

			void ParticleSystem::ApplyForcesToParticles(forces::Force* force)
			{
				for (unsigned i = 0; i < emiters.size(); ++i)
				{
					//if emitter is dead skip it.
					//Note that deactivated emitters still need update.
					if (emiters[i]->IsAlive())
					{
						emiters[i]->ApplyForce(force);
					}
				}
			}

			void ParticleSystem::UpdateParticles(unsigned long deltaTime)
			{
				float seconds = deltaTime / 1000.0f;
				for (unsigned i = 0; i < emiters.size(); ++i)
				{
					//if emitter is dead skip it.Note that deactivated emitters
					//still need update.
					if (emiters[i]->IsAlive())
					{
						//update particle emitters

						if (!emiters[i]->Update(seconds))
						{
							SubmitDeath(i);
						}
					}
				}
			}

			void ParticleSystem::RemoveLastEmiters()
			{
				unsigned int size = emiters.size() - 1;

				if (size < maxCachedEmiters)
					return;

				for (unsigned i = size; i >= maxCachedEmiters; i--)
				{
					//if emitter is alive stop the iteration
					if (emiters[i]->IsAlive())
						break;

					//reduce last in use id
					lastInUseId--;

					//delete emitter
					delete emiters[i];

					//and remove it from the vector
					emiters.pop_back();
					size--;
				}
			}

			void ParticleSystem::MoveEmiters(float x, float y)
			{
				for (auto& emitter : emiters)
				{
					if (emitter->IsActive())
					{
						emitter->Move(x, y);
					}
				}
			}

			void ParticleSystem::MoveManipulators(float x, float y)
			{
				//TODO: add move method here if field manipulators added
			}

			void ParticleSystem::MoveSystem(float x, float y, bool moveParticles)
			{
				//position.Move(x, y);
				MoveEmiters(x, y);
			}

			void ParticleSystem::AddParticles()
			{
				for (auto& emitter : emiters)
				{
					//emitter should be alive in order to add particles
					if (emitter->IsAlive())
					{
						if (emitter->IsRecycler())
						{
							emitter->AddParticlesViaRecycle(creationThreashold);
						}
						else
						{
							emitter->AddParticles(creationThreashold);
						}
					}
				}
			}

			void ParticleSystem::TileCollision(const LevelMap* map, float mapOffsetX, float mapOffsetY)
			{
				for (auto& emitter : emiters)
				{
					if (emitter->IsAlive())
					{
						emitter->TileCollisionCheck(map, mapOffsetX, mapOffsetY);
					}
				}
			}
		};
	};
};