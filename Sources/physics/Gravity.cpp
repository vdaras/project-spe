/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdlib.h>

#include "Gravity.h"
#include "PhysicalObject.h"

namespace spe
{
	namespace physicsSystem
	{
		namespace forces
		{
			Gravity::Gravity(const Math::Vector2F& gravity_) : gravity(gravity_)
			{
			}

			Gravity::Gravity(float magnitude, int direction) : gravity(magnitude, direction, false)
			{
			}

			void Gravity::Prepare(float seconds)
			{
				//gravity.RotateVector(rand()% 3);
			}

			void Gravity::SetGravity(Math::Vector2F gravity)
			{
				this->gravity = gravity;
			}

			Math::Vector2F Gravity::GetGravity() const
			{
				return gravity;
			}

			void Gravity::Affect(PhysicalObject* object)
			{
				if (!object->HasInfiniteMass())
				{
					object->SetGeneralAcceleration(gravity * object->GetMass());
				}
			}
		};
	};
};