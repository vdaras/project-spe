/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _RANDLIB_H
#define _RANDLIB_H

#include <iostream>
#include <typeinfo>
#include "Vector2.h"

namespace spe
{
	namespace ParkMillerRNG
	{
		double Uniform(double a, double b);
		long Equilikely(long a, long b);
		double Random(void);
	}

	namespace Math
	{
		void ReinitRand(long x);

		int Random(int min, int max);
		float Random(float min, float max);
		double Random(double min, double max);
		Math::Vector2F Random(const Math::Vector2F& min, const Math::Vector2F& max);

		bool EventCheck(float chance);
	}
}

#endif /* _RANDLIB_H */
