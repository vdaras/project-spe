/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _RESOURCE_H
#define	_RESOURCE_H
#include <cstdlib>

namespace spe
{

template <typename Type>
class Resource
{
public:

    Resource()
    {
        references = 0;
        important = false;
        rawData = nullptr;
    }

    Resource(Type* inRes)
    {
        rawData = inRes;
        references = 0; //default value
        important = false; //default value
    }

    Resource(bool inImportant, Type* inRes)
    {
        important = inImportant;
        rawData = inRes;
        references = 0; //default value
    }

    virtual ~Resource()
    {
        delete rawData;
    }

    int GetReferences()
    {
        return references;
    }

    void SetImportant()
    {
        important = true;
    }

    void SetUnimportant()
    {
        important = false;
    }

    Type* GetRawData()
    {
        return rawData;
    }

    bool IsImportant()
    {
        return important;
    }

    void IncreaseReference()
    {
        references++;
    }

    void DecreaseReference()
    {
        if(references > 0)
            references--;
    }

private:
    /**
     * The number of references this Resource has.
     * Used by garbage collector.
     */
    int references;

    /**
      * Indicator used by garbage collector
      */
    bool important;

    /**
     * Pointer to the true data.
     */
    Type* rawData;
};

};

#endif	/* _RESOURCE_H */

