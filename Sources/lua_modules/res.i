%module res
%{
/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "../Resource.h"
#include "../global_script_functions.h"
#include "../sdl/Font.h"

using namespace spe;
%}

namespace spe
{

%nodefaultctor Resource;
template <typename Type>
class Resource
{
public:
    void SetImportant();
    void SetUnimportant();
    bool IsImportant();
    void DecreaseReference();
};

%template(FontResource) Resource<spe::sdl::Font>;

%nodefaultctor ResourceManager;
class ResourceManager
{
public:
      Resource<spe::sdl::Font>* GetFont(const char* name, int size);  
};

ResourceManager* GetResourceManager();

}