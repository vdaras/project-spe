%include <std_string.i>

%module audio
%{

#include "../global_script_functions.h"
#include "../audio/AudioCore.h"
#include "../audio/AbstractALSource.h"
#include "../audio/AudioManager.h"
#include "../audio/AudioSource.h"
#include "../audio/StreamSource.h" 

using namespace spe;
%}

namespace spe
{

namespace al
{

%nodefaultctor AbstractALSource;
class AbstractALSource
{
public:
    void Play();
    void Pause();
    virtual void Rewind();
    void Stop();
    bool IsPlaying();
    bool IsPaused();
    bool IsStopped();
    float GetPitch();
    void SetPitch(float pitch);
    float GetGain();
    void SetGain(float gain);
    void SetLooping(bool loop);
    bool IsLooping();
    void SetNumOfLoops(int numOfLoops);
};

%nodefaultctor AudioSource;
class AudioSource : public AbstractALSource
{
public:
    bool Load(const char* file);
    void SetOneTime(bool oneTime);
private:
	virtual ~AudioSource();	
};

%nodefaultctor StreamSource;
class StreamSource : public AbstractALSource
{
public:
    virtual void Rewind();
	bool Prepare(const char* filepath);
private:
	virtual ~StreamSource();	
};

class AudioManager
{
public:
    void PlayAll();
    void PauseAll();
    AudioSource* CreateSource();
    void KillSource(AudioSource* toKill);
    StreamSource* CreateStream();
    void KillStream(StreamSource* toKill);
    void KillOneTimeSounds();
};

};

void PlaySound(const std::string& name);
void PlayMusic(const std::string& name);

};