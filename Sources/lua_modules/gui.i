%include <std_string.i>

%module gui
%{ 
/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "../gui/Button.h"
#include "../gui/Container.h"
#include "../gui/ImageButton.h"
#include "../gui/Label.h"
#include "../gui/Picture.h"  
#include "../gui/gui_script_functions.h"

using namespace spe;
%}

namespace spe
{

namespace gui
{
    %nodefaultctor Component;
    class Component
    {
       public:

       virtual void Resize(int width, int height) = 0;
       
       void Hide();
       int GetX() const;
       int GetY() const;
       int GetRelativeX() const;
       int GetRelativeY() const;
       int GetWidth() const;
       int GetHeight() const;
       bool IsVisible() const;
       std::string GetName() const;
       void SetName(const std::string &name);
       void SetPosition(int x, int y);
       void SetX(int x);
       void SetY(int y);
       void Show();
    };


    %nodefaultctor AbstractButton;
    class AbstractButton : public Component
    {

    };


    %nodefaultctor Button;
    class Button : public AbstractButton
    {
    public:
        virtual void Resize(int width, int height);
        
        void SetCaption(const std::string &caption);
        void SetBackgroundColor(const sdl::Color &color);
        void SetForegroundColor(const sdl::Color &color);
        void SetHighlightBackgroundColor(const sdl::Color &color);
        void SetHighlightForegroundColor(const sdl::Color &color);
        void SetClickedBackgroundColor(const sdl::Color &color);
        void SetClickedForegroundColor(const sdl::Color &color);
    };

    %nodefaultctor Container;
    class Container : public Component
    {
    public:
        virtual void Resize(int width, int height);
       
        void AddComponent(Component* toAdd);  
        void AddComponent(Component* toAdd, int x, int y);
        void BringToFront(Component* comp);
        Component *FindComponent(const std::string& name);
    };


    %nodefaultctor ImageButton;
    class ImageButton : public AbstractButton
    {

    };


    %nodefaultctor Label;
    class Label : public Component
    {
    public:
        void SetText(const std::string& text);
        void SetTextColor(const sdl::Color &color);
        void SetFont(Resource<sdl::Font> *fnt);
    };


    %nodefaultctor Picture;
    class Picture : public Component
    {

    };

    Button *CreateButton(Container* parent, const std::string &name, int x, int y, int width, int height, const std::string& caption, Resource<sdl::Font> *fnt);
    Picture *CreatePicture(Container* parent, const std::string &name, int x, int y, const std::string &path);
    Label *CreateLabel(Container* parent, const std::string &name, const std::string& txt, int x, int y, int width, int height, Resource<sdl::Font> *fnt);
}

}
