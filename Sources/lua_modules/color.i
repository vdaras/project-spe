%module color
%{
/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "../sdl/GLColor.h"  
%}

namespace spe
{
namespace gl
{
class Color
{
public:
	Color();
	Color(GLubyte r, GLubyte g, GLubyte b, GLubyte a = 255);
	const Color operator + (const Color& org) const;
	const Color operator - (const Color& org) const;
	const Color operator * (float value) const;
	const Color operator / (float value) const;
	void SetRed(GLubyte r);
	void SetGreen(GLubyte g);
	void SetBlue(GLubyte b);
	void SetAlpha(GLubyte a);
	GLubyte Red() const;
	GLubyte Green() const;
	GLubyte Blue() const;
	GLubyte Alpha() const;
};
}
}