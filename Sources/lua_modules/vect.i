%module vect
%{
/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "../Vector2.h"  
%}

namespace spe
{

namespace Math
{

template <typename NumType >
class Vector2Templ
{
public:
    Vector2Templ();
    Vector2Templ(const Vector2Templ& toCopy);
    Vector2Templ(NumType magnitude, int angle, bool clockwise);
    Vector2Templ(float x, float y);

    void Set(NumType magnitude, int angle, bool clockwise);
    void Set(const Vector2Templ& v);
    void Set(float x, float y);
    void SetX(float x);
    void SetY(float y);
    NumType GetX() const;
    NumType GetY() const;
    void Move(float mx, float my);
    bool IsNull() const;
    bool operator ==(const Vector2Templ& p) const;
    bool operator !=(const Vector2Templ& p) const;
    const Vector2Templ operator -() const;
    Vector2Templ & operator =(const Vector2Templ& v);
    Vector2Templ operator +(const Vector2Templ& v) const;
    Vector2Templ operator -(const Vector2Templ& v) const;
    Vector2Templ operator *(const Vector2Templ& v) const;
    Vector2Templ operator /(const Vector2Templ& v) const;
    Vector2Templ operator +(float v) const;
    Vector2Templ operator -(float v) const;
    Vector2Templ operator *(float v) const;
    Vector2Templ operator /(float v) const;
    bool GetDirectionFactor(float* factor);
    bool ArePerpendicular(const Vector2Templ& vector) const;
    bool AreOpposite(const Vector2Templ& vector) const;
    bool AreParellel(const Vector2Templ& vector) const;
    float Length() const;
    NumType SquareLength() const;
    template<typename T> float Distance(const Vector2Templ<T>& vector) const;
    float CrossProduct(const Vector2Templ& v) const;
    float DotProduct(const Vector2Templ& v) const;
    float AngleInRads(const Vector2Templ& v) const;
    int AngleInDegrees(const Vector2Templ& v) const;
    void Normalize();
    Vector2Templ<float> Normal() const;
    void Invert();
    void Flip();
    void Absolute();
    Vector2Templ GetAbsolute() const;
    int GetAngleInDegrees() const;
    Vector2Templ GetRotatedVector(float angleRads) const;
    void RotateVector(float angleRads);
    Vector2Templ ProjectionOn(const Vector2Templ& vect) const;
};

}

}

%template(Vector2F) spe::Math::Vector2Templ<float>;