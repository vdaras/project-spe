%include <std_string.i>
 
%module core
%{
/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "../Entity.h"
#include "../EntityManager.h"
#include "../RenderingManager.h"
#include "../physics/PhysicsManager.h"
#include "../LuaScriptState.h"
#include "../Property.h"
#include "../Vector2.h"
#include "../global_script_functions.h"  
#include "../audio/AudioManager.h"

using namespace spe;
%}

namespace spe
{

namespace EntitySystem
{
    class AnimationLogic
    {
    public:
        enum AfterEnd
        {
            ANIM_ONESHOT,
            ANIM_LOOPING,
            ANIM_TODEFAULT
        };
		
		//Animation methods
        void Play(const std::string& animation, AfterEnd afterEnd = ANIM_TODEFAULT);
        void Pause();
        void Resume();
        void SetPlaying(bool playing);
        void SetCurrentFrame(int currentFrame);
        void SetCurrentAnimation(const std::string &currentAnimation);
        void SetDefaultAnimation(const std::string &defaultAnimation);
		
		//Drawable methods
		void Hide();
		void Show();
		
		void SetScale(const spe::Math::Vector2Templ<float>& scale);
		spe::Math::Vector2Templ<float> GetScale() const;
		void SetAngle(float angle);
		float GetAngle() const;
		void SetColor(gl::Color color);
		gl::Color GetColor() const;
    };

  
    class Property
    {
    public:
        void StoreInt(int);
        int AsInt();
        void StoreFloat(float);
        float AsFloat();
        void StoreString(const std::string& str);
        std::string AsString();
        void StoreVector(const spe::Math::Vector2Templ<float> &vector);
        spe::Math::Vector2Templ<float>* AsVector();
    };


    class PhysicsLogic
    {
    public:
        bool HasSolidContactOn(const spe::Math::Vector2Templ< float >& direction);
        bool HasTileContactOn(const spe::Math::Vector2Templ< float >& direction, const std::string& flag);
        bool HasEntityContactOn(const spe::Math::Vector2Templ< float >& direction);
        void SetIgnoreGeneralForces(bool value);
        void ApplyDampingOnX(bool dampingOnX);
        void ApplyDampingOnY(bool dampingOnY);
        void SetVelocity(const spe::Math::Vector2Templ< float >& velocity);
        const spe::Math::Vector2Templ< float >& GetVelocity() const;
    };


    class Entity
    {
    public:
        std::string GetID();
        void Kill();
        std::string GetType() const;
        std::string GetCategory() const;
		
		void SetPosition(const spe::Math::Vector2Templ< float >& pos);
		void SetPosition(float x, float y);
		
        spe::Math::Vector2Templ< float > GetPosition();
        int GetPositionX();
        int GetPositionY();
        AnimationLogic* GetAnimationLogic();
        PhysicsLogic* GetPhysicsLogic();
        Property* GetProperty(const std::string& name);
		
		void NotifyState(const std::string &msg);
    }; 
};


class PhysicsManager
{
public:
    template <class NumType>
    spe::Math::Vector2Templ<NumType> RaycastTile(const spe::Math::Vector2Templ<NumType> &rayStart, const spe::Math::Vector2Templ<NumType> &direction, float distance);

    template <class NumType>
    spe::Math::Vector2Templ<NumType> RaycastObject(const spe::Math::Vector2Templ<NumType> &rayStart, const spe::Math::Vector2Templ<NumType> &direction, float distance);
};


class Camera
{
public:
    Camera(int x,int y, int w, int h);
    void SetFocus(int x, int y);
    void Move(int x, int y);
};


class RenderingManager
{
public:
    void AddLayer(const std::string& layer, float parallaxFactor = 0);
    void RemoveLayer(const std::string& layer);
    void BringToFront(const std::string& layer);
    void SendToBack(const std::string& layer);
    void BringForward(const std::string& layer, unsigned int positions = 1);
    void SendBackward(const std::string& layer, unsigned int positions = 1);
    Camera *GetCamera();
};

%rename(renderer) LuaScriptState::m_renderingManager;
%rename(physics) LuaScriptState::m_physicsManager;
%rename(audioMan) LuaScriptState::m_audioManager;

class LuaScriptState
{
public:
    RenderingManager* m_renderingManager;
    PhysicsManager* m_physicsManager;
    al::AudioManager* m_audioManager;

    void LoadMap(const std::string &file, const std::string& layer);
	void UnLoadMap();
    spe::gui::Container *GuiContents();
};

EntitySystem::Entity* SpawnEntity(const std::string& type, const std::string& id, int x, int y, const std::string& layer);
EntitySystem::Entity* FindEntity(const std::string& ID);
int SpawnParticles(const std::string& type, const spe::Math::Vector2Templ< float > position, const spe::Math::Vector2Templ< float > direction, bool oneTime, const std::string& layer);

void PushState(const std::string& scriptStateClass);

void PopStateStack();

void AddDrawable(const std::string& imageName, int x, int y, const std::string& layer);



}