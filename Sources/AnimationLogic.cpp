/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "AnimationLogic.h"
#include "Logger.h"
#include "sdl/GraphicsCore.h"
#include "Entity.h"
#include "PhysicsLogic.h"

#define FirstFrame 0

namespace spe
{
	namespace EntitySystem
	{
		AnimationLogic::AnimationLogic()
		{
			currentAnimation = defaultAnimation;
			currentFrame = FirstFrame;
		}

		AnimationLogic::AnimationLogic(const AnimationLogic& orig) :
			DrawLogic(orig),
			currentAnimation(orig.currentAnimation),
			defaultAnimation(orig.defaultAnimation),
			currentFrame(orig.currentFrame),
			playing(orig.playing),
			afterEnd(orig.afterEnd)
		{
			AnimationTable::const_iterator it = orig.animationMap.begin();
			AnimationTable::const_iterator end = orig.animationMap.end();

			for (; it != end; ++it)
			{
				this->animationMap[it->first] = it->second;
			}
		}

		AnimationLogic::~AnimationLogic()
		{
		}

		AnimationLogic* AnimationLogic::Clone()
		{
			return new AnimationLogic(*this);
		}

		void AnimationLogic::Draw(sdl::GraphicsCore* gCore, Camera* camera)
		{
			//get current animation frame
			AnimationFrame cFrame = animationMap[currentAnimation];

			//update animation
			int frameNumber = Update(cFrame);

			////draw
			const gl::Texture* texture = m_display->GetRawData();
			if (texture == nullptr)
			{
				LOG(Logger::CHANNEL_GRAPHICS, "Error") << "Animation logic failed to draw. Image is nullptr.";
				return;
			}

			Math::Rect firstFrame = cFrame.firstFrame;
			//create the clip of the image
			Math::Rect clip(
				firstFrame.GetTop(),
				firstFrame.GetLeft() + firstFrame.GetWidth() * (frameNumber),
				firstFrame.GetHeight(),
				firstFrame.GetWidth()
				);
			//actual draw
			gCore->Render(*texture, clip, m_color, camera->Translate(GetPosition()), m_scale, m_angle, cFrame.flip, m_blending);

#ifdef DEBUG_DRAW

			PhysicsLogic* ph = parent->GetPhysicsLogic();
			if (!ph) return;

			Math::Vector2F cent(camera->Translate(GetPosition()) + clip.GetDimensions() / 2);

			gCore->DrawLineRect(camera->Translate(GetPosition())
				, camera->Translate(GetPosition()) + ph->GetRect()->GetDimensions(), ph->IsAwake() ? gl::colors::YELLOW : gl::colors::CYAN, 2);

			gCore->DrawLine(cent, cent + ph->GetVelocity(), gl::colors::GREEN);

			gCore->DrawLine(cent + Math::Vector2F::Right * 4, cent + Math::Vector2F::Right * 4 + ph->GetGeneralAcceleration(), gl::colors::RED);

			gCore->DrawLine(cent + Math::Vector2F::Left * 4, cent + Math::Vector2F::Left * 4 + ph->GetOldVelocity(), gl::colors::BLUE);
#endif
		}

		int AnimationLogic::Update(AnimationFrame& frame)
		{
			//if playing is false it means that there should be no animation
			if (playing)
			{
				//check if enough time has passed since the last update
				if (timer.Update(frame.totalTime))
				{
					//add one to current frame and check if it has surpassed the limit
					if (++currentFrame >= frame.numberOfFrames)
					{
						//if animation is one time play stop it
						if (afterEnd == ANIM_ONESHOT)
						{
							playing = false;
						}
						else if (afterEnd == ANIM_TODEFAULT)  //return to default
						{
							currentAnimation = defaultAnimation;
							//and reset the counter
							currentFrame = FirstFrame;
							//frame = animationMap[currentAnimation];
						}
						else/*afterEnd == ANIM_LOOPING*/
						{
							currentFrame = FirstFrame;
						}
					}
				}
			}

			return currentFrame;
		}

		void AnimationLogic::Play(const std::string& animation, AfterEnd afterEnd)
		{
			if (animation == currentAnimation)
			{
				this->afterEnd = afterEnd;
				return;
			}

			if (animationMap.find(animation) != animationMap.end())
			{
				currentAnimation = animation;
				this->afterEnd = afterEnd;
				playing = true;
				//reset frame counter
				currentFrame = FirstFrame;
				//...and timer
				timer.Reset();
			}
			else
			{
				currentAnimation = defaultAnimation;
				LOG(Logger::CHANNEL_GRAPHICS, "Error") << "Animation logic failed to play animation. "
					"Such animation name (" << animation << ") does not exist.";
			}
		}

		void AnimationLogic::Pause()
		{
			timer.Reset();
			this->playing = false;
		}

		void AnimationLogic::Resume()
		{
			playing = true;
		}

		void AnimationLogic::AddFrame(const std::string& name, const AnimationFrame& frame)
		{
			animationMap[name] = frame;
		}

		bool AnimationLogic::GetFrame(const std::string& name, AnimationFrame& frame)
		{
			if (animationMap.find(name) != animationMap.end())
			{
				frame = animationMap[name];
				return true;
			}
			return false;
		}

		void AnimationLogic::SetPlaying(bool playing)
		{
			this->playing = playing;
		}

		void AnimationLogic::SetCurrentFrame(int currentFrame)
		{
			this->currentFrame = currentFrame;
		}

		void AnimationLogic::SetCurrentAnimation(const std::string& currentAnimation)
		{
			this->currentAnimation = currentAnimation;
		}

		std::string AnimationLogic::GetCurrentAnimation() const
		{
			return currentAnimation;
		}

		void AnimationLogic::SetDefaultAnimation(const std::string& defaultAnimation)
		{
			this->defaultAnimation = defaultAnimation;
		}

		/**
		 * Overrides Draw Logic's GetWidth in order to supply valid image width.
		 *
		 * @return current frame's width.
		 */

		int AnimationLogic::GetWidth() const
		{
			//find current animation
			std::map< std::string, AnimationFrame >::const_iterator currAnimation(animationMap.find(currentAnimation));

			return(currAnimation != animationMap.end()) ? currAnimation->second.firstFrame.GetWidth() : 0;
		}

		/**
		 * Overrides Draw Logic's GetHeight in order to supply valid image height.
		 *
		 * @return current frame's height.
		 */

		int AnimationLogic::GetHeight() const
		{
			//find current animation
			std::map< std::string, AnimationFrame >::const_iterator currAnimation(animationMap.find(currentAnimation));

			return(currAnimation != animationMap.end()) ? currAnimation->second.firstFrame.GetHeight() : 0;
		}
	}
}