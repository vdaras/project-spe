/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ext/tinyxml/tinyxml.h"
#include "TileProperties.h"
#include "Globals.h"
#include "Logger.h"

namespace spe
{

TileProperties::TileProperties()
{
}

TileProperties::~TileProperties()
{
}

bool TileProperties::ParsePoperties(const char* filename)
{
    TiXML::TiXmlDocument xFile((Globals::GetInstance()->GetPath("TileProperties") + filename).c_str());
    if(!xFile.LoadFile()) return false;

    //get the root of the file : map
    TiXML::TiXmlElement* xProps = xFile.FirstChildElement("TileProperties");
    if(!xProps) return false;

    TiXML::TiXmlElement* xItem = xProps->FirstChildElement();
    unsigned long key = 0;
    while(xItem)
    {
        std::string value(xItem->Attribute("value"));
        //with each iteration we get the next binary number power of 2

        tileProperties[(1 << key)] = value;
        key++;
        {
            //free this item and fetch the next one
            TiXML::TiXmlElement* tmp = xItem->NextSiblingElement();
            xItem->Clear();
            xItem = tmp;
        }
    }
    return true;
}

unsigned long TileProperties::ValueOf(const std::string& name) const
{
    propertiesMap::const_iterator it = tileProperties.begin();
    propertiesMap::const_iterator end = tileProperties.end();
    for(; it != end; ++it)
    {
        if(it->second == name)
            return it->first;
    }
    LOGERROR << "Name : " << name <<" not found in tile properties";
    return 0;
}

std::string TileProperties::NameOf(unsigned long flag)
{
    return tileProperties[flag];
}

}
