/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ENTITYPARTICLECOMPONENT_H
#define	ENTITYPARTICLECOMPONENT_H

#include "EntityComponent.h"
#include "../Vector2.h"
#include <string>

namespace spe
{

namespace EntitySystem
{

    namespace components
    {

        class ParticleComponent : public EntityComponent
        {
        public:
            ParticleComponent(const std::string& sys,const Math::Vector2F& relPosit);

            ParticleComponent(const ParticleComponent& orig);

            virtual ~ParticleComponent();

            virtual ParticleComponent* Clone();

            bool RegisterEmiter();

            /**
             * @return The type of this component.
             */
            virtual ENTCOMP_TYPES GetType()
            {
                return ENTCOMP_PARTICLES;
            }

            void SetSystem(std::string& system);

            void SetRelativePosition(const Math::Vector2F& relPos);

        private:

            /**
             * The name of the particle system this component registers its emitters.
             */
            std::string system;

            /**
             * The relative position of this component in Entity.
             */
            Math::Vector2F relPosition;

            int emiterId;
        };

    };

};

};

#endif	/* ENTITYPARTICLECOMPONENT_H */
