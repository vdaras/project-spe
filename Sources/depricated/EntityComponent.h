/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ENTITYCOMPONENT_H
#define	ENTITYCOMPONENT_H

#include "EntityComponentTypes.h"

namespace spe
{

namespace EntitySystem
{

    class Entity;
    union GameEvent;

    namespace components
    {

        /**
         * This is the root of all EntityComponents. Provides the required interface
         * in order to handle events.
         */
        class EntityComponent
        {
        public:

            EntityComponent() {}
            virtual ~EntityComponent() {}
            /**
             * @return The type of this component.
             */
            virtual ENTCOMP_TYPES GetType() = 0;

            /**
             *
             * @return a new instance of the specific subtype of EntityComponent.
             */
            virtual EntityComponent* Clone() = 0;

            inline void SetParent(Entity* parent)
            {
                this->parent = parent;
            }

            inline Entity* GetParent() const
            {
                return parent;
            }
        protected:
            Entity* parent;//the entity this Component is attached to.
        };

    };
};

};

#endif	/* ENTITYCOMPONENT_H */
