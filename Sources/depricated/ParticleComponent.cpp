/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ParticleComponent.h"
#include "../physics/ParticleSystemManager.h"
#include "../Entity.h"
#include "../physics/PhysicsManager.h"

namespace spe
{

namespace EntitySystem
{
    namespace components
    {

        ParticleComponent::ParticleComponent(const std::string& sys,const Math::Vector2F& relPosit):system(sys),relPosition(relPosit)
        {
            emiterId = -1;
        }

        ParticleComponent::ParticleComponent(const ParticleComponent& orig):system(orig.system),relPosition(orig.relPosition)
        {
            emiterId = -1;
        }

        ParticleComponent::~ParticleComponent()
        {
            if(emiterId != -1)
            {
                //FIXME Andy
                //physicsSystem::particleSystem::ParticleSystem* sys = physicsSystem::PhysicsManager::GetInstance()->GetParticleSystem(system);
                //sys->KillEmiter(emiterId);
                emiterId = -1;
            }
        }

        ParticleComponent* ParticleComponent::Clone()
        {
            return new ParticleComponent(*this);
        }

        bool ParticleComponent::RegisterEmiter()
        {

            //emiterId = sys->AddPrototypedEmiterAt(relPosition + parent->GetPosition());
            return true;
        }

    }

}
}
