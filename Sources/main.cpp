/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Globals.h"
#include "Logger.h"
#include "EngineCore.h"

//if on MS VS include the memory profiler
#ifdef _MSC_VER
//#include <vld.h>
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

using namespace spe;

//The entry point of the program

int main(int argc, char* args[])
{
	Logger::GetInstance()->InitializeFiles(true, false);

	LOGNOTE << "Engine started. " << "Build date : ";
	LOGCHAIN_NOTE << BUILD_TIME;

	Globals* glob = Globals::GetInstance();

	glob->Initialization(true, "main.xml");

	EngineCore game;

	int result;

	if (game.Init(glob->GetScreenWidth(), glob->GetScreenHeight(), 32, glob->GetFullscreen()))
	{
		result = game.Run();
	}
	else
	{
		result = -1;
		LOG(Logger::CHANNEL_LOADING, "Error") << "Failed to initialize engine.";
	}

	LOGNOTE << "Engine terminated.";

	Logger::DeleteInstance();
	return result;
}
