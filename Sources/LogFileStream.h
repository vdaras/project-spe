/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef LOGFILESTREAM_H
#define	LOGFILESTREAM_H

#include "Vector2.h"
#include "Rect.h"
#include "Circle.h"

#include <list>
#include <fstream>

namespace spe
{
	class LogFileStream : public std::ofstream
	{
	public:

		/**
		 * Error Level ids.
		 */
		enum LEVEL
		{
			LEVEL_ERROR,
			LEVEL_WARNING,
			LEVEL_NOTE
		};

		/**
		 * Writes a new line with a timestamp and returns the stream for the user to start logging.
		 * @param level The level id. Use the LogFileStream::LEVEL_* enum values.
		 * @return The stream ready to accept external values.
		 */
		LogFileStream& Log(int level = LEVEL_NOTE);

		void Initialize(const char* name, bool html);

		bool IsTerminated();

		void DeclareAsTerminated();

		void DeclareAsUnterminated();

		bool IsChainWrite();

		void SetChanWrite(bool flag);

	private:
		/**
		 * A flag to indicate if a stream has been terminated or not.
		 * Termination is used with html formating and chain writting.
		 */
		bool m_terminated;

		/**
		 * A flag to indicate that the stream is in a state of chain write.
		 * Via chain write we cann add multiple lines in a file without adding a new header.
		 * (date,open tags etc.).
		 */
		bool m_chainWrite;
	};
};

/**
 * Adds the current system time in the stream.
 * Should be called just like endl.
 * @return
 */
std::ostream& Now(std::ostream& stream);

/**
 * As the above but the date has an html format.
 * Should be called just like endl.
 * @return
 */
std::ostream& NowTaged(std::ostream& stream);

/**
 * Closes the div element.
 * Should be called just like endl.
 * @return
 */
std::ostream& CloseTag(std::ostream& stream);

/*
 * Overloaded stream operators used to print out engine specific classes easily.
 */
std::ostream& operator <<(std::ostream& stream, const spe::Math::Vector2I& vector);
std::ostream& operator <<(std::ostream& stream, const spe::Math::Vector2F& vector);
std::ostream& operator <<(std::ostream& stream, const spe::Math::Vector2D& vector);
std::ostream& operator <<(std::ostream& stream, const spe::Math::Circle& circle);
std::ostream& operator <<(std::ostream& stream, const spe::Math::Rect& rect);
std::ostream& operator <<(std::ostream& stream, const std::list<std::string>& list);

#endif	/* LOGFILESTREAM_H */
