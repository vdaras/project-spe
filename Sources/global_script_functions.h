/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef GLOBAL_SCRIPT_FUNCTIONS_H
#define	GLOBAL_SCRIPT_FUNCTIONS_H

#include "physics/CollisionEvents.h"
#include "Entity.h"
#include "PhysicsLogic.h"
#include "ResourceManager.h"
#include <string>

namespace spe
{

class Camera;

using namespace EntitySystem;

/**
 * This routine is used by scripts in order to spawn permanet entities in the
 * game. If we just instansiated an Entity object in script it would be killed
 * when all it's references went out of scope.
 *
 * @param type - type of the entity
 * @param x - spawn x coordinate
 * @param y - spawn y coordinate
 * @param layer - rendering layer to be spawned on.
 */
Entity* SpawnEntity(const std::string& entityType, const std::string& id, int x, int y, const std::string& layer);


/**
 * Returns an Entity by ID. If it does not exist, return nullptr.
 *
 * @param ID: id of the entity.
 * @return a handle to the requested Entity or nullptr if it doesn't exist.
 */
Entity* FindEntity(const std::string& ID);

void AddDrawable(const std::string& imageName, int x, int y, const std::string& layer);

/**
 * This function is used by scripts in order to create a particle effect.
 * @param type - The type of the effect to spawn.
 * @param position - The position of this effect.
 * @param oneTime - If true the effect will play only once.
 * @param layer - rendering layer to spawn particles on
 * @return  the id of the emitter that was created for this effect. Useful if oneTime is set to false.
 */
int SpawnParticles(const std::string& type, const Math::Vector2F& position, const Math::Vector2F& direction, bool oneTime, const std::string& layer);

/**
 * Creates an one time audio source to play the specified sound.
 * @param name
 * @param position
 */
void PlaySound(const std::string& name/*, const Math::Vector2F position*/);

/**
 * Plays the specified music file in loop.
 * @param name
 */
void PlayMusic(const std::string& name);

/**
 * Returns the requested entity. nullptr if not found.
 * @param name
 * @return
 */
Entity* FindEntity(const std::string& name);

/**
 * Returns a handle to the current camera service.
 *
 * @return a pointer to a camera.
 */
Camera* GetCamera();


/**
 * Loads a key mapping from the xml file keymap.xml.
 *
 * @param tag: the xml node in keympap.xml that contains the mapping.
 */
void LoadKeyAssociations(const std::string& tag);


/**
 * Pushes a script state to the state stack.
 *
 * @param scriptStateClass: name of the angelscript ScriptState class.
 */
void PushState(const std::string& scriptStateClass);


/**
 * Pops the state stack.
 */
void PopStateStack();


/**
 * Proxy method used to extract entity contact info from a physical logic object.
 * Used in order to surpass the triangle of independence between Entity PhysicsLogic and EntityContactInfo
 * @param logic
 * @return
 */
physicsSystem::PhysicsInternals::EntityContactInfo* GetNextContact(PhysicsLogic* logic);

/**
 * Prints a string to the standard output.
 *
 * @param toPrint - string to print
 */
void Print(const std::string& toPrint);


/**
 * Prints an integer to the standard output.
 *
 * @param toPrint - integer to print
 */
void PrintInt(int toPrint);

/**
 * Prints a bool to the standard output.
 *
 * @param toPrint - bool to print
 */
void PrintBool(bool toPrint);


/**
 * Prints an unsigned integer to the standard output.
 *
 * @param toPrint - unsigned to print
 */
void PrintUnsigned(Uint32 toPrint);


/**
 * Prints a float or a double.
 *
 * @param toPrint - number to print
 */
void PrintReal(double toPrint);

/**
 * Prints a vector.
 *
 * @param toPrint - vector to print
 */
void PrintVector(const Math::Vector2F& toPrint);


/**
 * Returns a pointer to the resource manager.
 */
ResourceManager* GetResourceManager();



}

#endif	/* GLOBAL_SCRIPT_FUNCTIONS_H */

