/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef SELFDRAWABLE_H
#define	SELFDRAWABLE_H

#include "Drawable.h"

namespace spe
{

class SelfDrawable : public Drawable
{
public:
    SelfDrawable();
    SelfDrawable(const SelfDrawable& orig);
    virtual ~SelfDrawable();

    virtual bool IsVisible(const Camera& camera) const;

    virtual Math::Vector2F GetPosition() const;
    virtual void SetPosition(const Math::Vector2F& position);
    void Draw(sdl::GraphicsCore* gCore, Camera* camera);
    void SetAlwaysVisible(bool alwaysVisible);
    bool IsAlwaysVisible() const;

    virtual int GetX() const;

    virtual void SetX(int newX);

    virtual int GetY() const;

    virtual void SetY(int newY);

    virtual int GetWidth() const;

    virtual int GetHeight() const;

private:

    Math::Vector2F position;
    bool alwaysVisible;
};

};

#endif	/* SELFDRAWABLE_H */

