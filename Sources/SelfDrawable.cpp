/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "SelfDrawable.h"
#include "Camera.h"
#include "sdl/GraphicsCore.h"

namespace spe
{

SelfDrawable::SelfDrawable()
{
}

SelfDrawable::SelfDrawable(const SelfDrawable& orig):Drawable(orig)
{
}

SelfDrawable::~SelfDrawable()
{
}

bool SelfDrawable::IsVisible(const Camera& camera) const
{
    if(alwaysVisible) return true;
    if(!m_visible) return false;
    int posX = position.GetX();
    int posY = position.GetY();
    return (camera.GetRect()->GetLeft() <= posX + m_display->GetRawData()->GetWidth() * GetScale().GetX() &&
            camera.GetRect()->GetTotalWidth() <= posX) &&
           (camera.GetRect()->GetTop() <= posY + m_display->GetRawData()->GetHeight()  * GetScale().GetY()&&
            camera.GetRect()->GetTotalHeight() <= posY);
}

int SelfDrawable::GetX() const
{
    return position.GetX();
}

void SelfDrawable::SetX(int newX)
{
    position.SetX(newX);
}

int SelfDrawable::GetWidth() const
{
    return Drawable::GetWidth();
}

int SelfDrawable::GetHeight() const
{
    return Drawable::GetHeight();
}

int SelfDrawable::GetY() const
{
    return position.GetY();
}

void SelfDrawable::SetY(int newY)
{
    position.SetY(newY);
}

Math::Vector2F SelfDrawable::GetPosition() const
{
    return position;
}

void SelfDrawable::SetPosition(const Math::Vector2F& position)
{
    this->position = position;
}

void SelfDrawable::Draw(sdl::GraphicsCore* gCore, Camera* camera)
{
    if(m_display != nullptr)
    {
        gCore->Render(*m_display->GetRawData(),m_clip,m_color,
                      Math::Vector2F(GetX() - camera->GetRect()->GetLeft(), GetY() - camera->GetRect()->GetTop()),
                      m_scale,m_angle,m_flip);
    }
}

void SelfDrawable::SetAlwaysVisible(bool alwaysVisible)
{
    this->alwaysVisible = alwaysVisible;
}

bool SelfDrawable::IsAlwaysVisible() const
{
    return alwaysVisible;
}

}
