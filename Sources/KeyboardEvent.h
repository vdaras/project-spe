/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef KEYBOARD_EVENT_H_
#define KEYBOARD_EVENT_H_

#include "Event.h"

#include <SDL/SDL.h>

namespace spe
{

typedef SDLKey Key;
typedef SDLMod KeyMod;

enum KeyboardEventType
{
    KEY_PRESSED,
    KEY_RELEASED,
    KEYBOARD_EVENT_NONE
};

class KeyboardEvent : public Event
{

private:
    KeyboardEventType m_evtType;

    Key m_key;

    KeyMod m_modifier;

    Uint16 m_unicode;

public:

    /**
     * Initializes the KeyboardEvent.
     *
     * @param type: type of the keyboard event.
     * @param key: key that triggered the event.
     * @param mod: modifiers applied when the key triggered the event.
     * @param unicode: unicode value of the key pressed.
     */

    KeyboardEvent(KeyboardEventType type, Key key, KeyMod mod, Uint16 unicode);


    ~KeyboardEvent();


    /**
     * Returns the type of the event.
     *
     * @retval KEY_PRESSED: a key press triggered the Keyboard Event.
     * @retval KEY_RELEASED: a key that was released triggered the Keyboard Event.
     */

    KeyboardEventType GetType() const;


    /**
     * Returns the key that triggered the Keyboard Event.
     *
     * @return the SDLkey that was prepssed or released.
     */

    SDLKey GetKey() const;


    /**
     * Returns the modifiers applied flag.
     *
     * @return a flag indicating the modifiers applied to the key that triggered
     *         the Keyboard Event.
     *         The flag is a bitwise or of these values:
     *         KMOD_NONE
     * 	       KMOD_LSHIFT
     * 	       KMOD_RSHIFT
     *         KMOD_LCTRL
     * 	       KMOD_RCTRL
     *         KMOD_LALT
     *         KMOD_RALT
     *         KMOD_LMETA
     *         KMOD_RMETA
     *         KMOD_NUM
     *         KMOD_CAPS
     *         KMOD_MODE
     *         KMOD_CTRL
     *         KMOD_SHIFT
     *         KMOD_ALT
     *         KMOD_META
     */

    KeyMod GetModifiers() const;


    /**
     * @return unicode value of the key that triggered this event.
     */

    Uint16 GetUnicode() const;
};

};

#endif