/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef STATESTACK_H
#define	STATESTACK_H

#include "NonCopyable.h"

#include <stack>

namespace spe
{

class State;

class StateStack : public NonCopyable
{
private:
    std::stack< State* > m_states;

    bool m_mustPop;

public:
    StateStack();

    ~StateStack();

    /**
     * Pushes a State to the stack.
     *
     * @param toPush
     */

    void Push(State* toPush);


    /**
     * Pops top State from the Stack.
     */

    void Pop();


    /**
     * Notify the StateStack that the top state must be popped when it's appropriate.
     */

    void SafePop();


    /**
     * Returns if the Stack should pop its top or not.
     */

    inline bool MustPop()
    {
        return m_mustPop;
    }


    /**
     * @return a pointer to the State at the top of the Stack.
     */

    State* Top() const;


    /**
     * @return Count of States in stack.
     */

    unsigned int Size() const;
};

};

#endif	/* STATESTACK_H */

