/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ReferenceCounted.h"

/**
 * Constructor initializes references to 1.
 */

namespace spe
{
	ReferenceCounted::ReferenceCounted()
		:
		m_refCount(1)
	{
	}

	ReferenceCounted::~ReferenceCounted()
	{
	}

	/**
	 * Increases references to this object.
	 */
	void ReferenceCounted::IncRef()
	{
		++m_refCount;
	}

	/**
	 * Decreases references to this object. If the reference count reaches zero
	 * frees the object.
	 */
	void ReferenceCounted::DecRef()
	{
		--m_refCount;
	}
}