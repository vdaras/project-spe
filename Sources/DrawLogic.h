/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef DRAWLOGIC_H
#define	DRAWLOGIC_H

#include "AbstractLogic.h"
#include "Drawable.h"

namespace spe
{
	namespace EntitySystem
	{
		/**
		 * This class handles all sprite drawing logic needed by Entity.
		 */
		class DrawLogic : public Drawable, public AbstractLogic
		{
		public:
			DrawLogic();
			DrawLogic(const DrawLogic& orig);
			virtual ~DrawLogic();
			virtual DrawLogic* Clone();
			//In order this class stays compatible with the old Drawable hierarchy we needed
			//these virtual methods.

			virtual bool IsVisible(const Camera& camera) const;
			virtual void Draw(sdl::GraphicsCore* gCore, Camera* camera);
			Math::Vector2I GetDisplayDimensions();

			virtual void SetPosition(const Math::Vector2F& position);
			virtual Math::Vector2F GetPosition() const;

			virtual int GetX() const;
			virtual void SetX(int newX);
			virtual int GetY() const;
			virtual void SetY(int newY);
			virtual int GetWidth() const;
			virtual int GetHeight() const;
		};
	};
};

#endif	/* DRAWLOGIC_H */
