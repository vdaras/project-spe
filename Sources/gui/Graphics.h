/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GRAPHICS_H
#define	GRAPHICS_H

namespace spe
{

namespace sdl
{
    class Color;
    class Surface;
};

namespace gfx
{
    /**
     * Uses the Bresenham's line drawing algorithm. Don't ask for comments, I don't know what it does I just coded the
     * algorithm. See http://en.wikipedia.org/wiki/Bresenham's_line_algorithm for further explanation.
     *
     * @param target: the surface which the line will be drawn on
     * @param x0: x coordinate of the starting point
     * @param y0: y coordinate of the starting point
     * @param x1: x coordinate of the ending point
     * @param y1: y coordinate of the ending point
     * @param color: the color of the line
     * @param thickness: thickness of the line (NOT implemented yet)
     */

    void drawLine(sdl::Surface* target, int x0, int y0, int x1, int y1, const sdl::Color& color, float thickness = 1.0f);


    /**
     * This routine draws a non-filled rectangle on the target surface.
     *
     * @param
     * @paramtarget: the target surface
     * @param upperX: the x coordinate of the rectangle's upper left corner
     * @param upperY: the y coordinate of the rectangle's upper left corner
     * @param width: width of the rectangle
     * @param height: height of the rectangle
     * @param color: color of the rectangle
     */

    void drawRect(sdl::Surface* target, int upperX, int upperY, int width, int height, const sdl::Color& color);
};

};

#endif	/* GRAPHICS_H */

