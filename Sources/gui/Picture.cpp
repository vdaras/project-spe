/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "../sdl/SDLException.h"
#include "Picture.h"
#include "GuiException.h"
#include "../ResourceManager.h"

namespace spe
{

    namespace gui
    {

        /**
         * Constructor initializes the Component part of the Picture and opens an image
         * located @ path 'picPath'. Then resizes the Component in order to fit the
         * image's size.
         *
         * @param picPath: path of the image on disc
         */

        Picture::Picture(const std::string& name, const std::string& picPath)
            :
            Component(name, 0, 0)
        {
            //try opening an image
            try
            {
                m_picRes = ResourceManager::GetInstance()->GetImage(picPath.c_str());

                m_pic = m_picRes->GetRawData();
            }
            catch(spe::sdl::SdlException& exc)
            {
                //an error occurred
                throw spe::gui::GuiException("Cannot create Picture! Image probably doesn't exist!");
            }

            int imageWidth = m_pic->GetWidth();
            int imageHeight = m_pic->GetHeight();

            m_visiblePart.w = imageWidth;
            m_visiblePart.h = imageHeight;

            //set Picture's size
            SetWidth(imageWidth);
            SetHeight(imageHeight);

            UpdateVisiblePart();
        }


        /**
         * Constructor initializes the Component part of the Picture and opens an image
         * located @ path 'picPath'. Then resizes the Component in order to fit the
         * image's size.
         *
         * @param picPath: path of the image on disc
         */

        Picture::Picture(const std::string& name, Container* parent, int x, int y, const std::string& picPath)
            :
            Component(name, parent, x, y, 0, 0)
        {
            //try opening an image
            try
            {
                m_picRes = ResourceManager::GetInstance()->GetImage(picPath.c_str());

                m_pic = m_picRes->GetRawData();
            }
            catch(spe::sdl::SdlException& exc)
            {
                //an error occurred
                throw spe::gui::GuiException("Cannot create Picture! Image probably doesn't exist!");
            }

            int imageWidth = m_pic->GetWidth();
            int imageHeight = m_pic->GetHeight();

            m_visiblePart.w = imageWidth;
            m_visiblePart.h = imageHeight;

            //set Picture's size
            SetWidth(imageWidth);
            SetHeight(imageHeight);

            UpdateVisiblePart();
        }


        Picture::~Picture()
        {
            m_picRes->DecreaseReference();
        }


        /**
         * Resizes this Label.
         *
         * @param width: new width.
         * @param height: new height.
         */

        void Picture::Resize(int width, int height)
        {

        }



        /**
         * Draws the picture on a surface.
         *
         * @param target: target surface.
         */

        void Picture::Draw(sdl::Surface* target) const
        {
            m_pic->Blit(GetX(), GetY(), target, &m_visiblePart);
        }


        /**
         * Updates the picture
         */

        void Picture::Update()
        {
        }

    };

};
