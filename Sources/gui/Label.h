/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef LABEL_H
#define	LABEL_H

#include "Component.h"

#include <string>
#include <memory>

#include "../sdl/Canvas.h"
#include "../sdl/Color.h"
#include "../sdl/Font.h"
#include "../Resource.h"

namespace spe
{

    namespace gui
    {


        /**
         * Class encapsulating the functionality of a label user control. Derives from
         * Component, making the objects of this class leaves of the Composite tree.
         */

        class Label : public Component
        {
        private:
            std::string m_text;
            sdl::Color m_txtColor;
            Resource<sdl::Font> *m_fontRes;
            sdl::Font* m_font;
            std::unique_ptr< sdl::Canvas > m_textDisplay;

        public:

            /**
             * Constructor initializes the Component part of the Label and the font which
             * the label uses to render its' text. After member initialization the constructor
             * allocates a surface and creates the display part of the Label.
             *
             * @param name: name of the Label Component
             * @param w: width of the Label
             * @param h: height of the Label
             * @param text: Label's text
             * @param txtColor: Label's text color
             * @param fnt: font used by the Label to render its' text, not owned by the Label object.
             */

            Label(const std::string& name, int w, int h, const std::string& text, const sdl::Color& txtColor, Resource<sdl::Font> *fnt);


            /**
             * Constructor initializes the Component part of the Label and the font, text and text color
             * which the label uses to render itself. After member initialization the constructor
             * allocates a surface and creates the display part of the Label. It also adds the
             * Label to the passed Container @x,y position.
             *
             * @param name: name of the Label Component
             * @param w: width of the Label
             * @param h: height of the Label
             * @param text: Label's text
             * @param txtColor: Label's text color
             * @param fnt: font used by the Label to render its' text, not owned by the Label object.
             */

            Label(const std::string& name, Container* parent, int x, int y, int w, int h, const std::string& text, const sdl::Color& txtColor, Resource<sdl::Font> *fnt);


            virtual ~Label();


            /**
             * Resizes this label.
             *
             * @param width: new width.
             * @param height: new height.
             */

            virtual void Resize(int width, int height);


            /**
             * Sets the text of the label.
             *
             * @param text: the new text
             */

            void SetText(const std::string& text);


            /**
             * Sets the color of the label's text.
             *
             * @param color: the new color
             */

            void SetTextColor(const sdl::Color& txtColor);


            /**
             * Sets the font of the label's text. The font is not owned by the Label and
             * it's client code's responsibility to manage it's memory.
             *
             * @param fnt: the new font
             */

            void SetFont(Resource<sdl::Font> *fnt);


            /**
             * Draws the label on a surface.
             *
             * @param target: the target surface.
             */

            virtual void Draw(sdl::Surface* target) const;


            /**
             * Updates the label.
             */

            virtual void Update();

        private:


            /**
             * This routine renders the graphical part of the Label using the text and color
             * members. Returns failure if an error occurs during rendering.
             *
             * @return a boolean value indicating if text rendering was successful or not.
             */

            bool RenderText();
        };

    };

};

#endif	/* LABEL_H */

