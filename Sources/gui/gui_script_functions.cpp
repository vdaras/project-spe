/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "gui_script_functions.h"
#include "../ServiceLocator.h"
#include "../StateStack.h"
#include "../State.h"

namespace spe
{
	namespace gui
	{
		Button* CreateButton(Container* parent, const std::string& name, int x, int y, int width, int height, const std::string& caption, Resource<sdl::Font> *fnt)
		{
			Button* newButton = new Button(name, width, height, caption, fnt);

			parent->AddComponent(newButton, x, y);

			StateStack* stateStack = ServiceLocator< StateStack >::GetService();

			newButton->AddActionListener(stateStack->Top());

			return newButton;
		}

		Picture* CreatePicture(Container* parent, const std::string& name, int x, int y, const std::string& path)
		{
			Picture* pic = new Picture(name, path);

			parent->AddComponent(pic, x, y);

			return pic;
		}

		Label* CreateLabel(Container* parent, const std::string& name, const std::string& txt, int x, int y, int width, int height, Resource<sdl::Font> *fnt)
		{
			Label* lbl = new Label(name, width, height, txt, sdl::Colors::BLACK, fnt);

			parent->AddComponent(lbl, x, y);

			return lbl;
		}
	};
};