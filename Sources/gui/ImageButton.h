/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef IMAGEBUTTON_H
#define	IMAGEBUTTON_H

#include "AbstractButton.h"

#include "../sdl/Image.h"

#include <memory>

namespace spe
{

    namespace gui
    {


        /**
         * Button using pre-rendered graphics for it's display.
         */

        class ImageButton : public AbstractButton
        {
        private:
            std::unique_ptr< sdl::Image > m_images[3];

        public:

            /**
             * Constructor initializes the Abstract Button parent class and then opens the desired
             * images for the display part of this Image Button. If any of the image's could not
             * be opened an exception is thrown.
             *
             * @param name: the identifier of this Component
             * @param regImage: path of the regular display of this button
             * @param highlightImage: path of the display of this button when it's highlighted
             * @param clickedImage: path of the display of this button when it's clicked
             */

            ImageButton(const std::string& name, const std::string& regImage, const std::string& highlightImage, const std::string& clickedImage);

            /**
             * Constructor initializes the Abstract Button parent class, adds the Image Button to
             * the specified container @x,y and then opens the desired images for the display part of this Image Button.
             * If any of the image's could not be opened an exception is thrown.
             *
             * @param name: the identifier of this Component
             * @param parent: this Component's container
             * @param x,y : position of the Component inside its' Container.
             * @param regImage: path of the regular display of this button
             * @param highlightImage: path of the display of this button when it's highlighted
             * @param clickedImage: path of the display of this button when it's clicked
             */

            ImageButton(const std::string& name, Container* parent, int x, int y, const std::string& regImage, const std::string& highlightImage, const std::string& clickedImage);


            virtual ~ImageButton();


            /**
             * Resizes this ImageButton.
             *
             * @param width: new width.
             * @param height: new height.
             */

            virtual void Resize(int width, int height);


            /**
             * Changes the regular display of the Image Button to the image located @
             * path.
             *
             * @param path: image on disk.
             */

            void SetRegularImage(const std::string& path);


            /**
             * Changes the highlighted display of the Image Button to the image located @
             * path.
             *
             * @param path: image on disk
             */

            void SetHighlightImage(const std::string& path);


            /**
             * Changes the clicked display of the Image Button to the image located @
             * path.
             *
             * @param path: image on disk
             */

            void SetClickedImage(const std::string& path);


            /**
             * Draws the Image Button on a surface.
             *
             * @param target: the target surface.
             */

            virtual void Draw(sdl::Surface* target) const;


            /**
             * Updates the Image Button.
             */

            virtual void Update();
        };

    };

};

#endif	/* IMAGEBUTTON_H */

