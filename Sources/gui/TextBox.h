/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef TEXTBOX_H
#define	TEXTBOX_H

#include "Component.h"

#include "../sdl/Canvas.h"
#include "../sdl/Font.h"
#include "../sdl/TextImage.h"
#include "Text.h"

namespace spe
{

namespace gui
{

    /*
     * This type of Component is used to get input from the user.
     */
    class TextBox : public Component
    {
    private:
        bool m_scrollingEnabled;

        sdl::Font* m_font;

        std::unique_ptr< sdl::Canvas > m_background;

        std::unique_ptr< Text > m_text;

    public:

        TextBox(const std::string& name, int w, int h, sdl::Font* fnt);


        TextBox(const std::string& name, Container* parent, int x, int y, int w, int h, sdl::Font* fnt);


        virtual ~TextBox();


        /**
         * Resizes this TextBox.
         *
         * @param width: new width.
         * @param height: new height.
         */

        virtual void Resize(int width, int height);


        /**
         * Returns the Text Box's current text.
         *
         * @return a string containing text entered.
         */

        std::string GetText() const;


        /**
         * Enables scrolling for this textbox.
         */

        void EnableScrolling();


        /**
         * Disables scrolling for this textbox.
         */

        void DisableScrolling();


        /**
         * Draws the Text Box on the target Surface.
         *
         * @param target: target Surface.
         */

        virtual void Draw(sdl::Surface* target) const;


        /**
         * Updates the Text Box.
         */

        virtual void Update();


        /**
         * This routine handles mouse input from the user.
         *
         * @param event: the mouse event
         */

        virtual void OnMouseEvent(MouseEvent& event);


        /**
         * This routine handles keyboard input from the user
         */

        virtual void OnKeyboardEvent(KeyboardEvent& event);

    private:

        /**
         * Based on mouse coordinates, returns whether the Text Box was clicked or not.
         *
         * @param mouseX: mouse's x coordinate
         * @param mouseY: mouse's y coordinate
         */

        bool IsClicked(int mouseX, int mouseY);
    };

};
};

#endif	/* TEXTBOX_H */

