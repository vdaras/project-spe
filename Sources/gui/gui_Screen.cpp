/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "gui_Screen.h"

#include "../MouseEvent.h"
#include "../Utilities.h"
#include <algorithm>

namespace spe
{

    namespace gui
    {

        /**
         * Default constructor initializes the actual screen pointer.
         *
         * @param screen: the game's screen
         */

        Screen::Screen(sdl::Canvas* screen)
            :
            Container("Screen", 0, 0, screen->GetWidth(), screen->GetHeight()),
            m_gameScreen(screen)
        {
            SDL_EnableUNICODE(SDL_ENABLE);
            //SDL_EnableKeyRepeat(500, 30);

            m_focusServer = new FocusServer;
        }



        Screen::~Screen()
        {
            SDL_EnableUNICODE(SDL_DISABLE);

            delete m_focusServer;
        }


        /**
         * Screen container should not be resized by any mean.
         *
         * @param width:
         * @param height:
         */

        void Screen::Resize(int width, int height)
        {
            //cannot resize screen container, leave empty.
        }


        /**
         * Draws the gui's contents on the game's screen.
         */

        void Screen::Draw() const
        {
            Container::Draw(m_gameScreen);
        }


        /**
         * Forwards the produced mouse event to the focus server.
         *
         * @param event: a mouse event occurred.
         */

        void Screen::OnMouseEvent(MouseEvent& event)
        {
            //if the event is a mouse movement then all the composite tree of the gui must be made aware of it.
            if(event.GetType() == MOUSE_MOTION)
            {
                Container::OnMouseEvent(event);
            }
            else if(event.GetType() == MOUSE_CLICKED && event.GetButtonUsed() == MBTN_LEFT)
            {
                Component* focused = m_focusServer->ResolveMouseFocus(event, this);

                if(focused)
                    focused->OnMouseEvent(event);
            }
            else if(event.GetType() == MOUSE_RELEASED && event.GetButtonUsed() == MBTN_LEFT)
            {
                Component* lastFocused = m_focusServer->GetLastFocused();

                if(lastFocused)
                    lastFocused->OnMouseEvent(event);
            }
        }


    };

};
