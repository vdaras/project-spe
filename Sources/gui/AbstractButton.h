/**
 * Project SPE
 *
 * Copyright (C) 2011-2013 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ABSTRACTBUTTON_H
#define	ABSTRACTBUTTON_H

#include "Component.h"

//standard library includes
#include <list>

//other includes

//project includes
#include "ActionListener.h"

class MouseEvent;

namespace spe
{

    namespace gui
    {

        /**
         * Class providing an interface and common functionality for several
         * types of buttons.
         */
        class AbstractButton : public Component
        {
        protected:

            const static int REGULAR = 0;
            const static int HIGHLIGHTED = 1;
            const static int CLICKED = 2;

            int m_currentDisplay;

            bool m_clicked;

        public:

            /**
             * Constructor.
             *
             * @param name
             *        Component's id
             *
             * @param width
             *        Width of the button
             *
             * @param height
             *        Hheight of button
             */
            AbstractButton(const std::string& name, int width, int height);


            /**
             * Constructor that initializes the Abstract Button's id, width , height, parent
             * container and its position inside the container.
             *
             * @param name
             *        Component's id
             *
             * @param parent
             *        Parent container of this component
             *
             * @param x
             *        X position inside the container
             *
             * @param y
             *        Y position inside the container
             *
             * @param width
             *        Width of button
             *
             * @param height
             *        Height of button
             */
            AbstractButton(const std::string& name, Container* parent, int x, int y, int width, int height);

            /**
             * Destructor.
             */
            virtual ~AbstractButton() = 0;


            /**
             * Handles a mouse event.
             *
             * @param event
             *        The mouse event to handle.
             */
            virtual void OnMouseEvent(MouseEvent& event);

        protected:

            /**
             * This routine returns whether the mouse's cursor is inside the
             * area of the Button.
             *
             * @param mouseX
             *        X coordinate of the mouse cursor
             *
             * @param mouseY
             *        Y coordinate of the mouse cursor
             *
             * @return
             *         Whether the mouse's cursor is inside this button or not.
             */
            bool CursorInArea(int mouseX, int mouseY);


            /**
             * This routine notifies each Action Listener that an action was performed to this
             * Button.
             */
            void FireActionEvent();
        };

    };

};
#endif	/* ABSTRACTBUTTON_H */

