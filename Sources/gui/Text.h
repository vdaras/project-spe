/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef TEXT_H
#define	TEXT_H

#include <vector>

#include "../sdl/Font.h"

namespace spe
{

    namespace gui
    {


        /**
         * Class encapsulating operations done on text.
         */

        class Text
        {
        protected:
            std::vector< std::string > m_rows;
            unsigned m_cursorRow, m_cursorColumn;

        public:
            Text();

            Text(const std::string& text);

            virtual ~Text();


            /**
             * This routine inserts a character after the cursor.
             *
             * @param c: the character to insert.
             * @param times: how many times to insert
             */

            void InsertCharacter(char c, int times = 1);


            /**
             * This routine deletes characters after or before the cursor. If a negative
             * value is supplied as an offset then it removes 'abs(offset)' characters BEFORE the cursor.
             * If the supplied offset is positive, then it removes 'offset' characters AFTER the cursor.
             *
             * @param offset: offset from cursor of the first character to delete
             */

            void DeleteCharacters(int offset);


            /**
             * Returns text from the row where the cursor is located currently.
             *
             * @return text
             */

            virtual std::string GetCurrentRow() const;


            /**
             * Set's cursor position in the text.
             *
             * @param row: the desired row for the cursor
             * @param column: the desired column for the cursor
             */

            void SetCursorPosition(unsigned row, unsigned column);


            /**
             * This routine take a 2D point and a font in order to translate it to
             * a cursor position inside the text.
             *
             * @param pointX: point's X coordinate
             * @param pointY: point's Y coordinate
             * @param font: the font to use in order to calculate text size
             */

            virtual void SetCursorFromPoint(int pointX, int pointY, sdl::Font* fnt);


            /**
             * Returns current row of the cursor.
             *
             * @return cursor's row.
             */

            unsigned GetCursorRow() const;


            /**
             * Returns current column of the cursor.
             *
             * @return cursor's column.
             */

            unsigned GetCursorColumn() const;


            /**
             * This method moves the cursor to the right. The cursor's current column cannot be
             * higher than the length of the string.
             */

            virtual void MoveCursorRight();


            /**
             * This method moves the cursor to the left. The cursor's current column cannot be lower
             * than zero.
             */

            virtual void MoveCursorLeft();


            /**
             * This method moves the cursor upwards. The cursor's current row cannot be lower
             * than zero.
             */

            virtual void MoveCursorUp();


            /**
             * This method moves the cursor downwards. The cursor's current row cannot be greater
             * than the total rows of the text.
             */

            virtual void MoveCursorDown();


            /**
             * Returns x coordinate of the cursor, starting from 0.
             *
             * @param font: font used to calculate text dimension.
             * @return cursor x
             */

            virtual int GetCursorX(sdl::Font* font) const;


            /**
             * Returns y coordinate of the cursor, starting from 0.
             *
             * @param font: font used to calculate text height
             * @return cursor's y
             */

            virtual int GetCursorY(sdl::Font* font) const;


            /**
             * This routine returns the contents of the Text.
             *
             * @param context of text in a string.
             */

            std::string GetContents() const;

        };

    };

};

#endif	/* TEXT_H */

