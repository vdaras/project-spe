/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PICTURE_H
#define	PICTURE_H

#include "Component.h"

#include "../sdl/Image.h"
#include "../Resource.h"

#include <memory>

namespace spe
{

namespace gui
{


    /**
     * This class represents a static picture on a user interface.
     */

    class Picture : public Component
    {
    private:
        Resource< sdl::Image >* m_picRes;
        sdl::Image* m_pic;


    public:

        /**
         * Constrcutor initializes the Component part of the Picture and opens an image
         * located @ path 'picPath'. Then resizes the Component in order to fit the
         * image's size.
         *
         * @param picPath: path of the image on disc
         */

        Picture(const std::string& name, const std::string& picPath);


        /**
         * Constrcutor initializes the Component part of the Picture and opens an image
         * located @ path 'picPath'. Then resizes the Component in order to fit the
         * image's size.
         *
         * @param picPath: path of the image on disc
         */

        Picture(const std::string& name, Container* parent, int x, int y, const std::string& picPath);


        virtual ~Picture();


        /**
         * Resizes this Label.
         *
         * @param width: new width.
         * @param height: new height.
         */

        virtual void Resize(int width, int height);


        /**
         * Draws the picture on a surface.
         *
         * @param target: target surface.
         */

        virtual void Draw(sdl::Surface* target) const;


        /**
         * Updates the picture
         */

        virtual void Update();
    };

};

};

#endif	/* PICTURE_H */

