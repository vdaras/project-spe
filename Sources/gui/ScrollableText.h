/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef SCROLLABLETEXT_H
#define	SCROLLABLETEXT_H

#include "Text.h"

namespace spe
{

namespace gui
{
    class ScrollableText : public Text
    {
    private:
        unsigned m_maxViewableCharacters;
        //low index is the first character of the current row that's currently viewable
        //high index is the last character of the current row that's currently viewable
        unsigned m_lowIndex, m_highIndex;

    public:
        ScrollableText();

        ScrollableText(const std::string& text);

        ~ScrollableText();


        /**
         * Returns viewable part of the current row.
         *
         * @param a string containing the viewble text of the current row.
         */

        std::string getViewableRow() const;


        /**
         * This method moves the cursor right. The extra functionality, besides the plain
         * Text's, is that if the cursor is pointing to a character higher than the high
         * index (maximum viewable index), then the low and high indices are moved one
         * spot to the right
         */

        virtual void MoveCursorRight();


        /**
         * This method moves the cursor left. The extra functionality, besides the plain
         * Text's, is that if the cursor is pointing to a character lower than the low
         * index (maximum viewable index), then the low and high indices are moved one
         * spot to the right
         */

        virtual void MoveCursorLeft();


        /**
         * This routine maps the cursor's X based on the scrolled text.
         *
         * @param fnt: font used to measure sizes
         * @return cursor's x position on screen.
         */

        virtual int GetCursorX(sdl::Font* fnt) const;


        /**
         * This routine translates a point to cursor indices in text. The problem with
         * scrollable text is that these indices are calculated differently than regular
         * text since these coordinates must be translated depending on the viewed area
         * of the text.
         *
         * @param fnt: font used to measure sizes
         */

        virtual void SetCursorFromPoint(int pointX, int pointY, sdl::Font* fnt);


        /**
         * Increases the high index of the scrollable text.
         */

        void IncreaseHigh();


        /**
         * Descreases the high index of the scrollable text.
         */

        void DecreaseHigh();


        /*
         * Increases the low index of the scrollable text.
         */

        void IncreaseLow();


        /**
         * Decrease the low index of the scrollable text.
         */

        void DecreaseLow();

    };

};

};

#endif	/* SCROLLABLETEXT_H */

