/**
 * Project SPE
 *
 * Copyright (C) 2011-2013 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ACTIONLISTENER_H
#define	ACTIONLISTENER_H

namespace spe
{

    namespace gui
    {

        class Component;

        /**
         * Interface that should be implemented by any class that wishes to listen
         * for events from a gui Component. IMPORTANT, if an Action Listener is destroyed
         * then it has to detach itself from each Component it's listening to!
         */

        class ActionListener
        {

        public:

            /**
             * Constructor.
             */
            ActionListener();

            /**
             * Destructpor.
             */
            virtual ~ActionListener();

            /**
             * This function is called when an event is fired from a
             * a component this listener is listening to.
             *
             * @param sender
             */
            virtual void ActionPerformed(Component* sender) = 0;
        };

    };

};

#endif	/* ACTIONLISTENER_H */

