/**
 * Project SPE
 *
 * Copyright (C) 2011-2013 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef BUTTON_H
#define	BUTTON_H

#include "AbstractButton.h"

//standard library includes
#include <memory>

//other includes

//project includes
#include "../sdl/Canvas.h"
#include "../sdl/Color.h"
#include "../sdl/Font.h"
#include "../Resource.h"


namespace spe
{

    namespace gui
    {

        /**
         * Simple button rendered using SDL Canvases for it's display part.
         */

        class Button : public AbstractButton
        {
            const static int BACKGROUND = 0;
            const static int FOREGROUND = 1;

            std::string m_caption;

            sdl::Color m_colors[3][2];

            std::unique_ptr<sdl::Canvas> m_displays[3];

            Resource<sdl::Font> *m_fontRes;

            sdl::Font* m_font;

        public:

            /**
             * Constructor. (<- no shit Sherlock!)
             *
             * @param name
             *        Id of the Button Component
             *
             * @param width
             *        Width of the Button Component
             *
             * @param height
             *        Height of the Button Component
             *
             * @param caption
             *        The caption of the button
             *
             * @param fnt
             *        Font used to render the Button's caption
             */
            Button(const std::string& name, int width, int height, const std::string& caption, Resource<sdl::Font>* fnt);


            /**
             * Constructor.
             *
             * @param name
             *        Id of the Button Component
             *
             * @param parent
             *        Container containing this Button
             *
             * @param x
             *        X position of the Button in the parent Container
             *
             * @param y
             *        Y position of the Button in the parent Container
             *
             * @param width
             *        Width of the Button Component
             *
             * @param height
             *         Height of the Button Component
             *
             * @param caption
             *        The caption of the button
             *
             * @param fnt
             *        Font used to render the Button's caption
             */
            Button(const std::string& name, Container* parent, int x, int y, int width, int height, const std::string& caption, Resource<sdl::Font>* fnt);


            /**
             * Destructor.
             */
            virtual ~Button();


            /**
             * Resizes this Button.
             *
             * @param width
             *        New width.
             *
             * @param height
             *        New height.
             */

            virtual void Resize(int width, int height);


            /**
             * Sets the Button's caption.
             *
             * @param caption
             *        New caption to set.
             */
            void SetCaption(const std::string& caption);


            /**
             * Sets the background color of the button.
             *
             * @param color
             *        New color to set
             */
            void SetBackgroundColor(const sdl::Color& color);


            /**
             * Sets the foreground color of the button.
             *
             * @param color
             *        New color to set
             */
            void SetForegroundColor(const sdl::Color& color);


            /**
             * Sets the background color of the button when it's highlighted.
             *
             * @param color
             *        New color to set
             */
            void SetHighlightBackgroundColor(const sdl::Color& color);


            /**
             * Sets the foreground color of the button when it's highlighted.
             *
             * @param color
             *        New color to set
             */
            void SetHighlightForegroundColor(const sdl::Color& color);


            /**
             * Sets the background color of the button when it's clicked.
             *
             * @param color
             *        New color to set
             */
            void SetClickedBackgroundColor(const sdl::Color& color);


            /**
             * Sets the foreground color of the button when it's clicked.
             *
             * @param color
             *        New color to set
             */
            void SetClickedForegroundColor(const sdl::Color& color);


            /**
             * Renders the button on a surface depending on its state.
             *
             * @param target
             *        Target surface.
             */

            virtual void Draw(sdl::Surface* target) const;


            /**
             * Updates the button.
             */
            virtual void Update();


        private:

            bool CreateDisplays();


            bool Initialize();
        };
    };
};

#endif	/* BUTTON_H */

