/**
 * Project SPE
 *
 * Copyright (C) 2011-2013 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Button.h"

//standard library includes
#include <iostream>

//other includes

//project includes

#include "../MouseEvent.h"
#include "../sdl/SDLException.h"
#include "../sdl/TextImage.h"
#include "../sdl/sdl_utilities.h"
#include "GuiException.h"

#ifdef _MSC_VER
#include <algorithm>
#endif

namespace spe
{

    namespace gui
    {


        Button::Button(const std::string& name, int width, int height, const std::string& caption, Resource<sdl::Font> *fnt)
            :
            AbstractButton(name, width, height),
            m_caption(caption),
            m_fontRes(fnt)
        {
            m_font = m_fontRes->GetRawData();
            //init button, if initialization fails
            if(!Initialize())
            {
                //notify the caller
                throw GuiException("gui::Button cannot be created!!");
            }
        }


        Button::Button(const std::string& name, Container* parent, int x, int y, int width, int height, const std::string& caption, Resource<sdl::Font> *fnt)
            :
            AbstractButton(name, parent, x, y, width, height),
            m_caption(caption),
            m_fontRes(fnt)
        {
            m_font = m_fontRes->GetRawData();
            //init button, if initialization fails
            if(!Initialize())
            {
                //notify the caller
                throw GuiException("gui::Button cannot be created!!");
            }
        }


        Button::~Button()
        {
            m_fontRes->DecreaseReference();
        }


        void Button::Resize(int width, int height)
        {
            int prevWidth = GetWidth();
            int prevHeight = GetHeight();

            SetWidth(width);

            SetHeight(height);

            if(!CreateDisplays())
            {
                SetWidth(prevWidth);
                SetHeight(prevHeight);
            }
        }


        void Button::SetCaption(const std::string& caption)
        {
            //save previous caption
            std::string previousCaption = m_caption;

            //change caption
            m_caption = caption;

            //render displays with the new caption, if rendering fails
            if(!CreateDisplays())
            {
                //revert caption change
                m_caption = previousCaption;
            }
        }


        void Button::SetBackgroundColor(const sdl::Color& color)
        {
            std::unique_ptr<sdl::Canvas> tempDisplay;

            try
            {
                //create a temporary canvas with the desired color
                tempDisplay.reset(new sdl::Canvas(GetWidth(), GetHeight(), color));
                //render caption
                std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, m_colors[REGULAR][FOREGROUND]);
                //blit caption centered on the temporary canvas
                txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplay.get());
            }
            catch(spe::sdl::SdlException& exc)
            {
                //if an error occurrs return
                return;
            }

            //everything went smoothly
            //swap display with the temporary canvas
            std::swap(m_displays[REGULAR], tempDisplay);
            m_colors[REGULAR][BACKGROUND] = color;
        }


        void Button::SetForegroundColor(const sdl::Color& color)
        {
            std::unique_ptr<sdl::Canvas> tempDisplay;

            try
            {
                //create a temporary canvas
                tempDisplay.reset(new sdl::Canvas(GetWidth(), GetHeight(), m_colors[REGULAR][BACKGROUND]));

                //render caption
                std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, color);

                //blit caption centered on the temporary canvas
                txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplay.get());
            }
            catch(spe::sdl::SdlException& exc)
            {
                //if an error occurs return
                return;
            }

            //everything went smoothly
            //swap display with the temporary canvas
            std::swap(m_displays[REGULAR], tempDisplay);
            m_colors[REGULAR][FOREGROUND] = color;
        }


        void Button::SetHighlightBackgroundColor(const sdl::Color& color)
        {
            std::unique_ptr<sdl::Canvas> tempDisplay;

            try
            {
                //create a temporary canvas with the desired color
                tempDisplay.reset(new sdl::Canvas(GetWidth(), GetHeight(), color));

                //render caption
                std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, m_colors[HIGHLIGHTED][FOREGROUND]);

                //blit caption centered on the temporary canvas
                txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplay.get());

            }
            catch(spe::sdl::SdlException& exc)
            {
                //if an error occurrs return
                return;
            }

            //everything went smoothly
            //swap display with the temporary canvas
            std::swap(m_displays[HIGHLIGHTED], tempDisplay);
            m_colors[HIGHLIGHTED][BACKGROUND] = color;
        }


        void Button::SetHighlightForegroundColor(const sdl::Color& color)
        {
            std::unique_ptr<sdl::Canvas> tempDisplay;

            try
            {
                //create a temporary canvas
                tempDisplay.reset(new sdl::Canvas(GetWidth(), GetHeight(), m_colors[HIGHLIGHTED][BACKGROUND]));

                //render caption
                std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, color);

                //blit caption centered on the temporary canvas
                txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplay.get());
            }
            catch(spe::sdl::SdlException& exc)
            {
                //if an error occurs return
                return;
            }

            //swap display with the temporary canvas
            std::swap(m_displays[HIGHLIGHTED], tempDisplay);
            m_colors[HIGHLIGHTED][FOREGROUND] = color;
        }



        void Button::SetClickedBackgroundColor(const sdl::Color& color)
        {
            std::unique_ptr<sdl::Canvas> tempDisplay;

            try
            {
                //create a temporary canvas with the desired color
                tempDisplay.reset(new sdl::Canvas(GetWidth(), GetHeight(), color));

                //render caption
                std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, m_colors[CLICKED][FOREGROUND]);

                //blit caption centered on the temporary canvas
                txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplay.get());

                //swap display with the temporary canvas

            }
            catch(spe::sdl::SdlException& exc)
            {
                //if an error occurrs return
                return;
            }

            //swap display with the temporary canvas
            std::swap(m_displays[CLICKED], tempDisplay);
            m_colors[CLICKED][BACKGROUND] = color;
        }


        void Button::SetClickedForegroundColor(const sdl::Color& color)
        {
            std::unique_ptr<sdl::Canvas> tempDisplay;

            try
            {
                //create a temporary canvas
                tempDisplay.reset(new sdl::Canvas(GetWidth(), GetHeight(), m_colors[CLICKED][BACKGROUND]));
                //render caption
                std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, color);
                //blit caption centered on the temporary canvas
                txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplay.get());
            }
            catch(spe::sdl::SdlException& exc)
            {
                //if an error occurrs return
                return;
            }

            //swap display with the temporary canvas
            std::swap(m_displays[CLICKED], tempDisplay);
            m_colors[CLICKED][FOREGROUND] = color;
        }


        void Button::Draw(sdl::Surface* target) const
        {
            m_displays[m_currentDisplay]->Blit(GetX(), GetY(), target, &m_visiblePart);
        }


        void Button::Update()
        {
        }


        bool Button::CreateDisplays()
        {
            std::unique_ptr<sdl::Canvas> tempDisplays[3];

            try
            {
                //for each type of display (REGULAR, HIGHLIGHTED, CLICKED)
                for(int displayType = REGULAR; displayType <= CLICKED; ++displayType)
                {
                    //allocate a new temporary canvas with size the same as the button and same color
                    //as the background
                    tempDisplays[displayType].reset(new sdl::Canvas(GetWidth(), GetHeight(), m_colors[displayType][BACKGROUND]));
                    //render caption
                    std::unique_ptr<sdl::TextImage> txt = m_font->GetImage(m_caption, m_colors[displayType][FOREGROUND]);
                    //blit caption on temporary canvas
                    txt->Blit((GetWidth() / 2) - (txt->GetWidth() / 2), (GetHeight() / 2) - (txt->GetHeight() / 2), tempDisplays[displayType].get());
                }
            }
            catch(spe::sdl::SdlException& exc)
            {
                return false;
            }

            //since all rendering was successful, swap displays to the temporary displays
            for(int displayType = REGULAR; displayType <= CLICKED; ++displayType)
            {
                std::swap(m_displays[displayType], tempDisplays[displayType]);
            }

            return true;
        }


        bool Button::Initialize()
        {
            //set the button's height the highest value between the desired height and the font's height
            SetHeight(std::max(m_font->GetHeight(), GetHeight()));

            //get the size of the caption in pixels
            int textWidth, textHeight;
            m_font->TextSize(m_caption, textWidth, textHeight);
            //set the button's width the highest value between the desired width and the text's width
            SetWidth(std::max(textWidth, GetWidth()));


            //initialize default colors
            m_colors[REGULAR][BACKGROUND] = sdl::Colors::GRAY;
            m_colors[REGULAR][FOREGROUND] = sdl::Colors::BLACK;
            m_colors[HIGHLIGHTED][BACKGROUND] = sdl::Colors::BLACK;
            m_colors[HIGHLIGHTED][FOREGROUND] = sdl::Colors::GRAY;
            m_colors[CLICKED][BACKGROUND] = sdl::Colors::BLACK;
            m_colors[CLICKED][FOREGROUND] = sdl::Colors::WHITE;

            m_clicked = false;

            //create displays, if something fails
            if(!CreateDisplays())
            {
                //return failure
                return false;
            }

            //return success
            return true;
        }

    };

};
