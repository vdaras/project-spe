/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "FocusServer.h"

#include "../MouseEvent.h"
#include "Container.h"

namespace spe
{

    namespace gui
    {

        FocusServer::FocusServer()
            :
            m_lastFocused(nullptr)
        {
        }


        FocusServer::~FocusServer()
        {

        }



        /**
         * Resolves which component is focused after a mouse event and returns it.
         *
         * @param event: the mouse event occurred.
         * @param root: root container that we want to check its structure for focus.
         * @return a handle to the focused component.
         */

        Component* FocusServer::ResolveMouseFocus(const MouseEvent& event, gui::Container* root)
        {
            int mouseX, mouseY;

            event.GetOccurredCoordinates(&mouseX, &mouseY);

            std::list< Component* >::iterator iter(root->m_components.begin()), end(root->m_components.end());

            //for each component of the root containe
            for(; iter != end;)
            {
                //check whether the component needs focus or not
                if((*iter)->NeedsFocus(mouseX, mouseY))
                {
                    //if the component is a container
                    Container* container = (*iter)->GetContainer();

                    if(container != nullptr)
                    {
                        //traverse the container's children
                        iter = container->m_components.begin();

                        end = container->m_components.end();
                    }
                    else
                    {
                        Component* focused = (*iter);

                        if(m_lastFocused != focused)
                        {
                            if(m_lastFocused)
                                m_lastFocused->Unfocus();

                            focused->Focus();

                            m_lastFocused = focused;
                        }

                        return focused;
                    }
                }
                else
                {
                    ++iter;
                }
            }

            return nullptr;
        }


        /**
         * @return a handle to the component that was focused last.
         */

        Component* FocusServer::GetLastFocused()
        {
            return m_lastFocused;
        }


    };

};
