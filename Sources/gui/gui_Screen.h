/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef GUI_SCREEN_H
#define	GUI_SCREEN_H

#include "Container.h"

#include "../sdl/Canvas.h"
#include "../NonCopyable.h"

namespace spe
{

    namespace gui
    {

        class FocusServer;

        class Screen : public Container
        {
        private:
            sdl::Canvas* m_gameScreen;

            FocusServer* m_focusServer;

        public:

            /**
             * Default constructor initializes the actual screen pointer.
             *
             * @param screen: the game's screen
             */

            Screen(sdl::Canvas* screen);


            ~Screen();


            /**
             * Screen container should not be resized by any mean.
             *
             * @param width:
             * @param height:
             */

            void Resize(int width, int height);


            /**
             * Draws the gui's contents on the game's screen.
             */

            void Draw() const;


            /**
             * Forwards the produced mouse event to the focus server.
             *
             * @param event: a mouse event occurred.
             */

            virtual void OnMouseEvent(MouseEvent& event);
        };

    };

};

#endif	/* SCREEN_H */

