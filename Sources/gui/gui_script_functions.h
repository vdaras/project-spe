/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GUI_SCRIPT_FUNCTIONS
#define GUI_SCRIPT_FUNCTIONS

#include "../sdl/Font.h"
#include "../Resource.h"
#include "Button.h"
#include "Container.h"
#include "Label.h"
#include "Picture.h"

namespace spe
{
	namespace gui
	{
		Button* CreateButton(Container* parent, const std::string& name, int x, int y, int width, int height, const std::string& caption, Resource<sdl::Font> *fnt);
		Picture* CreatePicture(Container* parent, const std::string& name, int x, int y, const std::string& path);
		Label* CreateLabel(Container* parent, const std::string& name, const std::string& txt, int x, int y, int width, int height, Resource<sdl::Font> *fnt);
	};
};

#endif
