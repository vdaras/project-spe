/**
 * Project SPE
 *
 * Copyright (C) 2011-2013 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "AbstractButton.h"

//standard library includes

//other includes

//project includes
#include "../MouseEvent.h"
#include "../sdl/sdl_utilities.h"

namespace spe
{

    namespace gui
    {

        AbstractButton::AbstractButton(const std::string& name, int width, int height)
            :
            Component(name, width, height),
            m_currentDisplay(REGULAR)
        {
        }


        AbstractButton::AbstractButton(const std::string& name, Container* parent, int x, int y, int width, int height)
            :
            Component(name, parent, x, y, width, height),
            m_currentDisplay(REGULAR)
        {
        }

        AbstractButton::~AbstractButton()
        {
        }


        bool AbstractButton::CursorInArea(int mouseX, int mouseY)
        {
            SDL_Rect absVisibleRect = GetVisibleRect();

            return sdl::PointInRect(mouseX, mouseY, &absVisibleRect);
        }

        void AbstractButton::FireActionEvent()
        {
            std::list< ActionListener* >::iterator iter(m_listeners.begin()), end(m_listeners.end());

            for(; iter != end; ++iter)
            {
                (*iter)->ActionPerformed(this);
            }
        }

        void AbstractButton::OnMouseEvent(MouseEvent& event)
        {
            if(event.GetType() == MOUSE_MOTION)
            {
                if(!m_clicked)
                {
                    int mouseX, mouseY;

                    event.GetOccurredCoordinates(&mouseX, &mouseY);

                    if(!event.IsConsumed())
                    {
                        if(CursorInArea(mouseX, mouseY))
                        {
                            m_currentDisplay = HIGHLIGHTED;

                            event.Consume();
                        }
                        else
                        {
                            m_currentDisplay = REGULAR;
                        }
                    }
                    else
                    {
                        m_currentDisplay = REGULAR;
                    }

                }
            }
            if(event.GetType() == MOUSE_CLICKED)
            {

                m_currentDisplay = CLICKED;

                m_clicked = true;
            }
            else if(event.GetType() == MOUSE_RELEASED)
            {
                int mouseX, mouseY;

                event.GetOccurredCoordinates(&mouseX, &mouseY);

                if(CursorInArea(mouseX, mouseY))
                {
                    m_currentDisplay = HIGHLIGHTED;
                }
                else
                {
                    m_currentDisplay = REGULAR;
                }

                FireActionEvent();

                m_clicked = false;
            }
        }
    };
};
