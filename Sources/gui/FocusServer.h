/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef FOCUS_SERVER_H_
#define FOCUS_SERVER_H_

namespace spe
{

    class MouseEvent;

    namespace gui
    {

        class Component;
        class Container;

        /**
         * Class used by the gui subsystem in order to determine which
         * component gets focused after a keyboard or mouse event occurrs.
         */
        class FocusServer
        {
        private:
            Component* m_lastFocused;

        public:

            FocusServer();


            ~FocusServer();


            /**
             * Resolves which component is focused after a mouse event and returns it.
             *
             * @param event: the mouse event occurred.
             * @param root: root container that we want to check its structure for focus.
             * @return a handle to the focused component.
             */

            Component* ResolveMouseFocus(const MouseEvent& event, gui::Container* root);


            /**
             * @return a handle to the component that was focused last.
             */

            Component* GetLastFocused();
        };

    };

};

#endif
