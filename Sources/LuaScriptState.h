/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "State.h"

extern "C"
{
#include "lua.h"

#include "lauxlib.h"

#include "lualib.h"
}

namespace spe
{
	class LuaScriptState : public State
	{
		const static int N_METHODS = 9;

		const static char* METHOD_TABLE[];

	private:
		lua_State* m_context;

		bool m_methodDefinitions[N_METHODS];

	public:

		LuaScriptState();

		LuaScriptState(EngineCore* engine, const std::string& script);

		~LuaScriptState();

		/**
		 * Calls the Create function residing in the script.
		 */

		virtual void Create();

		/**
		 * Updates the State by calling the corresponding method in the Lua script.
		 * The standard Update must occur first (Entity, Rendering and Physics managers).
		 *
		 * @param delta: time elapsed since last call.
		 */

		virtual void Update(unsigned delta);

		/**
		 * Receives a message from an entity.
		 *
		 */

		virtual void OnEntityMessage(EntitySystem::Entity* sender, const std::string& msg);

		/**
		 * Pushes keyboard events to the Entities. Then it forwards keyboard event to the script.
		 *
		 * @param event: a keyboard event occurred.
		 */

		virtual void OnKeyboardEvent(KeyboardEvent& event);

		/**
		 * Calls regular State Update. Then forwards the mouse event to the script.
		 *
		 * @param event: a mouse event occurred.
		 */

		virtual void OnMouseEvent(MouseEvent& event);

		/**
		 * Called when an action is performed on a gui component belonging to this State.
		 *
		 * @param sender: gui component that triggered the action.
		 */

		virtual void ActionPerformed(gui::Component* sender);

		/**
		 * Calls the pause function implemented in script.
		 */

		virtual void Pause();

		/**
		 * Calls the resume function implemented in script.
		 */

		virtual void Resume();

		/**
		 * Calls the cleanup function implemented in script.
		 */
		virtual void Cleanup();

	private:

		/**
		 * Checks which methods of State are implemented in the script.
		 */

		void CheckMethodDefinitions();

		/**
		 * Prints an error occurred when executing a script function.
		 *
		 * @param methodIndex: index (in METHOD_TABLE) of the function that failed to execute.
		 * @param errorCode: error code returned by lua.
		 */

		void PrintExecError(int methodIndex, int errorCode);
	};
};
