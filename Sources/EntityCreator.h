/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef ENTITYCREATOR_H
#define	ENTITYCREATOR_H

#include <map>

namespace spe
{

//predefinitions
namespace FileParser
{
    class EntityParser;
}

namespace EntitySystem
{
    class EntityCreator
    {
    public:

        EntityCreator();

        virtual ~EntityCreator();

        /**
         * @brief Returns a new entity as described in its script.
         * @param key - The name of the entity (script) to be created.
         * @return The new entity.
         */
        Entity* CreateEntity(const std::string& key);

        /**
         * Creates A new entity as above but its used only in case of inheritance.
         * @param key
         * @param child
         * @return
         */
        Entity* CreateEntity(const std::string& key,const std::string& child);

        /**
         * Returns true if key has already been asigned to a value.
         * @param key
         * @return
         */
        bool CheckPrototype(const std::string& key);

        /**
         * Registers the given entity as a prototype to be created later via CreateEntity.
         * @param prototype
         * @param key
         * @return
         */
        bool RegisterEntityPrototype(Entity* prototype,const std::string& key);

        /**
         * Deletes all prototypes cached. Most common use between diferent levels which
         * use different Entities.
         */
        void ResetCache();
    private:
        std::map<std::string,Entity*> prototypes;
        FileParser::EntityParser* parser;
    };

};

};

#endif	/* ENTITYCREATOR_H */

