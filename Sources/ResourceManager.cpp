/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "ResourceManager.h"
#include "Globals.h"
#include "ResourceManager.h"

namespace spe
{

SingletonInitialization(ResourceManager)

ResourceManager::~ResourceManager()
{
}

void ResourceManager::InitializePools()
{
    imagePool.SetPath(Globals::GetInstance()->GetPath("Images"));
    texturePool.SetPath(Globals::GetInstance()->GetPath("Images"));
    fontPool.SetPath(Globals::GetInstance()->GetPath("Fonts"));
    soundPool.SetPath(Globals::GetInstance()->GetPath("Sounds"));
}

Resource<sdl::Image>* ResourceManager::GetImage(const string& name)
{
    Loader<sdl::Image> loader;
    loader.request = name;
    return imagePool.RequestResource(loader);
}

Resource<sdl::Image>* ResourceManager::GetImage(const string& path, const string& name)
{
    Loader<sdl::Image> loader;
    loader.request = name;
    loader.path = path;
    return imagePool.RequestResource(loader);
}

Resource<gl::Texture>* ResourceManager::GetTexture(const string& name)
{
    Loader<gl::Texture> loader;
    loader.request = name;
    return texturePool.RequestResource(loader);
}

Resource<gl::Texture>* ResourceManager::GetTexture(const string& path, const string& name)
{
    Loader<gl::Texture> loader;
    loader.request = name;
    loader.path = path;
    return texturePool.RequestResource(loader);
}

Resource<al::Sound>* ResourceManager::GetSound(const string& name)
{
    Loader<al::Sound> loader;
    loader.request = name;
    Resource<al::Sound>* r = soundPool.RequestResource(loader);
    if(r)
    {
        r->SetImportant();
    }
    return r;
}

Resource<al::Sound>* ResourceManager::GetSound(const string& path, const string& name)
{
    Loader<al::Sound> loader;
    loader.request = name;
    loader.path = path;
    Resource<al::Sound>* r = soundPool.RequestResource(loader);
    if(r)
    {
        r->SetImportant();
    }
    return r;
}

Resource<sdl::Font>* ResourceManager::GetFont(const string& name, int size)
{
    Loader<sdl::Font> loader;
    loader.request = name;
    loader.size = size;
    return fontPool.RequestResource(loader);
}

Resource<sdl::Font>* ResourceManager::GetFont(const string& path, const string& name, int size)
{
    Loader<sdl::Font> loader;
    loader.request = name;
    loader.size = size;
    loader.path = path;
    return fontPool.RequestResource(loader);
}

/**
 * Garbage collects all unused resources.
 * @param killEmAll if true important resources will be collected as well if they have no references.
 */
void ResourceManager::GarbageCollect(bool killEmAll /*= false*/)
{
    imagePool.GarbageCollection(killEmAll);
    texturePool.GarbageCollection(killEmAll);
    fontPool.GarbageCollection(killEmAll);
    soundPool.GarbageCollection(killEmAll);
}

}
