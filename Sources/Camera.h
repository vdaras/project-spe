/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef CAMERA_H
#define CAMERA_H

#include "Rect.h"
#include <SDL/SDL.h>

namespace spe
{

class ScriptRegister;

/**
 * This class handles which portion of the game is going
 * to be blitted on the screen. For this purpose it holds an
 * Math::Rect in order to do proper clipping
 */
class Camera
{
private:

    /**
     * The viewing clip of this camera.
     * The origin of this clip is the focus of the camera.
     */
    Math::Rect m_clip;

    /**
     * Origin + (width,Height) <= (borderX,borderY)
    */
    int m_xBorder, m_yBorder;

public:

    Camera(int x, int y, int width, int height);

    Camera(const Camera& c);

    ~Camera();

    Math::Vector2F Translate(const Math::Vector2F& origin);

    /**
     * Moves camera's focus at x,y
     * @param x
     * @param y
     */
    void SetFocus(int x, int y);

    /**
     * Sets camera's borders. Setting focus or moving the camera beyond
     * this border is not possible.
     * @param maxX
     * @param maxY
     */
    void SetBorders(int maxX, int maxY); //sets camera borders

    /**
     * Moves camera's focus from current to current += (x,y)
     * @param x
     * @param y
     */
    void Move(int x, int y);

    const Math::Vector2F GetFocus();

    const Math::Rect* GetRect() const;
};

};

#endif
