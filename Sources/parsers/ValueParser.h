/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef VALUEPARSER_H
#define	VALUEPARSER_H

#include "AbstractParser.h"

#include <string>

#include "../ext/tinyxml/tinyxml.h"

namespace spe
{

namespace FileParser
{

    class ValueParser : public AbstractParser
    {
    public:

        ValueParser()
        {
        }

        /**
         * Parses an xml root and checks if it has Include tags.
         * @param root
         * @return
         */
        bool ParseIncludeList(TiXML::TiXmlElement* root);
    private:

        /**
         * Parses a value file and loads its data into valueRepository.
         * @param file the name of the file without the .smt
         * @param fileExt the .smt
         * @return
         */
        bool ParseFile(const std::string& file, const std::string& fileExt);
    };

};
	
};

#endif	/* VALUEPARSER_H */

