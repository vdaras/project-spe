/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PARTICLESYSTEMPARSER_H
#define	PARTICLESYSTEMPARSER_H

#include <list>
#include <vector>
#include "../physics/ParticleSystem.h"
#include "IntermidiateParser.h"

namespace TiXML
{
    class TiXmlElement;
}

namespace spe
{

namespace FileParser
{

    class ParticleSystemParser : public IntermidiateParser
    {
    public:
        ParticleSystemParser(const char* name,physicsSystem::particleSystem::ParticleSystem* system);
        bool ParseSystem();
    protected:
        bool ParseParticle(TiXML::TiXmlElement* xPart);
        bool ParseEmiter(TiXML::TiXmlElement* xEmiter);
    private:
        std::string fname;
        physicsSystem::particleSystem::ParticleSystem* system;
        physicsSystem::particleSystem::ParticleEmiterPrototype emiter;
    };

};
	
};
#endif	/* PARTICLESYSTEMPARSER_H */

