/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ENTITYPARSER_H
#define	ENTITYPARSER_H

#include <string>

#include "../ext/tinyxml/tinyxml.h"
#include "../PhysicsLogic.h"
#include "IntermidiateParser.h"

namespace spe
{

namespace EntitySystem
{
    class AnimationLogic;
    class DrawLogic;
    class EntityCreator;
    class Entity;
}

namespace FileParser
{

    using namespace EntitySystem;

    /*
     * TODO:
     * create a private Validate method to check for all entity properties.
     */

    /**
     * The class which parses an entity file and modifies an entity object.
     */
    class EntityParser : public IntermidiateParser
    {

        enum AnimeOffsetStyle
        {
            AOS_NONE,
            AOS_HEIGHT
        };

    public:
        EntityParser(EntityCreator* creator, const std::string& fileExtension);

        virtual ~EntityParser();

        bool ParseEntity(Entity** cacheEntity, const std::string& key, const std::string& fatherOf);

        void SetFname(std::string fname);

    protected:

        void ManageInheritanceList(const std::string& fatherOf);

        bool CheckInheritance(const char* parent, const std::string& key, Entity** entity, bool* shapedData);

        /*
         * It is essential to bear in mind that all memory allocations of Logic components
         * should be handled by end methods as ParseSimpleDraw.
         */

        //Parse Draw Hierarchy
        bool ParseDraw(TiXML::TiXmlElement* xItem, Entity* cacheEntity);
        AnimationLogic* ParseSimpleDraw(TiXML::TiXmlElement* xItem, Entity* cacheEntity);
        AnimationLogic* ParseAnimated(TiXML::TiXmlElement* xItem, Entity* cacheEntity);
        void ParseAnimationTable(TiXML::TiXmlElement* xAnimations, AnimationLogic* anime, AnimeOffsetStyle offset);
        void ParseBlending(TiXML::TiXmlElement* xBlending, DrawLogic* draw);
        //Parse Physics Hierarchy
        bool ParsePhysics(TiXML::TiXmlElement* xItem, Entity* cacheEntity, bool* shapeData);
        PhysicsLogic* ParseSimplePhysics(TiXML::TiXmlElement* xItem, Entity* cacheEntity
                                         , bool* shapeData);
        //Parse Script Hierarchy
        bool ParseScripts(TiXML::TiXmlElement* scripts, Entity* cacheEntity);
        //Parse Script
        bool ParseScript(TiXML::TiXmlElement* scriptRoot, Entity* cacheEntity);

        bool ParseProperties(TiXML::TiXmlElement* propertyRoot, Entity* cacheEntity);
    private:
        EntityCreator* creator; //the entity creator which handles this parser.
        std::string fileEx; //the extension of entity files.
        std::list<std::string> inheritanceList;
    };
}
}

#endif	/* ENTITYPARSER_H */

