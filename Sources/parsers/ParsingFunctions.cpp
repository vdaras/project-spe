/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ParsingFunctions.h"

#include <sstream>
#include <cstdlib>

namespace spe
{

namespace FileParser
{

    namespace ParsingFunctions
    {

        template<>
        Math::Vector2F Parse(const char* data)
        {
            std::istringstream iss(data);
            double x, y;
            char garb;
            iss >> x;
            iss >> garb;
            iss >> y;
            Math::Vector2F result(x, y);
            return result;
        }

        template<>
        gl::Color Parse(const char* data)
        {
            std::istringstream iss(data);
            int r, g, b, a;
            char garb;
            iss >> r;
            iss >> garb;
            iss >> g;
            iss >> garb;
            iss >> b;
            iss >> garb;
            iss >> a;
            return gl::Color(r, g, b, a);
        }

        template<>
        Math::Rect Parse(const char* data)
        {
            std::istringstream iss(data);
            int top, left, width, height;
            char garb;
            iss >> top;
            iss >> garb;
            iss >> left;
            iss >> garb;
            iss >> width;
            iss >> garb;
            iss >> height;
            return Math::Rect(top, left, width, height);
        }

        template<>
        bool Parse(const char* data)
        {
            return(strcmp(data, "true") == 0);
        }

        template<>
        int Parse(const char* data)
        {
            return std::atoi(data);
        }

        template<>
        float Parse(const char* data)
        {
            return std::atof(data);
        }

        template<>
        double Parse(const char* data)
        {
            return std::atof(data);
        }

    }
}

}
