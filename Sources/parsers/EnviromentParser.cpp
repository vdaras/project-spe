/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "EnviromentParser.h"
#include "../physics/PhysicsManager.h"
#include "../ext/tinyxml/tinyxml.h"
#include "../Globals.h"
#include "../physics/Gravity.h"
#include "../Logger.h"
#include "../ServiceLocator.h"
#include "../StateStack.h"
#include "../State.h"

namespace spe
{

namespace FileParser
{

    EnviromentParser::EnviromentParser()
    {
    }

    EnviromentParser::~EnviromentParser()
    {
    }

    bool EnviromentParser::ParseEnviroment(std::string& fname)
    {
        //Load file
        TiXML::TiXmlDocument xFile((Globals::GetInstance()->GetPath("EnviromentalData") + fname).c_str());
        if(!xFile.LoadFile())
        {
            LOG(Logger::CHANNEL_LOADING,"Error") << "Error parsing " << fname << " file not loaded. :" << xFile.ErrorDesc();
            return false;
        }

        TiXML::TiXmlElement* xRoot = xFile.RootElement();
        {
            TiXML::TiXmlElement* xPhysics = xRoot->FirstChildElement("Physics");//parse Physics
            {
                ParseGravity(xPhysics->FirstChildElement("Gravity"));
                ParseWind(xPhysics->FirstChildElement("Wind"));
                ParseResistance(xPhysics->FirstChildElement("Resistance"));
                xPhysics->Clear();
            }

            xRoot->Clear();
        }
        xFile.Clear();
        return true;
    }

    bool EnviromentParser::ParseGravity(TiXML::TiXmlElement* xGrav)
    {
        if(!xGrav)
        {
            return true;
        }
        double force;
        int direction;
        xGrav->Attribute("force", (double*) &force);
        xGrav->Attribute("direction", &direction);
        //register data to Physics Manager

        //FIXME Andy
        StateStack* stateStack = ServiceLocator< StateStack >::GetService();

        State* currentState = stateStack->Top();

        PhysicsManager* physicsManager = currentState->GetPhysicsManager();

        physicsManager->AddGlobalForce(new physicsSystem::forces::Gravity(force, direction),"gravity");
        xGrav->Clear();
        return true;
    }

    bool EnviromentParser::ParseWind(TiXML::TiXmlElement* xWind)
    {
        if(!xWind)
        {
            return true;
        }

        double minI, maxI, minD, maxD;
        {
            TiXML::TiXmlElement* xIntensity = xWind->FirstChildElement("Intensity");
            xIntensity->Attribute("minValue", &minI);
            xIntensity->Attribute("maxValue", &maxI);
            xIntensity->Clear();
        }
        {
            TiXML::TiXmlElement* xDirection = xWind->FirstChildElement("Direction");
            xDirection->Attribute("minValue", &minD);
            xDirection->Attribute("maxValue", &maxD);
            xDirection->Clear();
        }
        //register data to Physics Manager
        //ServiceLocator<PhysicsManager>.GetService()->AddForce(new physicsSystem::Wind(minI, maxI, minD, maxD));
        xWind->Clear();
        return true;
    }

    bool EnviromentParser::ParseResistance(TiXML::TiXmlElement* xRes)
    {
        if(!xRes)
        {
            return true;
        }
        double force;
        xRes->Attribute("force", &force);
        xRes->Clear();
        //register data to Physics Manager
        //ServiceLocator<PhysicsManager>.GetService()->AddForce(new physicsSystem::Resistance(force));
        return true;
    }

}
	
}
