/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef INTERMIDIATEPARSER_H
#define	INTERMIDIATEPARSER_H

#include <list>
#include <vector>
#include <sstream>

#include "../ext/tinyxml/tinyxml.h"
#include "../sdl/GLColor.h"
#include "../Rect.h"
#include "../ServiceLocator.h"
#include "../ValueRepository.h"
#include "../Vector2.h"
#include "AbstractParser.h"

namespace spe
{

namespace FileParser
{

    /**
     * A simple parser used to parse values which may contain a reference to an included
     *  value.
     */
    class IntermidiateParser : public AbstractParser
    {
    public:

        IntermidiateParser()
        {
            repo = ServiceLocator<ValueRepository>::GetService();
        }

        virtual ~IntermidiateParser()
        {
        }

    protected:

        /**
         * @param data The data to parse, maybe a raw value of T type or a reference
         * to a T type value.
         * @param defaultValue If data is nullptr or a nonexistent reference the default will be returned.
         * @return
         */
        template<typename T>
        T ParseValue(const char* data, const T& defaultValue)
        {
            if(data == nullptr)
                return defaultValue;
            if(data[0] == '@')
            {
                T value;
                if(repo->GetValue<T > (&data[1], value))
                {
                    return value;
                }
                else
                {
                    return defaultValue;
                }
            }
            else
            {
                return Parse<T > (data);
            }
        }

        template<typename T>
        T ParseValue(const char* data)
        {
            if(data == nullptr)
                return T();
            if(data[0] == '@')
            {
                T value;
                if(repo->GetValue<T > (&data[1], value))
                {
                    return value;
                }
                else
                {
                    return T();
                }
            }
            else
            {
                return Parse<T > (data);
            }
        }

    private:

        /**
         * The global valueRepository
         */
        ValueRepository* repo;

    };

}

}

#endif	/* INTERMIDIATEPARSER_H */

