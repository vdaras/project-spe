/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PARSINGFUNCTIONS_H
#define	PARSINGFUNCTIONS_H


#include "../sdl/GLColor.h"
#include "../Rect.h"
#include "../Vector2.h"

namespace spe
{

namespace FileParser
{
    namespace ParsingFunctions
    {

        /**
         * Parses the given string as T type variable.
         * @param data
         * @return
         */

        template<typename T>
        T Parse(const char* data);

        template<>
        Math::Vector2F Parse(const char* data);

        template<>
        gl::Color Parse(const char* data);

        template<>
        Math::Rect Parse(const char* data);

        template<>
        bool Parse(const char* data);

        template<>
        int Parse(const char* data);
    }

}

}

#endif	/* PARSINGFUNCTIONS_H */

