/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RESOURCELOADERS_H
#define	RESOURCELOADERS_H

#include <sstream>
#include <assert.h>

#include "../audio/Sound.h"
#include "../sdl/Image.h"
#include "../sdl/Font.h"
#include "../sdl/SDLException.h"
#include "../sdl/Texture.h"

namespace spe
{

/**
 * In this file template loader is described which is used by ResourcePools
 * in order to load resources. All resource loading logic is encapsulated in
 * the loader.
 *
 * For each different resource type a specialization of Loader must exist.
 */

template <typename Type>
struct Loader
{

    bool operator()(Type** data, const std::string& defaultPath) const
    {
        //g++ is coomplaining about these, thus I disable them for the moment
        //LOGERROR << "Unspeciallized Loader was invoked!" << SOURCE;
        //assert(0 && "Unspeciallized Loader was invoked!");

        return false;
    }

    const char* GetKey() const
    {
        //g++ is complaining about these, thus I disable them for the moment
        //LOGERROR << "Unspeciallized Loader was invoked!" << SOURCE;
        //assert(0 && "Unspeciallized Loader was invoked!");

        return false;
    }
};

/**
 * Specialization of Loader for sdl::Image
 */
template <>
struct Loader<sdl::Image>
{

    bool operator()(sdl::Image** data, const std::string& defaultPath) const
    {
        try
        {
            if(this->path.empty())
            {
                *data = new sdl::Image((defaultPath + request).c_str());
            }
            else
            {
                *data = new sdl::Image((this->path + request).c_str());
            }
            return true;
        }
        catch(spe::sdl::SdlException& e) //unsuccessful load
        {
            return false;
        }
    }

    const char* GetKey() const
    {
        return request.c_str();
    }

    std::string request;
    std::string path;
};

/**
 * Specialization of Loader for sdl::Font
 */
template <>
struct Loader<sdl::Font>
{

    bool operator()(sdl::Font** data, const std::string& defaultPath) const
    {
        try
        {
            if(this->path.empty())
            {
                *data = new sdl::Font((defaultPath + request).c_str(), size);
            }
            else
            {
                *data = new sdl::Font((this->path + request).c_str(), size);
            }
            return true;
        }
        catch(spe::sdl::SdlException& e) //unsuccessful load
        {
            return false;
        }
    }

    const char* GetKey() const
    {
        std::ostringstream oss;
        oss << request << size;
        return oss.str().c_str();
    }

    std::string request;
    std::string path;
    int size;
};

/**
 *  Specialization of loader class for Texture
 */
template <>
struct Loader<gl::Texture>
{

    bool operator()(gl::Texture** data, const std::string& defaultPath) const
    {
        try
        {
            if(this->path.empty())
            {
                *data = new gl::Texture;
                (*data)->Load((defaultPath + request).c_str());
            }
            else
            {
                *data = new gl::Texture;
                (*data)->Load((this->path + request).c_str());
            }
            return true;
        }
        catch(spe::sdl::SdlException& e) //unsuccessful load
        {
            return false;
        }
    }

    const char* GetKey() const
    {
        return request.c_str();
    }

    std::string request;
    std::string path;
};

/**
 *  Specialization of loader class for Sound
 */
template <>
struct Loader<al::Sound>
{
    bool operator()(al::Sound** data, const std::string& defaultPath) const
    {
        *data = new al::Sound;
        if(request.find(".wav",0,4) != request.npos)
        {
            return (*data)->LoadWAV((defaultPath + request).c_str());
        }
        else if(request.find(".ogg",0,4) != request.npos)
        {
            return (*data)->LoadOGG((defaultPath + request).c_str());
        }
        else
        {
            return false;
        }
    }

    const char* GetKey() const
    {
        return request.c_str();
    }

    std::string request;
    std::string path;
};

};

#endif	/* RESOURCELOADERS_H */
