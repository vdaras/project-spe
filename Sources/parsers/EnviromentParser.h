/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ENVIROMENTPARSER_H
#define	ENVIROMENTPARSER_H

#include "AbstractParser.h"

#include <string>

namespace spe
{

namespace FileParser
{

    /**
     * Reads an environment data xml file.
     */
    class EnviromentParser : public AbstractParser
    {
    public:
        EnviromentParser();
        virtual ~EnviromentParser();
        bool ParseEnviroment(std::string& fname);
    private:
        bool ParseGravity(TiXML::TiXmlElement* xGrav);
        bool ParseWind(TiXML::TiXmlElement* xWind);
        bool ParseResistance(TiXML::TiXmlElement* xRes);
    };

};

};

#endif	/* ENVIROMENTPARSER_H */

