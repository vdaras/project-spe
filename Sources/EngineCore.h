/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef ENGINECORE_H
#define	ENGINECORE_H

#include "sdl/Application.h"

namespace spe
{

class EventProcessor;
class GlobalTimer;
class KeyMapper;
class ResourceManager;
class ScriptManager;
class StateStack;
class ValueRepository;

/**
 * Central class of the engine. All control passes through this classe's single
 * instance (this is not a Singleton class, we do not wish for the instance to
 * be global).
 */

class EngineCore : public sdl::Application
{
private:

    Camera* m_camera;

    GlobalTimer* m_gTimer;

    KeyMapper* m_keyMapper;

    StateStack* m_stateStack;

    EventProcessor* m_eventProcessor;

    ResourceManager* m_resourceManager;

    ValueRepository* m_valueRepository;

public:
    EngineCore();

    ~EngineCore();


    /**
     * This method initializes the engine. The sdl::Application::Init method is
     * called first in order to initialize the graphics core and the gui's
     * screen.
     *
     * @param screenW - screen width
     * @param screenH - screen height
     * @param bpp - bits per pixel
     * @param fullscreen - flag that indicated if the application is fullscreen.
     * @return a boolean value indicating if initialization was successful or
     *         not.
     */

    bool Init(int screenW, int screenH, int bpp, bool fullscreen);


    /**
     * This method is the central control point of the engine. While the engine
     * runs, execution is located inside this method. A brief overview of Run:
     *
     * There is a loop running as long as the m_running flag is enabled. If the
     * flag is set to false then the application exits. A whole loop marks a
     * frame. At the start of the frame we get the top of the  State stack in
     * order to determine the current State of the engine (get a look at the
     * State design pattern). There is a polling loop for events, and if an
     * event occurrs it gets forwarded to the current State. Then the overriden
     * methods that Update and Render the current State are called and after
     * that there is a frame rate capping segment of code. At the end of the
     * frame the garbage collectors for Script Objects and Resources are called.
     *
     * @return an exit code
     */

    int Run();


    /**
     * @return A pointer to the Engine Core's Camera.
     */

    Camera* GetCamera();

private:

    /**
     * Renders State and gui.
     */

    void Render();
};

};

#endif	/* ENGINECORE_H */

