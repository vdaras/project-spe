/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _TILESET_H
#define	_TILESET_H

#include <string>
#include "sdl/Texture.h"
#include "Resource.h"

namespace spe
{

namespace sdl
{
    class Image;
}

class Tileset
{
private:
    Resource<gl::Texture> *m_tiles;
    int m_firstGid;

public:
    Tileset();

    Tileset(const Tileset& toCopy);

    ~Tileset();

    bool Open(const std::string& path,const std::string& file,int fgid, unsigned tilesWidth);

    int GetFirstGid() const;

    unsigned GetTotalColumns(int tilesWidth) const;

    const gl::Texture* GetTexture() const;

    Resource<gl::Texture> *GetResource() const;

    Tileset& operator = (const Tileset& toAssign);
};

};

#endif	/* _TILESET_H */

