/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "StateStack.h"

#include "State.h"
#include "EngineCore.h"
#include "ServiceLocator.h"
#include <assert.h>


namespace spe
{

StateStack::StateStack()
    :
    m_mustPop(false)
{
}


StateStack::~StateStack()
{
    while(m_states.size() > 0)
    {
        m_states.top()->Cleanup();
        delete m_states.top();
        m_states.pop();
    }
}


/**
 * Pushes a State to the stack. IMPORTANT, the State object's lifetime is now
 * determined by the StateStack.
 *
 * @param toPush
 */

void StateStack::Push(State* toPush)
{
    if(!m_states.empty())
        m_states.top()->Pause();

    m_states.push(toPush);

    //set the gui contents of the pushed state as the active gui contents
    EngineCore* engineCore = ServiceLocator< EngineCore >::GetService();
    gui::Screen* gScreen = engineCore->GetGuiScreen();
    gScreen->AddComponent(toPush->GuiContents(), 0, 0);
}


/**
 * Pops top State from the Stack.
 */

void StateStack::Pop()
{
    if(m_mustPop)
        m_mustPop = false;

    assert(!m_states.empty());

    //clean last State
    m_states.top()->Cleanup();

    delete m_states.top();

    m_states.pop();

    EngineCore* engineCore = ServiceLocator< EngineCore >::GetService();
    gui::Screen* gScreen = engineCore->GetGuiScreen();

    gScreen->Clear();

    if(m_states.empty())
    {
        //resume previous State
        m_states.top()->Resume();

        //set the gui contents of the previous state as the active gui contents
        gScreen->AddComponent(m_states.top()->GuiContents(), 0, 0);
    }
}


/**
 * Notify the StateStack that the top state must be popped when it's appropriate.
 */

void StateStack::SafePop()
{
    m_mustPop = true;
}


/**
 * @return a pointer to the State at the top of the Stack.
 */

State* StateStack::Top() const
{
    assert(!m_states.empty());

    return m_states.top();
}


/**
 * @return Count of States in stack.
 */

unsigned StateStack::Size() const
{
    return m_states.size();
}

}
