/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _DRAWABLE_H
#define	_DRAWABLE_H

#include "sdl/Texture.h"
#include "sdl/GLColor.h"
#include "Camera.h"
#include "Vector2.h"
#include "Resource.h"
#include "sdl/BlendingMethod.h"

namespace spe
{

namespace sdl
{
    class GraphicsCore;
}

class RenderingManager;

/**
 * Everything that can be rendered must
 * implement this interface. To achieve rendering each object
 * must be registered to a RenderingManager class instance.
 */

class Drawable
{
protected:
    Resource<gl::Texture>* m_display;

    RenderingManager* m_registeredTo;

    /**
     * The tint of the drawable.
     *
     */
    gl::Color m_color;

    /**
     * The blending method to be used when rendering this.
     */
    gl::BlendingMethodName m_blending;

    /**
     * The portion of the texture this drawable represents.
     */
    Math::Rect m_clip;

    /**
     * The scale of this drawable.
     * X of this vector is the scale in X axis.
     * Y of this vector is the scale in Y axis.
     * 1 means original dimensions.
     */
    Math::Vector2F m_scale;

    /**
     * The angle in which this will be rendered in degrees clockwise.
     */
    float m_angle;

    /**
     * If true image in the given axis will be flipped.
     * 0 -> X
     * 1 -> Y
     */
    bool m_flip[2];

    /**
     * If false this will not be rendered in any case.
     */
    bool m_visible;

public:
    Drawable();

    Drawable(const Drawable& toCopy);

    virtual ~Drawable();

    Drawable& operator =(const Drawable& toAssign);

    void SetDisplay(const Resource<gl::Texture> *display);

    void Hide();

    void Show();

    void SetClip(Math::Rect clip);

    Math::Rect GetClip() const;

    void SetColor(gl::Color color);

    gl::Color GetColor() const;

    void SetVisible(bool visible);

    bool IsVisible() const;

    //In order for this class to stay compatible with the old Drawable hierarchy we needed
    //these virtual methods.
    virtual bool IsVisible(const Camera& camera) const = 0;
    virtual int GetWidth() const = 0;
    virtual int GetHeight() const = 0;
    virtual void Draw(sdl::GraphicsCore* gCore, Camera* camera = nullptr) = 0;
    virtual Math::Vector2F GetPosition() const = 0;
    virtual void SetPosition(const Math::Vector2F& position) = 0;
    /*TO BE REMOVED*/
    virtual int GetX() const = 0;
    virtual void SetX(int newX) = 0;
    virtual int GetY() const = 0;
    virtual void SetY(int newY) = 0;
    /**/
    void SetScale(Math::Vector2F scale);
    Math::Vector2F GetScale() const;
    void SetAngle(float angle);
    float GetAngle() const;
    void SetBlending(gl::BlendingMethodName blending);
    gl::BlendingMethodName  GetBlending() const;

    /**
     * Sets the Rendering Manager that this Drawable is registered to.
     *
     * @param renderer: the rendering manager that this Drawable is registered to.
     */

    void SetRenderingManager(RenderingManager* renderer);

};

};

#endif	/* _DRAWABLE_H */

