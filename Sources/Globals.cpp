/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "ext/tinyxml/tinyxml.h"
#include "Globals.h"
#include "Singleton.h"
#include "Logger.h"
#include <string>
#include <fstream>
#include <iostream>
#include <iosfwd>
#include <sstream>

namespace spe
{
	SingletonInitialization(Globals)

	void Globals::Initialization(bool flag, const char* filename)
	{
		bool result = false;
		if (flag)
		{
			result = LoadFromXML(filename);
		}

		if (!result)  //hardcode initialization due to failure
		{
			screenHeight = 600;
			screenWidth = 800;
			fullscreen = false;
			appName = "The Game";
			LOGERROR << "FAILED TO LOAD MAIN.XML";
		}
	}

	bool Globals::LoadFromXML(const char* filename)
	{
		using namespace TiXML;

		TiXmlDocument* main = new TiXmlDocument(filename);
		if (!main->LoadFile()) {
			return false;
		}

		TiXmlElement* game = main->FirstChildElement("Game");

		if (!game)
		{
			return 0;
		}

		appName = game->Attribute("name");
		game->Attribute("height", &screenHeight);
		game->Attribute("width", &screenWidth);

		std::string full(game->Attribute("fullscreen"));
		fullscreen = (full == "true");

		if (!screenHeight || !screenWidth)
		{
			return false; //if one of the two dims is nullptr return error
		}

		newBuild = false;

		const std::string buidlTime = GenerateTimestamp();

		const char* timestamp = game->Attribute("timestamp");
		if (timestamp)
		{
			newBuild = strcmp(timestamp, buidlTime.c_str()) != 0;
		}
		else
		{
			newBuild = true;
		}

		if (newBuild)
		{
			game->SetAttribute("timestamp", buidlTime.c_str());
			main->SaveFile();
		}

		{
			TiXmlElement* root = game->FirstChildElement("Paths");
			TiXmlElement* xPath;
			xPath = root->FirstChildElement();
			while (xPath)
			{
				std::string name = xPath->Value();
				std::string pathValue = xPath->Attribute("path");
				NormalizePath(pathValue);
				paths[name] = pathValue;

				TiXML::TiXmlElement* e = xPath->NextSiblingElement();
				xPath->Clear();
				xPath = e;
			}
			root->Clear();
		}

		{
			TiXmlElement* root = game->FirstChildElement("Files");
			TiXmlElement* xFile;
			xFile = root->FirstChildElement();
			while (xFile)
			{
				std::string name = xFile->Value();
				std::string pathValue = xFile->Attribute("name");
				NormalizePath(pathValue);
				files[name] = pathValue;

				TiXML::TiXmlElement* e = xFile->NextSiblingElement();
				xFile->Clear();
				xFile = e;
			}
			root->Clear();
		}

		game->Clear();
		main->Clear();
		delete main;
		return true;
	}

	unsigned long Globals::GetScreenWidth()
	{
		return screenWidth;
	}

	unsigned long Globals::GetScreenHeight()
	{
		return screenHeight;
	}

	bool Globals::GetFullscreen()
	{
		return fullscreen;
	}

	void Globals::NormalizePath(std::string& path)
	{
		size_t found = 0;
		found = path.find("$");
		if (found != path.npos)
		{
			std::string pathRoot = path.substr(0, found);
			path.erase(0, found + 1);
			path = paths[pathRoot] + path;
			return;
		}
	}

	void Globals::SetAppName(std::string appName)
	{
		this->appName = appName;
	}

	std::string Globals::GetAppName() const
	{
		return appName;
	}

	void Globals::SetFullscreen(bool fullscreen)
	{
		this->fullscreen = fullscreen;
	}

	bool Globals::IsFullscreen() const
	{
		return fullscreen;
	}

	void Globals::SetScreenHeight(int screenHeight)
	{
		this->screenHeight = screenHeight;
	}

	void Globals::SetScreenWidth(int screenWidth)
	{
		this->screenWidth = screenWidth;
	}

	bool Globals::IsNewBuild() const
	{
		return newBuild;
	}

	std::string Globals::GetPath(const std::string& name)
	{
		return paths[name];
	}

	std::string Globals::GetFile(const std::string& name)
	{
		return files[name];
	}
}