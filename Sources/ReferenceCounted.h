/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef SCRIPTREFTYPE_H
#define	SCRIPTREFTYPE_H

namespace spe
{

/**
 * Classes that need to be reference counted inherit this class.
 */

class ReferenceCounted
{
protected:
    int m_refCount;

public:

    /**
     * Constructor initializes references to 1.
     */

    ReferenceCounted();


    virtual ~ReferenceCounted();


    /**
     * Increases references to this object.
     */

    virtual void IncRef();


    /**
     * Decreases references to this object.
     */

    virtual void DecRef();
};

};

#endif	/* SCRIPTREFTYPE_H */

