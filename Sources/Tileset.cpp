/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Tileset.h"
#include "Resource.h"
#include "ResourceManager.h"
#include <cstdlib>

namespace spe
{

Tileset::Tileset()
{
    m_tiles = nullptr;
    m_firstGid = 0;
}


Tileset::Tileset(const Tileset& toCopy)
{
    m_tiles = nullptr;

    if(toCopy.m_tiles)
    {
        m_tiles = toCopy.m_tiles;
        m_tiles->IncreaseReference();
    }

    m_firstGid = toCopy.m_firstGid;
}


Tileset::~Tileset()
{
    if(m_tiles)
        m_tiles->DecreaseReference();
}


bool Tileset::Open(const std::string& path, const std::string& file, int fgid, unsigned tilesWidth)
{
    m_firstGid = fgid;
    Resource<gl::Texture>*  resource = ResourceManager::GetInstance()->GetTexture(path.c_str(),file.c_str());
    //m_totalColumns = m_tiles->getResImage_ptr()->GetWidth()/tilesWidth;
    if(resource)
    {
        if(m_tiles)
        {
            m_tiles->DecreaseReference();
        }

        m_tiles = resource;
    }

    return m_tiles != nullptr;
}


int Tileset::GetFirstGid() const
{
    return m_firstGid;
}


unsigned Tileset::GetTotalColumns(int tilesWidth) const
{
    return  m_tiles->GetRawData()->GetWidth() / tilesWidth;
}

const gl::Texture* Tileset::GetTexture() const
{
    return m_tiles->GetRawData();
}

Resource<gl::Texture> *Tileset::GetResource() const
{
    return m_tiles;
}

Tileset& Tileset::operator =(const Tileset& toAssign)
{
    if(this != &toAssign)
    {
        if(m_tiles)
            m_tiles->DecreaseReference();

        m_tiles = toAssign.m_tiles;

        m_tiles->IncreaseReference();

        m_firstGid = toAssign.m_firstGid;
    }

    return *this;
}

}
