/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef TILEPROPERTIES_H
#define	TILEPROPERTIES_H

#include <map>
#include <string>

namespace spe
{

/**
 * A container class. It encapsulates the different properties map tiles may have and
 * binds the string with the bit flag of the tiles and the logic underneath the property.
 */
class TileProperties
{
public:
    TileProperties();
    virtual ~TileProperties();
    bool ParsePoperties(const char* filename);
    unsigned long ValueOf(const std::string& name) const;
    std::string NameOf(unsigned long flag);
private:
    typedef std::map< unsigned long,std::string > propertiesMap;
    propertiesMap tileProperties;
};

};

#endif	/* TILEPROPERTIES_H */

