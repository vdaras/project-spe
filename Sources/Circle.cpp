/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "Mathematics.h"
#include "Circle.h"

namespace spe
{

namespace Math
{

// <editor-fold defaultstate="collapsed" desc="Constructors">

    Circle::Circle(const Circle& orig) : center(orig.center)
    {
        radious = orig.radious;
    }

    Circle::Circle(const Vector2F& cent, float radious) : center(cent)
    {
        this->radious = radious;
    }

    Circle::Circle(float centerX, float centerY, float radious) : center(centerX, centerY)
    {
        this->radious = radious;
    }

    Shape* Circle::Clone()
    {
        return new Circle(*this);
    }

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Intersection Proxy Methods ">

    bool Circle::Intersect(const Shape& shape) const
    {
        return shape.Intersect(*this);
    }

    bool Circle::Intersect(const Vector2I& vector) const
    {
        return (vector.ConvertTo<float>() - center).Length() <= radious;
    }

    bool Circle::Intersect(const Vector2F& vector) const
    {
        return (vector - center).Length() <= radious;
    }

    bool Circle::Intersect(const Vector2D& vector) const
    {
        return (vector.ConvertTo<float>() - center).Length() <= radious;
    }// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Intersection Ex Proxy Methods">

    IntersectionType Circle::Intersect(const Shape& shape, Vector2Templ<float>& axisInfo) const
    {
        return shape.IntersectWith(*this, axisInfo);
    }// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Close Distance Proxy methods">

    Vector2Templ<float> Circle::DistanceClosestPoint(const Shape& shape) const
    {
        return shape.DistanceClosestPointFrom(*this);
    }

    Vector2Templ<float> Circle::DistanceClosestPoint(const Vector2F& vect) const
    {
        return Math::DistanceClosestPoint(*this, vect);
    }// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Proxy methods for double dispatch">

    float Circle::CalculateArea() const
    {
        return static_cast<float>(2 * radious * PI);
    }

    bool Circle::IntersectWith(const Circle& circle) const
    {
        return Math::Intersect(circle, *this);
    }

    bool Circle::IntersectWith(const Rect& rect) const
    {
        return Math::Intersect(*this, rect);
    }

    IntersectionType Circle::IntersectWith(const Circle& circle, Vector2Templ<float>& axisInfo) const
    {
        return Math::Intersect(circle, *this, axisInfo);
    }

    IntersectionType Circle::IntersectWith(const Rect& rect, Vector2Templ<float>& axisInfo) const
    {
        return Math::Intersect(*this, rect, axisInfo);
    }

    Vector2Templ<float> Circle::DistanceClosestPointFrom(const Rect& rect) const
    {
        return Math::DistanceClosestPoint(*this, rect);
    }

    Vector2Templ<float> Circle::DistanceClosestPointFrom(const Circle& circle) const
    {
        return Math::DistanceClosestPoint(circle, *this);
    }// </editor-fold>

}

}

