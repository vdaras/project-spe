/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _TILE_H
#define	_TILE_H

#include "Rect.h"

namespace spe
{

class Tileset;

class Tile
{
private:
    /**
     * Mask used to determine if one gid is flipped in X axis.
     */
    static const unsigned int maskX = 0x80000000;

    /**
     * Mask used to determine if one gid is flipped in Y axis.
     */
    static const unsigned int maskY = 0x40000000;

    Math::Rect m_clip;

    unsigned long m_flag;

    Tileset* m_tileset;

    int m_gid;

    bool m_flipX;

    bool m_flipY;

public:
    Tile(unsigned int sGid = 0, unsigned long flag = 0);

    ~Tile();

    void SetRect(unsigned x, unsigned y, unsigned width, unsigned height);

    void SetRect(const Math::Rect& rect);

    void SetGid(int sGid);

    /**
     *
     * @return the true id of this tileset regardless of its flips.
     * Use this when flipping doesn't matter. ex. On collision calculations.
     */
    int GetTilesetID() const;

    void SetTileset(Tileset* sTileset);

    Tileset* GetTileset();

    /**
     *
     * @return the gid of this tile based on its flips.
     * Use this when flipping matters. ex. On drawing.
     */
    int GetGid() const;

    const Math::Rect& GetClip() const;

    /**
     * Clears out the flag.
     */
    inline void ClearFlag()
    {
        m_flag ^= m_flag;
    }

    /**
     * Adds a flag to this tile.
     */
    inline void AddToFlag(unsigned int value)
    {
        m_flag |= value;
    }

    /**
     * Removes a mask from the tile.
     */
    inline void RemoveFromFlag(unsigned int value)
    {
        m_flag = m_flag ^ value;
    }

    /**
     * Returns true if this tile is value.
     */
    inline bool CheckFlag(unsigned int value)
    {
        return m_flag & value;
    }

	inline unsigned long GetFlag() const
    {
        return m_flag;
    }
    bool IsFlipY() const;
    bool IsFlipX() const;
};

};

#endif	/* _TILE_H */
