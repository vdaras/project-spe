/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef UTILITIES_H
#define	UTILITIES_H

#include <vector>
#include <list>
#include <map>
#include <string>

namespace spe
{

/**
 * Calls delete for every element of the list.
 * @param lista - The list to clear out.
 * @param T - The type of elements of list.
 */
template<typename T>
void SafeRelease(std::list<T>& lista)
{
    using namespace std;
    typename list<T>::iterator iter;
    for(iter = lista.begin(); iter != lista.end(); ++iter)
    {
        T toDie = (*iter);
        delete toDie;
    }
    lista.clear();
}

/**
 * Calls delete for every element of the vector.
 * @param vect - The vector to clear out.
 * @param T - The type of elements of vector.
 */
template<typename T>
void SafeRelease(std::vector<T>& vect)
{
    using namespace std;
    typename std::vector<T>::iterator iter;
    for(iter = vect.begin(); iter != vect.end(); ++iter)
    {
        T toDie = (*iter);
        delete toDie;
    }
    vect.clear();
}

/**
 * Calls delete for every element of the map.
 * @param map - The map to clear out.
 * @param T - The type of elements of map.
 */
template<typename Key,typename T>
void SafeRelease(std::map<Key,T>& map)
{
    using namespace std;
    typename std::map<Key,T>::iterator iter;
    for(iter = map.begin(); iter != map.end(); ++iter)
    {
        T toDie = iter->second;
        delete toDie;
    }
    map.clear();
}

/**
 * Template functor that deallocates an object of any type.
 *
 * @param ptr - pointer to the object.
 */
template <class PtrType>
struct FreePointer
{
    void operator()(PtrType* ptr)
    {
        delete ptr;
    }
};


/**
 * Converts a string to upper case.
 *
 * @param str: OUTPUT parameter, modifies this string's characters.
 */
void StrToUpper(std::string& str);

};

#endif	/* UTILITIES_H */

