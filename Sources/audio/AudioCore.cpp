/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "AudioCore.h"
#include "../Logger.h"
#include "AudioSource.h"
#include <string>
#include <fstream>
#include <iostream>
#include <vector>

#define OV_EXCLUDE_STATIC_CALLBACKS
#include <vorbis/vorbisfile.h>

#ifdef __WIN32
#include <al.h>
#include <alc.h>
#include <list>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

namespace spe
{
	namespace al
	{
		AudioCore::AudioCore()
		{
			//set the listener at 0,0,0 (3dxyz)
			alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
		}

		bool AudioCore::Initialize()
		{
			//open device
			ALCdevice* device = alcOpenDevice(nullptr);
			if (device)
			{
				//create context and bind it with the device
				ALCcontext* context = alcCreateContext(device, nullptr);
				if (context)
				{
					alcMakeContextCurrent(context);
				}
				else
				{
					LOGERROR << "Error creating sound context.";
					alcCloseDevice(device);
					return false;
				}
			}
			else//error opening device
			{
				LOGERROR << "Error opening sound device.";
				return false;
			}

			// Clear Error Code (so we can catch any new errors)
			alGetError();

			return true;
		}

		void AudioCore::Shutdown()
		{
			ALCcontext* context;
			ALCdevice* device;

			context = alcGetCurrentContext();
			device = alcGetContextsDevice(context);

			alcMakeContextCurrent(nullptr);
			alcDestroyContext(context);
			alcCloseDevice(device);
		}

		AudioCore::~AudioCore()
		{
			Shutdown();
		}

		std::string AudioCore::ErrorStringOV(int code)
		{
			switch (code)
			{
			case OV_EREAD:
				return std::string("Read from media.");
			case OV_ENOTVORBIS:
				return std::string("Not Vorbis data.");
			case OV_EVERSION:
				return std::string("Vorbis version mismatch.");
			case OV_EBADHEADER:
				return std::string("Invalid Vorbis header.");
			case OV_EFAULT:
				return std::string("Internal logic fault (bug or heap/stack corruption.");
			default:
				return std::string("Unknown Ogg error.");
			}
		}

		std::string AudioCore::ErrorStringAL(int code)
		{
			switch (code)
			{
			case AL_OUT_OF_MEMORY:
				return std::string("AL out of memory.");
			case AL_ILLEGAL_COMMAND:
				return std::string("Illegal or invalid command.");
			case AL_INVALID_VALUE:
				return std::string("Invalid Vorbis header.");
			case AL_INVALID_ENUM:
				return std::string("Illegal or invalid enum.");
			case AL_INVALID_NAME:
				return std::string("Invalid name.");
			default:
				return std::string("Unknown Error.");
			}
		}

		bool AudioCore::Check()
		{
			int error = alGetError();
			if (error != AL_NO_ERROR)
			{
				LOG(Logger::CHANNEL_AUDIO, "Error") << ErrorStringAL(error);
				return false;
			}
			return true;
		}

		void AudioCore::ClearErrorBuffer()
		{
			alGetError();
		}
	}
}