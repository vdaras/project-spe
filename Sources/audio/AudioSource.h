/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef AUDIOSOURCE_H
#define	AUDIOSOURCE_H

#include "AbstractALSource.h"

#include "../Resource.h"
#include "Sound.h"

namespace spe
{

namespace al
{

    /**
     * A concrete class that represents a source of Sound.
     * It is used in combination with Sound class.
     * Its use is to play sound effects.
     */
    class AudioSource : public AbstractALSource
    {
        //AudioManager is a friend of this class in order to force the creation and deletion
        //of instances of this class to be achieved via it.
        friend class AudioManager;

    public:

        /**
         * Loads the specified sound file (if needed) and binds it with this source.
         * In case this source already has Loaded a different sound it will be Released.
         * @param file
         * @return
         */
        bool Load(const char* file);

        void SetOneTime(bool oneTime);

        bool IsOneTime() const;

    private:

        bool AttachBuffer(ALuint buffer);

        /**
         * The constructor is private in order to force the creation of new sources
         * via the AudioManager::CreateSource method.
         * @param source
         * @param oneTime
         */
        AudioSource(ALuint rawSource, bool oneTime = true);

        /**
         * The destructor is private in order to force the deletion of sources
         * via the AudioManager::KillSource method.
         */
        virtual ~AudioSource();

        /**
         * The sound currently playing in this source
         */
        Resource<al::Sound>* soundData;

        /**
         * If set to true the source will be killed after playing the sound once.
         */
        bool oneTime;
    };

};
	
};
#endif	/* AUDIOSOURCE_H */

