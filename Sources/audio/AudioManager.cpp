/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "AudioManager.h"
#include "../Logger.h"
#include "AudioSource.h"
#include "AudioCore.h"
#include "StreamSource.h"
#include <string>
#include <fstream>
#include <iostream>
#include <list>
#include <algorithm>

#ifdef __WIN32
#include <al.h>
#include <alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

namespace spe
{

namespace al
{
    namespace predicates
    {

        struct StreamUpdater
        {
            void operator()(StreamSource* stream)
            {
                if(stream->IsPlaying())
                {
                    if(!stream->UpdateStream())
                    {
                        stream->Pause();
                    }
                }
            }
        };

        struct SourcePauser
        {
            void operator()(AbstractALSource* src)
            {
                if(src->IsPlaying())
                    src->Pause();
            }
        };

        struct SourcePlayer
        {
            void operator()(AbstractALSource* src)
            {
                if(src->IsPaused())
                    src->Play();
            }
        };

    }

    AudioManager::AudioManager()
    {

    }

    AudioManager::~AudioManager()
    {
    }

    bool AudioManager::Update()
    {
        static predicates::StreamUpdater su;
        std::for_each(streams.begin(), streams.end(), su);

        KillOneTimeSounds();

        return true;
    }

    AudioSource* AudioManager::CreateSource()
    {
        ALuint sID = CreateSourceID();
        if(sID)
        {
            AudioSource* src = new AudioSource(sID);

            sources.push_back(src);
            return src;
        }

        return nullptr;
    }

    void AudioManager::KillSource(AudioSource* toKill)
    {
        AudioCore::ClearErrorBuffer();
        sources.remove(toKill);
        delete toKill;
        if(!AudioCore::Check())
        {
            LOG(Logger::CHANNEL_AUDIO,"Error") << "Source deletion failed.";
        }
    }

    StreamSource* AudioManager::CreateStream()
    {
        ALuint sID = CreateSourceID();
        if(sID)
        {
            StreamSource* stream = new StreamSource(sID);
            streams.push_back(stream);
            return stream;
        }

        return nullptr;
    }

    void AudioManager::KillStream(StreamSource* toKill)
    {
        AudioCore::ClearErrorBuffer();
        streams.remove(toKill);
        delete toKill;
        if(!AudioCore::Check())
        {
            LOG(Logger::CHANNEL_AUDIO,"Error") << "Stream deletion failed.";
        }
    }

    void AudioManager::KillOneTimeSounds()
    {
        AudioListIter it = sources.begin();
        AudioListConstIter end = sources.end();
        while(it != end)
        {
            if((*it)->IsOneTime() && !(*it)->IsPlaying())
            {
                AudioSource* toKill = (*it);
                ++it;
                KillSource(toKill);
            }
            else
            {
                ++it;
            }
        }
    }

    void AudioManager::PlayAll()
    {
        static predicates::SourcePlayer sp;
        std::for_each(streams.begin(), streams.end(), sp);
    }

    void AudioManager::PauseAll()
    {
        static predicates::SourcePauser sp;
        std::for_each(streams.begin(), streams.end(), sp);
    }

    unsigned int AudioManager::CreateSourceID()
    {
        ALuint sID;
        AudioCore::ClearErrorBuffer();
        alGenSources(1, &sID);

        if(!AudioCore::Check())
        {
            LOG(Logger::CHANNEL_AUDIO, "Error") << "Source creation failed.";
            return 0;
        }

        return sID;
    }

}
	
}
