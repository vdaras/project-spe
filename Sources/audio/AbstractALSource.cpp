/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "AbstractALSource.h"

namespace spe
{

namespace al
{

    AbstractALSource::AbstractALSource(ALuint id)
    {
        sourceId = id;
        numOfLoops = 0;
    }

    AbstractALSource::~AbstractALSource()
    {
    }

    void AbstractALSource::Play()
    {
        alSourcePlay(sourceId);
    }

    bool AbstractALSource::IsPlaying()
    {
        ALint iState;
        alGetSourcei(sourceId, AL_SOURCE_STATE, &iState);
        return iState == AL_PLAYING;
    }

    bool AbstractALSource::IsPaused()
    {
        ALint iState;
        alGetSourcei(sourceId, AL_SOURCE_STATE, &iState);
        return iState == AL_PAUSED;
    }

    bool AbstractALSource::IsStopped()
    {
        ALint iState;
        alGetSourcei(sourceId, AL_SOURCE_STATE, &iState);
        return iState == AL_STOPPED;
    }

    void AbstractALSource::Pause()
    {
        alSourcePause(sourceId);
    }

    void AbstractALSource::Stop()
    {
        alSourceStop(sourceId);
    }

    void AbstractALSource::Rewind()
    {
        alSourceRewind(sourceId);
    }

    float AbstractALSource::GetPitch()
    {
        float value;
        alGetSourcef(sourceId, AL_PITCH, &value);
        return value;
    }

    void AbstractALSource::SetPitch(float pitch)
    {
        alSourcef(sourceId, AL_PITCH, pitch);
    }

    float AbstractALSource::GetGain()
    {
        float value;
        alGetSourcef(sourceId, AL_GAIN, &value);
        return value;
    }

    void AbstractALSource::SetGain(float gain)
    {
        alSourcef(sourceId, AL_GAIN, gain);
    }

    ALuint AbstractALSource::GetRawSource()
    {
        return sourceId;
    }

    void AbstractALSource::SetLooping(bool loop)
    {
        alSourcei(sourceId, AL_LOOPING, loop);
    }

    void AbstractALSource::SetNumOfLoops(int numOfLoops)
    {
        this->numOfLoops = numOfLoops;
    }

    int AbstractALSource::GetNumOfLoops() const
    {
        return numOfLoops;
    }

    bool AbstractALSource::IsLooping()
    {
        int value;
        alGetSourcei(sourceId, AL_LOOPING, &value);
        return static_cast < bool >(value);
    }

    ALuint AbstractALSource::GetSourceState()
    {
        ALint state;
        alGetSourcei(sourceId, AL_SOURCE_STATE, &state);
        return state;
    }
}
}
