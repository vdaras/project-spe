/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef STREAMSOURCE_H
#define	STREAMSOURCE_H

#define AL_LOOP_FOREVER -1

#include "AbstractALSource.h"

namespace spe
{

namespace al
{

    class OGGstream;

    class StreamSource : public AbstractALSource
    {
        friend class AudioManager;

    public:

        /**
         * Sets the timer of this stream back to 0.
         */
        virtual void Rewind();

        /**
         * Prepares this stream source to play the specified file.
         * This method is the core method of this class.
         * @param name
         * @return
         */
        bool Prepare(const char* filepath);

        /**
         * If needed this stream updates all its buffers.
         * @return
         */
        bool UpdateStream();

    private:
        /**
         * The constructor is private in order to force the creation of new streams
         * via the AudioManager::CreateStream method.
         * @param source
         */
        StreamSource(ALuint source);

        /**
         * The destructor is private in order to force the deletion of streams
         * via the AudioManager::KillStream method.
         */
        virtual ~StreamSource();

        /**
         * The file stream binded with this stream source.
         */
        OGGstream* dataStream;

        /**
         * If false this stream will stop updating.
         */
        bool updatable;
    };

};

};


#endif	/* STREAMESOURCE_H */

