/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "AudioSource.h"
#include "AudioCore.h"
#include <iostream>

#ifdef __WIN32
#include <al.h>
#else
#include <AL/al.h>
#endif

#include "../ResourceManager.h"

namespace spe
{

namespace al
{

    AudioSource::AudioSource(ALuint rawSource,bool oneTime /*= true*/) : AbstractALSource(rawSource)
    {
        this->oneTime = oneTime;
        alSource3f(sourceId, AL_POSITION, 0.0, 0.0, 0.0);
        alSource3f(sourceId, AL_VELOCITY, 0.0, 0.0, 0.0);
        alSource3f(sourceId, AL_DIRECTION, 0.0, 0.0, 0.0);
        alSourcef(sourceId, AL_ROLLOFF_FACTOR, 0.0);
        alSourcei(sourceId, AL_SOURCE_RELATIVE, AL_TRUE);

        /* Rolloff factor judges the strength of attenuation over distance.
         * By setting it to 0 we will have turned it off.
         * This means that no matter how far away the Listener is to the source
         * will still hear it.
         * The same idea applies to source relativity.
         */
        soundData = nullptr;
    }

    AudioSource::~AudioSource()
    {
        AudioCore::ClearErrorBuffer();

        alDeleteSources(1, &sourceId);

        if(!AudioCore::Check())
        {
            LOG(Logger::CHANNEL_AUDIO, "Note") << "alDeleteSources failed trying to delete source ";  // << SOURCE;
        }

        if(soundData != nullptr)
        {
            soundData->DecreaseReference();
        }
    }

    bool AudioSource::Load(const char* file)
    {
        //if data already exists release it
        if(soundData != nullptr)
        {
            soundData->DecreaseReference();
        }

        //load sound via resource manager
        soundData = ResourceManager::GetInstance()->GetSound(file);
        if(soundData != nullptr)
        {
            AttachBuffer(soundData->GetRawData()->GetBufferId());
            return true;
        }

        return false;
    }

    void AudioSource::SetOneTime(bool oneTime)
    {
        this->oneTime = oneTime;
    }

    bool AudioSource::IsOneTime() const
    {
        return oneTime;
    }

    bool AudioSource::AttachBuffer(ALuint buffer)
    {
        AudioCore::ClearErrorBuffer();

        alSourcei(sourceId, AL_BUFFER, buffer);

        if(!AudioCore::Check())
        {
            LOG(Logger::CHANNEL_AUDIO, "Note") << "alSourcei failed trying to attach buffer ";  // << SOURCE;
            return false;
        }
        return true;
    }

}
	
}
