/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef SOUND_H
#define SOUND_H

#if defined(_WIN32)
#include <al.h>
#include <alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

namespace spe
{

namespace al
{

    /**
     * Represents a sound loaded from disk.
     */
    class Sound
    {
    public:
        Sound();

        virtual ~Sound();

        /**
         * Loads a WAV sound file and fills this sound's buffer with it.
         * If buffer is not empty it will be released first.
         * @param path
         * @return true in success
         */
        bool LoadWAV(const char* path);

        /**
         * Loads an OGG sound file and fills this sound's buffer with it.
         * If buffer is not empty it will be released first.
         * @param path
         * @return true in success
         */
        bool LoadOGG(const char* path);

        /**
         * Release the internal buffer.
         * @return
         */
        bool Release();

        /**
         * Returns the id given to the internal buffer by OpenAL.
         * @return
         */
        ALuint GetBufferId();

    private:

        /**
         * Sets the buffer id.
         * @param buffer
         * @return
         */
        bool SetData(ALuint buffer);

        /**
         * The id of the buffer.
         */
        ALuint bufferId;
    };
};

};
#endif
