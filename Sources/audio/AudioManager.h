/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef AUDIOMANAGER_H
#define	AUDIOMANAGER_H

#include <list>

namespace spe
{

namespace al
{

    class AudioSource;
    class AudioCore;
    class StreamSource;

    class AudioManager
    {
    public:
        AudioManager();

        virtual ~AudioManager();

        /**
         * Update streams.
         * @return
         */
        bool Update();

        /**
         * Returns a newly created audio source.
         * @return
         */
        AudioSource* CreateSource();

        /**
         * Deallocates an audio source.
         * @param toKill
         */
        void KillSource(AudioSource* toKill);

        StreamSource* CreateStream();

        void KillStream(StreamSource* toKill);

        void KillOneTimeSounds();

        /**
         * All paused sources and streams (not stopped) start playing.
         */
        void PlayAll();

        /**
         * All playing sources and streams pause;
         */
        void PauseAll();

        //global grain streams
        //global grain sources

    private:

        /**
         * Created a new valid id to be used as openal source id.
         * @return
         */
        unsigned int CreateSourceID();

        typedef std::list<AudioSource*> AudioList;
        typedef AudioList::iterator AudioListIter;
        typedef AudioList::const_iterator AudioListConstIter;
        AudioList sources;

        typedef std::list<StreamSource*> StreamList;
        typedef StreamList::iterator StreamListIter;
        typedef StreamList::const_iterator StreamListConstIter;
        StreamList streams;

    };

};

};
#endif	/* AUDIOMANAGER_H */

