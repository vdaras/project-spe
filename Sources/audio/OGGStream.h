/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef OGGSTREAM_H
#define OGGSTREAM_H

#include "Sound.h"

#if defined(_WIN32)
#include <vorbis/vorbisfile.h>
#else
#include <vorbis/vorbisfile.h>
#endif

namespace spe
{

namespace al
{

    enum STREAM_UPDATE_RESULT
    {
        SUR_EOF = 0,
        SUR_OK = 1,
        SUR_LOOP,
        SUR_ERROR
    };

    class OGGstream
    {
    public:

        OGGstream();

        virtual ~OGGstream();

        /**
         * Opens tha specified file and initializes all structs neede in order to
         * begin streaming from it.
         * @param fname
         * @return
         */
        bool Open(const char* fname);

        /**
         * Release all resources of this.
         */
        void Release();

        void UnqueueBuffers(ALuint source);

        /**
         * Must be called after Open when Stream is ready to begin the streaming.
         * @param source - The source id of the Stream binded with this OGGStream.
         * @return
         */
        bool InitializeStream(ALuint source);

        /**
         * Updtae the buffers of the stream if it's necessary.
         * @param source
         * @param loop
         * @return
         */
        STREAM_UPDATE_RESULT Update(ALuint source,bool loop);

        /**
         * Rewinds the stream.
         * @param source
         */
        void Rewind(ALuint source);

    protected:

        /**
         * Reloads the buffers if needed.
         * @param buffer
         * @return
         */
        STREAM_UPDATE_RESULT Stream(ALuint buffer);

    private:

        /**
         * The file being streamed.
         */
        FILE* oggFile;

        /**
         * The ogg vorbis file handle.
         */
        OggVorbis_File oggStream;

        /**
         * Formating data.
         */
        vorbis_info* vorbisInfo;

        /**
         * Audio format.
         */
        ALuint format;

        /**
         * Front and backbuffer of the streamed file.
         */
        ALuint* buffers;
    };

};
	
};

#endif
