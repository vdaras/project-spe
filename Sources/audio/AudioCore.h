/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _AUDIOCORE_H
#define	_AUDIOCORE_H

#include <string>

//macros used for vorbis read function.
//Specifies big or little endian byte packing. 0 for little endian, 1 for b if endian. Typical value is 0.
#define ENDIAN 0
//Specifies word size. Possible arguments are 1 for 8-bit samples, or 2 or 16-bit samples. Typical value is 2.
#define WORD_SIZE 2
//Signed or unsigned data. 0 for unsigned, 1 for signed. Typically 1.
#define SGNED 1

namespace spe
{

namespace al
{

    const long BUFFER_SIZE = (4096 * 10);
    const int SINGLE_BUFFER_LIMIT = 4096;

    class AudioCore
    {
    public:

        AudioCore();

        virtual ~AudioCore();

        bool Initialize();

        void Shutdown();

        void SetListener(float x, float y, float z = 0);

        /**
         * Checks if openal reported any errors.
         * @return
         */
        static bool Check();

        /**
         * Clears of openal reported errors so that new errors are reported
         * correctly
         */
        static void ClearErrorBuffer();
    private:

        static std::string ErrorStringAL(int code);

        static std::string ErrorStringOV(int code);
    };
};
};

#endif	/* _AUDIOCORE_H */
