/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef ABSTRACTALSOURCE_H
#define	ABSTRACTALSOURCE_H

#ifdef _WIN32
#include <al.h>
#else
#include <AL/al.h>
#endif

namespace spe
{
	namespace al
	{
		/**
		 * Describes a sound source capable of playing a sound.		 
		 */
		class AbstractALSource
		{
		public:
			AbstractALSource(ALuint id);
			virtual ~AbstractALSource();

			void Play();
			void Pause();
			virtual void Rewind();
			void Stop();

			bool IsPlaying();
			bool IsPaused();
			bool IsStopped();

			float GetPitch();
			void SetPitch(float pitch);

			float GetGain();
			void SetGain(float gain);

			void SetLooping(bool loop);
			bool IsLooping();

			void SetNumOfLoops(int numOfLoops);
			int GetNumOfLoops() const;

			ALuint GetRawSource();
		protected:
			ALuint GetSourceState();

			/**
			 * The id provided for this source by openal.
			 */
			ALuint sourceId;

			/**
			 * The number of loops this source still has.
			 */
			int numOfLoops;

		private:
			//restrict the use of the default const.
			//children should use AbstractALSource(ALuint id).

			AbstractALSource()
			{
			}
		};
	};
};

#endif	/* ABSTRACTALSOURCE_H */
