/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "StreamSource.h"

#include "OGGStream.h"

namespace spe
{

namespace al
{
    StreamSource::StreamSource(ALuint source) : AbstractALSource(source)
    {
        dataStream = nullptr;
        numOfLoops = 0;
    }

    StreamSource::~StreamSource()
    {
        dataStream->Release();
    }

    bool StreamSource::UpdateStream()
    {
        if(GetSourceState() == AL_PLAYING && updatable)
        {
            if(dataStream != nullptr)
            {
                STREAM_UPDATE_RESULT res = dataStream->Update(sourceId, numOfLoops != 0);
                if(res == SUR_LOOP)
                {
                    --numOfLoops;
                }
                else if(res == SUR_EOF)
                {
                    updatable = false;
                    this->Stop();
                }
            }
        }
        else
        {
            int processed;
            //get processed buffers
            alGetSourcei(sourceId, AL_BUFFERS_PROCESSED, &processed);
            if(processed == 2)
            {
                Stop();
            }
        }

        return true;
    }

    void StreamSource::Rewind()
    {
        dataStream->Rewind(sourceId);
        this->Play();
        updatable = true;
    }

    bool StreamSource::Prepare(const char* name)
    {
        if(dataStream != nullptr)
        {
            this->Stop();
            dataStream->UnqueueBuffers(sourceId);
            delete dataStream;
        }

        dataStream = new OGGstream;

        if(dataStream->Open(name))
        {
            return dataStream->InitializeStream(sourceId);
        }
        return false;
    }
}
}
