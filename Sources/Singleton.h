/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef SINGLETON_H
#define	SINGLETON_H

#include "NonCopyable.h"
#include <cstdlib>


/**
 * Using this macro you make a class singleton and non copyable.
 * to use it just place this macro after the first bracket of the class declaration;
 * ex.
 *
 * IWantToBeSingleton.h
 *
 * class IWantToBeSingleton : public ExamplarClass
 * {
 *      SingletonClass(IWantToBeSingleton)
 * public:
 *  ....
 * }
 *
 * IWantToBeSingleton.cpp
 *
 * SingletonInitialization(IWantToBeSingleton);
 *
 * If you need to use a custom Default Constructor use SingletonClassCustomConst
 * instead.
 *
 */
#define SingletonClass(x) public : \
static x* GetInstance(){ \
if(!instance){instance = new x;} return instance;} \
static void DeleteInstance(){if(instance)delete instance;}  \
private: x(){} \
static x* instance; \
x(const x &toCopy); \
x &operator = (const x &toAssign) { return *this; }

#define SingletonClassCustomConst(x) public :\
static x* GetInstance(){\
if(!instance){instance = new x;} \
return instance;}\
static void DeleteInstance(){if(instance)delete instance;}  \
private: x();\
static x* instance;\
x(const x &toCopy);\
x &operator = (const x &toAssign) { return *this; }

/**
 * Used to initialize static instance of the Singleton class.
 */
#define SingletonInitialization(x) x* x::instance = nullptr;


//DEPRICATED
template <class T>
class Singleton : spe::NonCopyable
{
public:
    virtual ~Singleton()
    {
    }


    static T* GetInstance()
    {
        static T* instance = nullptr;

        if(!instance)
        {
            instance = new T;
        }

        return instance;
    }

protected:
    Singleton()
    {
    }
};

#endif	/* SINGLETON_H */
