/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef LUA_SCRIPT_LOGIC_PROTO
/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#define LUA_SCRIPT_LOGIC_PROTO

#include "LuaScriptLogic.h"

#include "Vector2.h"
#include <boost/variant.hpp>
#include <map>
#include <string>

namespace spe
{

namespace EntitySystem
{

    class LuaScriptLogicProto : public LuaScriptLogic
    {
        typedef boost::variant< int, float, bool, std::string, Math::Vector2F > Attribute;

        typedef std::map< std::string, Attribute > AttributeMap;


    private:
        std::string m_scriptPath;

        AttributeMap m_attributes;
    public:

        LuaScriptLogicProto(const std::string& scriptPath);

        ~LuaScriptLogicProto();


        /**
         * Pushes an attribute for the LuaState. Only copies of this prototype
         * will attain the attribute.
         */

        template <class T>
        bool PushAttribute(const std::string& name, const T& value)
        {
            Attribute att = value;

            m_attributes.insert(m_attributes.end(), std::pair< std::string, Attribute >(name, att));

            return true;
        }

        /**
         * Returns a LuaScriptLogic object based on this prototype.
         */

        virtual ScriptLogic* Clone();


        virtual void SetParent(Entity* parent);
    };

};

};

#endif