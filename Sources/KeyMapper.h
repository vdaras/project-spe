/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef KEYMAPPER_H
#define	KEYMAPPER_H

#include <map>
#include <SDL/SDL.h>
#include <string>


namespace spe
{

/**
 * Class that maps SDL Keys to strings.
 */

struct InputKey
{
    SDLKey key;
    SDLMod mod;
};

struct CompareKeys
{
    bool operator()(const InputKey& a,const InputKey& b) const
    {
        return (a.key + 512 * a.mod) < (b.key + 512 * b.mod);
    }
};

class KeyMapper
{
    typedef std::map< std::string ,SDLKey> KeyMap;
    typedef std::map< InputKey, std::string,CompareKeys > AssociationMap;
    typedef std::map< std::string,SDLMod > ModMap;

private:
    AssociationMap m_keyAssociations;
    KeyMap m_enumMap;
    ModMap m_modMap;
    SDLMod validMods;
public:

    KeyMapper();

    ~KeyMapper();


    /**
     * Creates a map associating sdlk values with a literal
     */
    void CreateAsciiMap();

    /**
     * Creates a map associating sdlmod values with a literal
     */
    void CreateModMap();

    /**
     * Reads key map file
     */
    bool ReadFile(char const* group = nullptr);

    /**
     * Associates an SDLKey with a string. E.g associate SDLK_SPACE
     * with "JUMP".
     *
     * @param association - the string to associate the key with
     * @param key - the key to associate (part 1 of the key)
     * @param mod - the mod to associate (part 2 of the key)
     */
    void AssociateKey(const std::string& association, SDLKey key, SDLMod mod = KMOD_NONE);


    /**
     * Returns the string that the key SDLkey is mapped to.
     *
     * @param retval - OUTPUT parameter, stores the associated string in here.
     * @param key
     * @param mod
     * @return a boolean value indicating if the key SDLKey is mapped to a value
     */

    bool MapKey(std::string& retval,SDLKey key,SDLMod mod/*= NONE*/) const;

    SDLMod GetValidMods() const;
};

};

#endif	/* KEYMAPPER_H */

