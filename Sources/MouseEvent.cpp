/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "MouseEvent.h"

namespace spe
{

/**
 * Constructor that initializes all the MouseEvent members.
 *
 * @param btn: button used that fired of the event.
 * @param type: event's type.
 * @param occurredX: cursor's absolute coordinate X when the MouseEvent occurred.
 * @param occurredY: cursor's absolute coordinate Y when the MouseEvent occurred.
 * @param relX: cursor's coordinate X, relative to its previous one.
 * @param relY: cursor's coordinate Y, relative to its previous one.
 */

MouseEvent::MouseEvent(MouseButton btn, MouseEventType type, int occurredX, int occurredY, int relX, int relY)
    :
    m_buttonUsed(btn),
    m_evtType(type),
    m_occurredX(occurredX),
    m_occurredY(occurredY),
    m_relativeX(relX),
    m_relativeY(relY)
{
}


MouseEvent::~MouseEvent()
{
}


/**
 * @return the button that triggered this mouse event.
 */

MouseButton MouseEvent::GetButtonUsed() const
{
    return m_buttonUsed;
}


/**
 * @return the mouse event type.
 */

MouseEventType MouseEvent::GetType() const
{
    return m_evtType;
}


/**
 * Returns the coordinates where the mouse event occurred.
 *
 * @param x: OUTPUT parameter, x coordinate is returned in here.
 * @param y: OUTPUT parameter, y coordinate is returned in here.
 */

void MouseEvent::GetOccurredCoordinates(int* x, int* y) const
{
    (*x) = m_occurredX;
    (*y) = m_occurredY;
}


/**
 * Returns the relative coordinates of the event. If the event's
 * type is not MOUSE_MOTION then the relative motion values are
 * -1, -1.
 *
 * @param x: OUTPUT parameter, x relative coordinate is returned in here.
 * @param y: OUTPUT parameter, y relative coordinate is returned in here.
 */

void MouseEvent::GetRelativeCoordinates(int* x, int* y) const
{
    if(m_evtType == MOUSE_MOTION)
    {
        (*x) = m_relativeX;
        (*y) = m_relativeY;
    }
    else
    {
        (*x) = -1;
        (*y) = -1;
    }
}


/**
 * Sets the button used to trigged this event.
 *
 * @param buttonUsed: the button used.
 */

void MouseEvent::SetButtonUsed(MouseButton buttonUsed)
{
    m_buttonUsed = buttonUsed;
}


/**
 * Sets the type of this mouse event.
 *
 * @param type: type of the mouse event.
 */

void MouseEvent::SetType(MouseEventType type)
{
    m_evtType = type;
}


/**
 * Sets the coordinates of this mouse event's occurrence.
 *
 * @param x: coordinate x.
 * @param y: coordinate y.
 */

void MouseEvent::SetOccurredCoordinates(int x, int y)
{
    m_occurredX = x;
    m_occurredY = y;
}


/**
 * Sets the relative coordinates of this mouse event's
 * occurrence.
 */

void MouseEvent::SetRelativeCoordinates(int relX, int relY)
{
    m_relativeX = relX;
    m_relativeY = relY;
}

}
