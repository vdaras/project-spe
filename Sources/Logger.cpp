/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <ctime>
#include <cstring>
#include "Logger.h"
#include "ext/tinyxml/tinyxml.h"

#ifdef __WIN32
#include <direct.h>
#endif

namespace spe
{

SingletonInitialization(Logger)

const char* channelNames[] =
{
    "logs/main",
    "logs/loading",
    "logs/audio",
    "logs/graphics",
    "logs/scripting",
    "logs/physics"
};

Logger::~Logger()
{
    FlushLogs();
    for(int i = 0; i < CHANNEL_COUNT; i++)
    {
        m_channels[i].close();
    }
}

void Logger::Clear(int channel)
{
    LogFileStream* file = &m_channels[channel];

    file->close();
    file->open(channelNames[channel], std::ios_base::trunc);
}

LogFileStream& Logger::Get(Channel channel /*= CHANNEL_MAIN*/, const char* level, bool chained)
{
    LogFileStream& stream = m_channels[channel];

    if(!stream.IsTerminated() && !chained)
    {
        //HTML ONLY
        if(m_htmlFormat)
            stream << CloseTag;
        stream.DeclareAsTerminated();
    }

    if(stream.IsTerminated())
    {
        //HTML ONLY
        if(m_htmlFormat)
        {
            stream << "<div class=\"" << level << "\">\n" << NowTaged;
        }
        else
        {
            stream <<"\n" << Now << ": "<<level;
        }

        stream.DeclareAsUnterminated();
    }

    if(m_previousChannel != channel && m_previousChannel != CHANNEL_COUNT && !m_channels[m_previousChannel].IsTerminated())
    {
        //HTML ONLY
        m_channels[m_previousChannel] << CloseTag;
        m_channels[m_previousChannel].DeclareAsTerminated();
        m_previousChannel = channel;
    }

    return stream;
}

bool Logger::InitializeFiles(bool html,bool append)
{

#if __WIN32
    _mkdir("./logs");
#endif

    for(int i = 0; i < CHANNEL_COUNT; i++)
    {
        std::string name = channelNames[i];
        name += (html ? ".html":".txt");
        m_channels[i].open(name.c_str(), append ? std::ios_base::ate : std::ios_base::out);
        m_channels[i].Initialize(name.c_str(),html);
        m_channels[i].SetChanWrite(false);
        m_channels[i].DeclareAsTerminated();
    }

    m_htmlFormat = html;
    m_previousChannel = CHANNEL_COUNT;

    return true;
}

void Logger::FlushLogs()
{
    for(int i = 0; i < CHANNEL_COUNT; i++)
    {
        if(!m_channels[i].IsTerminated())
        {
            //HTML ONLY
            if(m_htmlFormat)
                m_channels[i] << CloseTag;
            m_channels[i].DeclareAsTerminated();
        }
        //
        m_channels[i].flush();
    }

}
	
}
