/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "Property.h"

namespace spe
{

namespace EntitySystem
{

    Property::Property()
    {
    }

    Property::Property(const Property& orig) : m_property(orig.m_property)
    {

    }

    Property::~Property()
    {

    }

    /**
     * Stores an integer in this property.
     *
     * @param x: to store.
     */

    void Property::StoreInt(int x)
    {
        m_property.operator=(x);
    }

    /**
     * Interprets this property as an integer.
     *
     * @return contents of this property as an integer.
     */

    int Property::AsInt()
    {
        return boost::get< int >(m_property);
    }

    /**
     * Stores a float in this property.
     *
     * @param x: to store.
     */

    void Property::StoreFloat(float x)
    {
        m_property = x;
    }

    /**
     * Interprets this property as a float.
     *
     * @return contents of this property as a float.
     */

    float Property::AsFloat()
    {
        return boost::get< float >(m_property);
    }

    /**
     * Stores a string in this property.
     */

    void Property::StoreString(const std::string& str)
    {
        m_property = str;
    }

    /**
     * Interprets this property as a string.
     *
     * @return contents of this property as a string.
     */

    std::string Property::AsString()
    {
        return boost::get< std::string > (m_property);
    }

    /**
     * Stores a Vector in this property.
     */

    void Property::StoreVector(const Math::Vector2F& vector)
    {
        m_property = vector ;
    }

    /**
     * Interprets this property as a Vector2F.
     *
     * @return contents of this property as a Vector2F.
     */

    Math::Vector2F* Property::AsVector()
    {
        return &boost::get< Math::Vector2F > (m_property);
    }
};
};