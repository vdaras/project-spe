/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef VALUEREPOSITORY_H
#define	VALUEREPOSITORY_H

#include "sdl/GLColor.h"
#include "Vector2.h"
#include "Rect.h"

#include <map>
#include <list>
#include <string>
#include <boost/variant.hpp>
#include <algorithm>

namespace spe
{

/**
 * Stores all values loaded from value files via xml inclusion.
 */
class ValueRepository
{
public:

    ValueRepository()
    {
    }

    virtual ~ValueRepository()
    {
    }

    void Clear()
    {
        values.clear();
    }

    /**
     * Checks if specified file is already loaded and if not it marks it as loaded.
     * @param file
     * @return true if file is loaded, false otherwise.
     */
    bool FileLoaded(const std::string& file)
    {
        std::list<std::string>::iterator it = std::find(loadedFiles.begin(), loadedFiles.end(), file);
        if(it == loadedFiles.end())
        {
            loadedFiles.push_back(file);
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Registers the variable file.name of type T with the specified value.
     * @param file
     * @param name
     * @param value
     * @return
     */
    template <typename T>
    bool RegisterValue(const std::string& file, const std::string& name, const T& value)
    {
        std::string key = file + "." + name;
        ValueMap::iterator it = values.find(key);

        bool overwrite = (it == values.end());
#ifdef REPO_NO_OVERWRITE
        if(!overwrite)
#endif
            values[key] = value;

        return overwrite;
    }

    /**
     * Returns true or false if the requested value exists. It it exists it can be retrieved
     * via the value reference.
     * @param key
     * @param value
     * @return
     */
    template <typename T>
    bool GetValue(const std::string& key, T& value)
    {
        ValueMap::iterator it = values.find(key);
        if(it == values.end())
        {
            return false;
        }

        value = boost::get<T> (it->second);
        return true;
    }

private:
    typedef boost::variant<int, float, bool, std::string, gl::Color, Math::Vector2F, Math::Rect> Value;
    typedef std::map<std::string, Value> ValueMap;

    /**
     * A map with all the loaded values.
     */
    ValueMap values;

    /**
     * A list containing all loaded files.
     */
    std::list<std::string> loadedFiles;
};

};


#endif	/* VALUEREPOSITORY_H */

