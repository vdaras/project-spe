/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef LOGGER_H
#define	LOGGER_H

#include "LogFileStream.h"
#include "Singleton.h"

//macros used for easier user of logger
//use them instead of Logger::GetInstance().Log(Logger::CHANNEL_MAIN,LogFileStream::LEVEL_NOTE)
//ex. LOGNOTE << "Note";
#define LOG(x,y) Logger::GetInstance()->Get(x,y,false).Log()
#define LOGCHAINED(x,y) Logger::GetInstance()->Get(x,y,true).Log()

#define LOGNOTE Logger::GetInstance()->Get(Logger::CHANNEL_MAIN,"Note",false).Log()
#define LOGERROR Logger::GetInstance()->Get(Logger::CHANNEL_MAIN,"Error",false).Log()
#define LOGWARNING Logger::GetInstance()->Get(Logger::CHANNEL_MAIN,"Warning",false).Log()

#define LOGCHAIN_NOTE Logger::GetInstance()->Get(Logger::CHANNEL_MAIN,"Note",true).Log()
#define LOGCHAIN_ERROR Logger::GetInstance()->Get(Logger::CHANNEL_MAIN,"Error",true).Log()
#define LOGCHAIN_WARNING Logger::GetInstance()->Get(Logger::CHANNEL_MAIN,"Warning",true).Log()

//macro used to get file and line number
//DEBUG must be defined
#ifdef DEBUG
#define SOURCE __FILE__ << " : (" <<__LINE__ << ")"
#else
#define SOURCE ""
#endif

//macro used to print out the time and date of the build
#define BUILD_TIME __DATE__ <<" : " << __TIME__

//macro used to print out the actuall literal of an expresion and its value
//ex. ECHO_EXPR(foo[i].bar()) foo[i].bar() = 25
#define ECHO_EXP(x) #x " = " << x << " "

//macro used as the above but prints a different literal than the expr
#define TAG_EXP(x,y) #x " = " << y << " "

namespace spe
{
	/**
	 * Use this class to log events and values. Everything logged using this class will be written to files, one per channel.
	 * One example of use could be:
	 * Logger::GetInstance().Log(Logger::CHANNEL_MAIN,LogFileStream::LEVEL_NOTE) << "lollercoaster";
	 */
	class Logger
	{
		SingletonClass(Logger)
	public:

		/**
		 * The ids of the channels.
		 */
		enum Channel
		{
			CHANNEL_MAIN,
			CHANNEL_LOADING,
			CHANNEL_AUDIO,
			CHANNEL_GRAPHICS,
			CHANNEL_SCRIPTING,
			CHANNEL_PHYSICS,

			/**
			 * The total number of channels.
			 */
			 CHANNEL_COUNT
		};

		~Logger();

		/**
		 * Creates the log directory and initializes the log files.
		 * @param append
		 * @return
		 */
		bool InitializeFiles(bool html, bool append);

		void FlushLogs();

		/**
		 * Returns the specified channel stream.
		 * @param channel The id of the channel. Use the Logger::CHANNEL_* enum values.
		 * @return The requested channel's LogFileStream.
		 */
		LogFileStream& Get(Channel channel, const char* level, bool chained);

		/**
		 * Deletes all content of the specified channel.
		 * @param channel The id of the channel to clear. Use the Logger::CHANNEL_* enum values.
		 */
		void Clear(int channel);

	private:

		/**
		 * The channel streams.
		 */
		LogFileStream m_channels[CHANNEL_COUNT];

		/**
		 * The names of the channels.
		 */
		char* channelNames_[CHANNEL_COUNT];

		Channel m_previousChannel;

		bool m_htmlFormat;
	};
};

#endif	/* LOGGER_H */