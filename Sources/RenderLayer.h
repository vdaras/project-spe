/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _RENDERLAYER_H
#define	_RENDERLAYER_H

#include "Drawable.h"
#include <vector>
#include <string>

namespace spe
{

/*
 * Class representing a drawing layer, layers are used by the rendering manager
 * in order to achieve rendering priority.
 */

class RenderLayer
{
private:
    std::vector<Drawable*> m_drawables;
    bool m_visible;
    float m_factor; // posa pixels toy this adistoixoyn sto reference layer

public:
    RenderLayer();
    RenderLayer(float distance);
    ~RenderLayer();

    void AddDrawable(Drawable* toAdd);
    void RemoveDrawable(Drawable* toRemove);
    void Render(sdl::GraphicsCore* gCore, Camera* camera = nullptr);
    void SetVisibility(bool sVisible);
    bool ContainsDrawable(const Drawable* toTest) const;
    void SortDrawables();
    float GetFactor();
};

};

#endif	/* _RENDERLAYER_H */

