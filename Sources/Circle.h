/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _CIRCLE_H
#define	_CIRCLE_H

#include "Shape.h"

namespace spe
{

namespace Math
{
//class predefinitions
    class Rect;

    class Circle : public Shape
    {
    public:

        Circle()
        {
        }

        ~Circle()
        {
        }
        Circle(const Vector2F& cent, float radious);
        Circle(float centerX, float centerY, float radious);
        Circle(const Circle& orig);

        virtual Shape* Clone();

        /* Methods which return the distance between this and other shapes*/
        virtual Vector2Templ<float> DistanceClosestPoint(const Shape& shape) const;
        virtual Vector2Templ<float> DistanceClosestPoint(const Vector2F& vect) const;
		
        /*return true if this intersects with the shape*/
        /*touch return false*/
        virtual bool Intersect(const Shape& shape) const;
        virtual bool Intersect(const Vector2I& vec) const;
        virtual bool Intersect(const Vector2F& vec) const;
        virtual bool Intersect(const Vector2D& vec) const;
		
        /*the extended Intersection method proxies*/
        /*for more info look @ Math.h */
        virtual IntersectionType Intersect(const Shape& shape, Vector2Templ<float>& axisInfo)  const;
		
        /*returns the area of the circle*/
        virtual float CalculateArea()const;

        virtual inline void SetCenter(const Vector2F& center)
        {
            this->center = center;
        }

        inline void SetCenter(const float x, const float y)
        {
            this->center.Set(x, y);
        }

        virtual Vector2F GetCenter() const
        {
            return center.ConvertTo<float>();
        }

        inline void SetRadious(const float radious)
        {
            this->radious = radious;
        }

        inline float GetRadious() const
        {
            return radious;
        }
		
    protected:
        //protected methods used for double dispatching
        //look at Shape.h for more info
        virtual bool IntersectWith(const Circle& circle) const;
        virtual bool IntersectWith(const Rect& rect) const;
        virtual IntersectionType IntersectWith(const Circle& circle, Vector2Templ<float>& axisInfo) const;
        virtual IntersectionType IntersectWith(const Rect& rect, Vector2Templ<float>& axisInfo) const;
        virtual Vector2Templ<float> DistanceClosestPointFrom(const Rect& rect) const;
        virtual Vector2Templ<float> DistanceClosestPointFrom(const Circle& circle) const;
    private:
	
		//the radius of the circle
        float radious; 
		
		//the center of the circle
        Math::Vector2F center; 
    };

}; //namespace

};

#endif	/* _CIRCLE_H */
