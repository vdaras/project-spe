/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "Tile.h"
#include "Tileset.h"
#include <cstdlib>

namespace spe
{

Tile::Tile(unsigned int sGid,unsigned long flag /* = 0*/)
{
    m_flipX = sGid & maskX;
    m_flipY = sGid & maskY;
    m_gid = sGid;
    m_tileset = nullptr;
    this->m_flag = flag;
}

Tile::~Tile()
{
}

void Tile::SetRect(unsigned x, unsigned y, unsigned width, unsigned height)
{
    m_clip.Set(static_cast<int>(y), static_cast<int>(x), static_cast<int>(height), static_cast<int>(width));
}

void Tile::SetRect(const Math::Rect& rect)
{
    m_clip = rect;
}

void Tile::SetGid(int sGid)
{
    m_gid = sGid;
}

int Tile::GetTilesetID() const
{
    int sGid = m_gid;
    if(m_flipX)
        sGid = sGid ^ maskX;
    if(m_flipY)
        sGid = sGid ^ maskY;
    return sGid;
}

void Tile::SetTileset(Tileset* sTileset)
{
    m_tileset = sTileset;
}

Tileset* Tile::GetTileset()
{
    return m_tileset;
}

int Tile::GetGid() const
{
    return m_gid;
}

const Math::Rect& Tile::GetClip() const
{
    return m_clip;
}

bool Tile::IsFlipY() const
{
    return m_flipY;
}

bool Tile::IsFlipX() const
{
    return m_flipX;
}

}
