/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef MOUSE_EVENT_H
#define MOUSE_EVENT_H

#include "Event.h"

namespace spe
{

enum MouseButton
{
    MBTN_LEFT,
    MBTN_RIGHT,
    MBTN_MIDDLE,
    MBTN_OTHER,
    MBTN_NONE
};

enum MouseEventType
{
    MOUSE_CLICKED,
    MOUSE_RELEASED,
    MOUSE_MOTION,
    MOUSE_EVENT_NONE
};


class MouseEvent : public Event
{
private:
    MouseButton m_buttonUsed;

    MouseEventType m_evtType;

    int m_occurredX, m_occurredY;

    int m_relativeX, m_relativeY;

public:

    /**
     * Constructor that initializes all the MouseEvent members.
     *
     * @param btn: button used that fired of the event.
     * @param type: event's type.
     * @param occurredX: cursor's absolute coordinate X when the MouseEvent occurred.
     * @param occurredY: cursor's absolute coordinate Y when the MouseEvent occurred.
     * @param relX: cursor's coordinate X, relative to its previous one.
     * @param relY: cursor's coordinate Y, relative to its previous one.
     */

    MouseEvent(MouseButton btn, MouseEventType type, int occurredX, int occurredY, int relX = -1, int relY = -1);


    ~MouseEvent();


    /**
     * @return the button that triggered this mouse event.
     */

    MouseButton GetButtonUsed() const;


    /**
     * @return the mouse event type.
     */

    MouseEventType GetType() const;


    /**
     * Returns the coordinates where the mouse event occurred.
     *
     * @param x: OUTPUT parameter, x coordinate is returned in here.
     * @param y: OUTPUT parameter, y coordinate is returned in here.
     */

    void GetOccurredCoordinates(int* x, int* y) const;


    /**
     * Returns the relative coordinates of the event. If the event's
     * type is not MOUSE_MOTION then the relative motion values are
     * -1, -1.
     *
     * @param x: OUTPUT parameter, x relative coordinate is returned in here.
     * @param y: OUTPUT parameter, y relative coordinate is returned in here.
     */

    void GetRelativeCoordinates(int* x, int* y) const;


    /**
     * Sets the button used to trigged this event.
     *
     * @param buttonUsed: the button used.
     */

    void SetButtonUsed(MouseButton buttonUsed);


    /**
     * Sets the type of this mouse event.
     *
     * @param type: type of the mouse event.
     */

    void SetType(MouseEventType type);


    /**
     * Sets the coordinates of this mouse event's occurrence.
     *
     * @param x: coordinate x.
     * @param y: coordinate y.
     */

    void SetOccurredCoordinates(int x, int y);


    /**
     * Sets the relative coordinates of this mouse event's
     * occurrence.
     */

    void SetRelativeCoordinates(int x, int y);
};

};

#endif