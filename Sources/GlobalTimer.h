/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef GLOBALTIMER_H
#define	GLOBALTIMER_H

#include "Singleton.h"
#include "sdl/Timer.h"

namespace spe
{

/**
 * Class that encapsulates the engines time data.
 */
class GlobalTimer : public Singleton< GlobalTimer >
{
private:
    Uint32 m_lastUpdate;
    Uint32 m_delta;

public:

    ~GlobalTimer();

    /**
     * Notifies the Global Timer that a new frame has started
     */

    void MarkFrameStart();


    /**
     * @return how much time has passed since the last frame.
     */

    Uint32 GetDeltaTime();

protected:
    friend class Singleton< GlobalTimer >;

    GlobalTimer();
};

};

#endif	/* GLOBALTIMER_H */

