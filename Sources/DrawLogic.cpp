/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "DrawLogic.h"
#include "Entity.h"
#include "Camera.h"
#include "sdl/GraphicsCore.h"
#include "PhysicsLogic.h"

namespace spe
{
	namespace EntitySystem
	{
		DrawLogic::DrawLogic()
		{
			this->parent = nullptr;
		}

		DrawLogic::DrawLogic(const DrawLogic& orig) :Drawable(orig)
		{
			this->parent = nullptr;
		}

		DrawLogic::~DrawLogic()
		{
		}

		bool DrawLogic::IsVisible(const Camera& camera) const
		{
			if (!m_visible) return false;
			int posX = parent->GetPositionX();
			int posY = parent->GetPositionY();
			return (camera.GetRect()->GetLeft() <= posX + this->GetWidth() * GetScale().GetX() &&
				camera.GetRect()->GetTotalWidth() >= posX) &&
				(camera.GetRect()->GetTop() <= posY + this->GetHeight() * GetScale().GetY() &&
				camera.GetRect()->GetTotalHeight() >= posY);
		}

		void DrawLogic::Draw(sdl::GraphicsCore* gCore, Camera* camera)
		{
			if (m_display != nullptr)
			{
				gCore->Render(*m_display->GetRawData(), m_clip, m_color, parent->GetPosition(), m_scale, m_angle, m_flip, m_blending);
			}
		}

		Math::Vector2I DrawLogic::GetDisplayDimensions()
		{
			return Math::Vector2I((int)m_display->GetRawData()->GetWidth(), (int)m_display->GetRawData()->GetHeight());
		}

		int DrawLogic::GetX() const
		{
			return parent->GetPositionX();
		}

		void DrawLogic::SetX(int newX)
		{
			parent->SetPosition(newX, parent->GetPositionY());
		}

		int DrawLogic::GetY() const
		{
			return parent->GetPositionY();
		}

		void DrawLogic::SetY(int newY)
		{
			parent->SetPosition(parent->GetPositionX(), newY);
		}

		Math::Vector2F DrawLogic::GetPosition() const
		{
			return parent->GetPosition();
		}

		void DrawLogic::SetPosition(const Math::Vector2F& position)
		{
			parent->SetPosition(position);
		}

		DrawLogic* DrawLogic::Clone()
		{
			return new DrawLogic(*this);
		}

		int DrawLogic::GetWidth() const
		{
			return m_display ? this->m_display->GetRawData()->GetWidth() : 0;
		}

		int DrawLogic::GetHeight() const
		{
			return m_display ? this->m_display->GetRawData()->GetHeight() : 0;
		}
	}
}