/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PROPERTY_H_
#define PROPERTY_H_

#include <boost/variant.hpp>

#include "Vector2.h"
#include <string>

namespace spe
{
	namespace EntitySystem
	{
		typedef boost::variant< int, float, std::string, Math::Vector2F > _property;

		class Property
		{
		public:

			Property();

			Property(const Property& orig);

			virtual ~Property();

			/**
			 * Stores an integer in this property.
			 *
			 * @param x: to store.
			 */

			void StoreInt(int x);

			/**
			 * Interprets this property as an integer.
			 */

			int AsInt();

			/**
			 * Stores a float in this property.
			 *
			 * @param x: to store.
			 */

			void StoreFloat(float x);

			/**
			 * Interprets this property as a float.
			 *
			 * @return contents of this property as a float.
			 */

			float AsFloat();

			/**
			 * Stores a string in this property.
			 */

			void StoreString(const std::string& str);

			/**
			 * Interprets this property as a string.
			 *
			 * @return contents of this property as a string.
			 */

			std::string AsString();

			/**
			 * Stores a Vector in this property.
			 */

			void StoreVector(const Math::Vector2F& vector);

			/**
			 * Interprets this property as a Vector2F.
			 *
			 * @return contents of this property as a Vector2F.
			 */

			Math::Vector2F* AsVector();
		private:

			_property m_property;
		};
	};
};

#endif