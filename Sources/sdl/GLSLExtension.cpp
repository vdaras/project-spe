/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "GLSLExtension.h"

PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB = nullptr;
PFNGLDELETEOBJECTARBPROC glDeleteObjectARB = nullptr;
PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB = nullptr;
PFNGLSHADERSOURCEARBPROC glShaderSourceARB = nullptr;
PFNGLCOMPILESHADERARBPROC glCompileShaderARB = nullptr;
PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB = nullptr;
PFNGLATTACHOBJECTARBPROC glAttachObjectARB = nullptr;
PFNGLGETINFOLOGARBPROC glGetInfoLogARB = nullptr;
PFNGLLINKPROGRAMARBPROC glLinkProgramARB = nullptr;
PFNGLUSEPROGRAMOBJECTARBPROC glUseProgramObjectARB = nullptr;
PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB = nullptr;

PFNGLUNIFORM1FARBPROC glUniform1fARB = nullptr;
PFNGLUNIFORM2FARBPROC glUniform2fARB = nullptr;
PFNGLUNIFORM3FARBPROC glUniform3fARB = nullptr;
PFNGLUNIFORM4FARBPROC glUniform4fARB = nullptr;

PFNGLUNIFORM1IARBPROC glUniform1iARB = nullptr;
PFNGLUNIFORM2IARBPROC glUniform2iARB = nullptr;
PFNGLUNIFORM3IARBPROC glUniform3iARB = nullptr;
PFNGLUNIFORM4IARBPROC glUniform4iARB = nullptr;

PFNGLUNIFORM1FVARBPROC glUniform1fvARB = nullptr;
PFNGLUNIFORM2FVARBPROC glUniform2fvARB = nullptr;
PFNGLUNIFORM3FVARBPROC glUniform3fvARB = nullptr;
PFNGLUNIFORM4FVARBPROC glUniform4fvARB = nullptr;

PFNGLUNIFORM1IVARBPROC glUniform1ivARB = nullptr;
PFNGLUNIFORM2IVARBPROC glUniform2ivARB = nullptr;
PFNGLUNIFORM3IVARBPROC glUniform3ivARB = nullptr;
PFNGLUNIFORM4IVARBPROC glUniform4ivARB = nullptr;

#define INITIALIZE_FN_PTR(x,y) x = (y)(SDL_GL_GetProcAddress(#x))

bool InitializeGLSL()
{
    //initialize all function pointers to the needed functions.
    INITIALIZE_FN_PTR(glCreateProgramObjectARB, PFNGLCREATEPROGRAMOBJECTARBPROC);
    INITIALIZE_FN_PTR(glDeleteObjectARB, PFNGLDELETEOBJECTARBPROC);
    INITIALIZE_FN_PTR(glCreateShaderObjectARB, PFNGLCREATESHADEROBJECTARBPROC);
    INITIALIZE_FN_PTR(glShaderSourceARB, PFNGLSHADERSOURCEARBPROC);
    INITIALIZE_FN_PTR(glCompileShaderARB, PFNGLCOMPILESHADERARBPROC);
    INITIALIZE_FN_PTR(glGetObjectParameterivARB, PFNGLGETOBJECTPARAMETERIVARBPROC);
    INITIALIZE_FN_PTR(glAttachObjectARB, PFNGLATTACHOBJECTARBPROC);
    INITIALIZE_FN_PTR(glGetInfoLogARB, PFNGLGETINFOLOGARBPROC);
    INITIALIZE_FN_PTR(glLinkProgramARB, PFNGLLINKPROGRAMARBPROC);
    INITIALIZE_FN_PTR(glUseProgramObjectARB, PFNGLUSEPROGRAMOBJECTARBPROC);
    INITIALIZE_FN_PTR(glGetUniformLocationARB, PFNGLGETUNIFORMLOCATIONARBPROC);

    INITIALIZE_FN_PTR(glUniform1fARB, PFNGLUNIFORM1FARBPROC);
    INITIALIZE_FN_PTR(glUniform2fARB, PFNGLUNIFORM2FARBPROC);
    INITIALIZE_FN_PTR(glUniform3fARB, PFNGLUNIFORM3FARBPROC);
    INITIALIZE_FN_PTR(glUniform4fARB, PFNGLUNIFORM4FARBPROC);

    INITIALIZE_FN_PTR(glUniform1iARB, PFNGLUNIFORM1IARBPROC);
    INITIALIZE_FN_PTR(glUniform2iARB, PFNGLUNIFORM2IARBPROC);
    INITIALIZE_FN_PTR(glUniform3iARB, PFNGLUNIFORM3IARBPROC);
    INITIALIZE_FN_PTR(glUniform4iARB, PFNGLUNIFORM4IARBPROC);

    INITIALIZE_FN_PTR(glUniform1fvARB, PFNGLUNIFORM1FVARBPROC);
    INITIALIZE_FN_PTR(glUniform2fvARB, PFNGLUNIFORM2FVARBPROC);
    INITIALIZE_FN_PTR(glUniform3fvARB, PFNGLUNIFORM3FVARBPROC);
    INITIALIZE_FN_PTR(glUniform4fvARB, PFNGLUNIFORM4FVARBPROC);

    INITIALIZE_FN_PTR(glUniform1ivARB, PFNGLUNIFORM1IVARBPROC);
    INITIALIZE_FN_PTR(glUniform2ivARB, PFNGLUNIFORM2IVARBPROC);
    INITIALIZE_FN_PTR(glUniform3ivARB, PFNGLUNIFORM3IVARBPROC);
    INITIALIZE_FN_PTR(glUniform4ivARB, PFNGLUNIFORM4IVARBPROC);

    //make sure that all functions were loaded properly.
    return(glCreateProgramObjectARB && glDeleteObjectARB &&
           glCreateShaderObjectARB && glShaderSourceARB &&
           glCompileShaderARB && glGetObjectParameterivARB &&
           glAttachObjectARB && glGetInfoLogARB && glLinkProgramARB &&
           glUseProgramObjectARB && glGetUniformLocationARB &&
           glUniform1fARB && glUniform1iARB);
}
