/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef CANVAS_H
#define	CANVAS_H

#include "Blittable.h"

/**
 * Class representing an empty Surface. The empty surface is filled with a
 * background color. This class' buffer can never be nullptr.  So:
 * - no default constructor is provided.
 * - the provided constructor throws an exception if a surface could not be allocated
 *   for some reason.
 * - method for creating a surface is private.
 */

namespace spe
{

namespace sdl
{

    class Color;

    class Canvas : public Blittable
    {
    public:
        Canvas(int width, int height);
        Canvas(int width, int height, const Color& background);
        ~Canvas();
        bool Render();

        /**
         * Fills the canvas with transparent pixels.
         */

        void FillTransparent();

    private:
        bool Create(int width,int height);

        bool Create(int width, int height, const Color& background);
    };

};

};

#endif	/* CANVAS_H */

