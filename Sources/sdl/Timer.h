/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef TIMER_H
#define	TIMER_H

#include "SDL/SDL_timer.h"

/*
 * Class wrapping up SDL_Timer functionality
 */

namespace spe
{

namespace sdl
{

    class Timer
    {
    private:
        Uint32 m_ticks;
        Uint32 m_pausedTicks;
        bool m_started;
        bool m_paused;

    public:
        Timer();
        ~Timer();

        Uint32 GetTicks() const;
        void Start();
        void Stop();
        void Pause();
        void Unpause();
    };

};

};

#endif	/* TIMER_H */

