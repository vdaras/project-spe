/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef IMAGE_H
#define	IMAGE_H

#include "Blittable.h"

#include <string>


 namespace spe
{
 
namespace sdl
{
	/*
	 * Class handling surfaces which load images located on disk. The class' main
	 * constraint is that the buffer should never be nullptr. So:
	 * - no default constructor is provided.
	 * - the provided constructor throws an exception if the image could not be loaded
	 *   for some reason.
	 * - method for opening an image is private.
	 */
    class Image : public Blittable
    {
    public:
        Image(const std::string& path);
        ~Image();

    private:
        bool Open(const std::string& path);
    };

};

};

#endif	/* IMAGE_H */

