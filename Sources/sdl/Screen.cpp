/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Screen.h"

#include <assert.h>
#include <GL/gl.h>
#include <vector>

#include "SDLException.h"

namespace spe
{

namespace sdl
{

    /*
     * Constructor initializes screen. If initilization fails then an Exception is
     * thrown and the application must exit, since it's a critical error
     *
     * @param
     * screenW: screen width
     * screenH: screen height
     * bpp: bits per pixel aka screen depth
     * fullscreen: boolean value indicating if the application runs on fullscreen mode
     */

    Screen::Screen(int screenW, int screenH, int bpp, bool fullscreen, bool opengl)
        :
        m_isFullscreen(fullscreen),
        m_openGL(opengl)
    {
        //try setting up the screen
        if(!init(screenW, screenH, bpp, fullscreen, opengl))
        {
            //if unsuccessful , indicate failure
            throw spe::sdl::SdlException();
        }
    }

    Screen::~Screen()
    {
    }

    /*
     * Flips and updates screen. Returns false if an error
     * occurs.
     */

    bool Screen::Flip()
    {
        if(m_openGL)
        {
            SDL_GL_SwapBuffers();
            return true;
        }
        else
        {
            SDL_Surface* buff = GetBuffer();

            //make sure the screen's surface is valid
            assert(buff != nullptr);

            //update screen, check operation's status
            if(SDL_Flip(buff) == -1)
            {
                //if error code is failure, indicate failure
                return false;
            }

            //indicate success
            return true;
        }
    }

    /**
     * Clears the screen to black color.
     */

    void Screen::Clear()
    {
        if(m_openGL)
        {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        else
        {
            SDL_Surface* buff = GetBuffer();

            //make sure that the screen's surface is valid
            assert(buff != nullptr);

            //fill the screen using black color
            SDL_Rect rect = { 0, 0, GetWidth(), GetHeight() };
            SDL_FillRect(GetBuffer(), &rect, 0);
        }
    }

    /**
     * Initializes screen, returns false if failed.
     *
     * @param
     * - screenW: screen width
     * - screenH: screen height
     * - bpp: bits per pixel aka pixel depth
     * - fullscreen: boolean value indicating if the application runs on fullscreen mode
     */

    bool Screen::init(int screenW, int screenH, int bpp, bool fullscreen, bool opengl)
    {
        //create a temporary primitive surface
        SDL_Surface* screen = nullptr;

        //if we need a fullscreen application
        if(fullscreen)
        {
            //setup a fullscreen doublebuffered surface
            if(opengl)
                screen = SDL_SetVideoMode(screenW, screenH, bpp, SDL_HWSURFACE | SDL_FULLSCREEN | SDL_OPENGL);
            else
                screen = SDL_SetVideoMode(screenW, screenH, bpp, SDL_HWSURFACE | SDL_FULLSCREEN | SDL_DOUBLEBUF);
        }
        else //if not
        {
            //setup a windowed surface
            if(opengl)
                screen = SDL_SetVideoMode(screenW, screenH, bpp, SDL_OPENGL);
            else
                screen = SDL_SetVideoMode(screenW, screenH, bpp, SDL_HWSURFACE);

        }

        //if setting up the screen was successful
        if(screen)
        {
            //set up the current buffer to the new screen
            SetBuffer(screen);
            //indicate success
            return true;
        }

        //setting up screen failed, indicate failure
        return false;
    }

};

};
