/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef VERTEX_H
#define	VERTEX_H

namespace spe
{

namespace gl
{

	/*
	 * A class to represent a 2d vertex point.
	 * To draw this vertex call the Apply method.
	 */
	class Vertex
	{
	public:

		Vertex()
		{}

		Vertex(GLdouble x,GLdouble y)
		{
			xy[0] = x;
			xy[1] = y;
		}

		inline void Apply()
		{
			glVertex2dv(xy);
		}

		inline void SetX(GLdouble x)
		{
			xy[0] = x;
		}

		inline void SetY(GLdouble y)
		{
			xy[1] = y;
		}

		inline void Set(GLdouble x,GLdouble y)
		{
			xy[0] = x;
			xy[1] = y;
		}

		inline GLdouble X()
		{
			return xy[0];
		}

		inline GLdouble Y()
		{
			return xy[1];
		}


	private:
		GLdouble xy[2];
	};
};

};


#endif	/* VERTEX_H */

