/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef FONT_H
#define	FONT_H


#include "SDL/SDL_ttf.h"
#include "../NonCopyable.h"

#include <memory>
#include <string>

/*
 * Class wrapping a TTF_Font. This class main constraint is that the TTF_Font's
 * instance should NEVER be nullptr. So opening is allowed only during construction.
 * Before creating a font, TTF subsystem should be initialized!
 */

namespace spe
{

namespace sdl
{

    class Color;
    class Surface;
    class TextImage;

    class Font : public NonCopyable
    {
    private:
        TTF_Font* m_font;
        int m_fontAscent;
        int m_fontDescent;
        int m_fontHeight;
        int m_fontLineSkip;
        int m_averageAdvance;

    public:
        Font(const std::string& path, int size);
        ~Font();

        //info routines
        int GetAscent();
        int GetDescent();
        int GetHeight();
        int GetLineSkip();
        int GetAverageAdvance();
        void TextSize(const std::string& text, int& width, int& height);
        void GlyphSize(Uint16 glyph, int& minX, int& maxX, int& minY, int& maxY, int& advance);

        bool RenderText(int x, int y, const std::string& text, const Color& color, Surface* target) const;
        bool Valid() const;
        std::unique_ptr<sdl::TextImage> GetImage(const std::string& text, const Color& color) const;

    private:
        bool Open(const std::string& path, int size);
    };

};

};

#endif	/* FONT_H */

