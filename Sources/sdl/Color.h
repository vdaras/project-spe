/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef COLOR_H
#define COLOR_H

#include <SDL/SDL.h>

namespace spe
{

namespace sdl
{

    class Surface;

    class Color
    {

    private:
        Uint8 m_r, m_g, m_b;

    public:
        Color(Uint8 r = 255, Uint8 g = 255, Uint8 b = 255);
        ~Color();

        Uint8 GetR() const;
        Uint8 GetG() const;
        Uint8 GetB() const;

        Uint32 MapRGB(sdl::Surface& surface) const;
        Uint32 MapRGBA(sdl::Surface& surface, Uint32 alpha) const;
    };


    namespace Colors
    {

        const Color RED(255, 0 ,0);
        const Color GREEN(0, 255, 0);
        const Color BLUE(0, 0, 255);
        const Color GRAY(105, 105, 105);
        const Color WHITE(255, 255, 255);
        const Color BLACK(0, 0, 0);

    };

};

};

#endif	/* COLOR_H */

