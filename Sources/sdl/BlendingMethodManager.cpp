/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "BlendingMethodManager.h"

#include "../Utilities.h"

namespace spe
{
	namespace gl
	{
		BlendingMethodManager::BlendingMethodManager()
		{
		}

		BlendingMethodManager::~BlendingMethodManager()
		{
		}

		void BlendingMethodManager::Initialize()
		{
			//enable blending in opengl
			glEnable(GL_BLEND);

			//add the blending methods
			AddMethod(BLEND_ALPHA, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			AddMethod(BLEND_ADD, GL_SRC_ALPHA, GL_ONE);
			AddMethod(BLEND_NONE, GL_ONE, GL_ONE);
			AddMethod(BLEND_FILTER, GL_DST_COLOR, GL_ZERO);
			AddMethod(BLEND_LIGHT, GL_DST_COLOR, GL_ONE);
			AddMethod(BLEND_DARK, GL_ZERO, GL_ONE_MINUS_SRC_COLOR);

			//enable alpha blending
			activeMethod = BLEND_NONE;
			EnableMethod(BLEND_ALPHA);
		}

		void BlendingMethodManager::AddMethod(BlendingMethodName name, GLenum sfactor, GLenum dfactor)
		{
			blendingTable[name].Set(sfactor, dfactor);
		}

		void BlendingMethodManager::EnableMethod(BlendingMethodName method)
		{
			//if requested is unknown use default
			if (method == BLEND_UNKNOWN)
				method = BLEND_ALPHA;

			//if requested method is already active there is nothing to be done
			if (method == activeMethod)
				return;

			//activate requested method
			activeMethod = method;
			blendingTable[method].Enable();
		}
	};
};