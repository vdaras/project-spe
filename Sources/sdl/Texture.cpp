/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Texture.h"
#include "Image.h"

namespace spe
{

namespace gl
{

    Texture::Texture()
    {
        ID = 0;
        width = 0;
        height = 0;
    }

    Texture::~Texture()
    {
        if(ID != 0)
            glDeleteTextures(1, &ID);
    }

    bool Texture::Load(const char* filename)
    {
        sdl::Image* image = new sdl::Image(filename);
        ID = image->ConvertToTexture();

        if(ID != 0)
        {
            width = image->GetWidth();
            height = image->GetHeight();

            delete image;
            return true;
        }
        else
        {
            delete image;
            return false;
        }

    }

    void Texture::Apply() const
    {
        glBindTexture(GL_TEXTURE_2D, ID);
    }

    unsigned int Texture::GetHeight() const
    {
        return height;
    }

    unsigned int Texture::GetWidth() const
    {
        return width;
    }

    unsigned int Texture::GetID() const
    {
        return ID;
    }

};

};
