/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef TEXTURECOORDS_H
#define	TEXTURECOORDS_H

namespace spe
{

namespace gl
{

    class TextureCoords
    {
    public:

        TextureCoords()
        {

        }

        TextureCoords(GLdouble x,GLdouble y)
        {
            xy[0] = x;
            xy[1] = y;
        }

        void Apply()
        {
            glTexCoord2dv(xy);
        }

        void SetX(GLdouble x)
        {
            xy[0] = x;
        }

        void SetY(GLdouble y)
        {
            xy[1] = y;
        }

        void Set(GLdouble x,GLdouble y)
        {
            xy[0] = x;
            xy[1] = y;
        }

        GLdouble X()
        {
            return xy[0];
        }

        GLdouble Y()
        {
            return xy[1];
        }


    private:
        GLdouble xy[2];
    };
};

};


#endif	/* TEXTURECOORDS_H */

