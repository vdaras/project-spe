/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef APPLICATION_H
#define	APPLICATION_H


#include "../audio/AudioCore.h"
#include "../gui/gui_Screen.h"
#include "../NonCopyable.h"
#include "GraphicsCore.h"

namespace spe
{

class Camera;

namespace sdl
{

    class Screen;

    /**
     * Class modeling an SDL application. Provides an interface for further applications
     * to be derived from depending on the situation.
     */

    class Application : public NonCopyable
    {
    protected:

        al::AudioCore* m_aCore;

        GraphicsCore* m_gCore;

        spe::gui::Screen* m_gScreen;

        int m_framesPerSecond; //frame cap

        bool m_running;

    public:
        Application();

        virtual ~Application();

        bool GetRunning();


        /**
         * Initializes the Application's graphics core and gui screen.
         *
         * @param screenW - screen's width
         * @param screenH - screen's height
         * @param bpp - bits per pixel
         * @param fullscreen - if true, the application will be fullscreen
         * @return a boolean value indicating if initialization was successful or
         * not.
         */

        virtual bool Init(int screenW, int screenH, int bpp, bool fullscreen = false);


        /**
         * Override this one to modify the Application's behavior.
         *
         * @return an Application exit code.
         */

        virtual int Run() = 0;


        /**
         * Sets the m_running flag of the Application to false.
         */

        virtual void Exit();

        /**
         * @return A pointer to the Application's GraphicsCore
         */

        GraphicsCore* GetGraphicsCore();


        /**
         * @return A pointer to the application's gui screen.
         */

        spe::gui::Screen* GetGuiScreen();
    };

};

};

#endif	/* APPLICATION_H */

