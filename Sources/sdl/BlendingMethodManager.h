/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef BLENDINGMETHODMANAGER_H
#define	BLENDINGMETHODMANAGER_H


#include <string>

#include "BlendingMethod.h"

namespace spe
{

namespace gl
{

    /**
     * Manages different BlendingMethods by storing enabling and deleting them.
     */
    class BlendingMethodManager
    {
    public:

        BlendingMethodManager();

        ~BlendingMethodManager();

        /**
         * Loads up defined methods.
         * Should be called AFTER the initialization of opengl.
         */
        void Initialize();

        /**
         * Adds a new blending method.
         * @param name
         * @param sfactor
         * @param dfactor
         */
        void AddMethod(BlendingMethodName name, GLenum sfactor, GLenum dfactor);

        /**
         * Enables the selected method.
         * @param method
         */
        void EnableMethod(BlendingMethodName method);

    private:
        BlendingMethodName activeMethod;
        BlendingMethod blendingTable[BLEND_ALL];
    };

};
	
};
#endif	/* BLENDINGMETHODMANAGER_H */

