/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "sdl_utilities.h"

namespace spe
{

namespace sdl
{

    /**
     * Returns whether a given point is inside an SDL_Rect structure or not.
     *
     * @param x: x coordinate of the point.
     * @param y: y coordinate of the point.
     * @param rect: a handle to a rectangle.
     * @return a boolean value indicating if x, y is inside rect/
     */

    bool PointInRect(int x, int y, SDL_Rect* rect)
    {
        return (
                   x >= rect->x &&
                   x <= rect->x + rect->w &&
                   y >= rect->y &&
                   y <= rect->y + rect->h
               );
    }

};

};
