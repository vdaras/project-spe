/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef BLITTABLE_H
#define	BLITTABLE_H

#include "Surface.h"

/*
 * Abstract class providing an interface for surfaces that can blit themselves
 * on other surfaces. This class violates sdl::Surface's encapsulation in some
 * ways but I couldn't come up with a better design :).
 */

namespace spe
{

namespace sdl
{

    class Blittable : public Surface
    {
    public:
        Blittable();
        virtual ~Blittable() = 0;

        void SetAlpha(Uint8 alpha);
        void SetColorKey(const Color& color);
        void Blit(int x, int y, Surface* target, const SDL_Rect* clip = nullptr) const;
    };

};

};
#endif	/* BLITTABLE_H */

