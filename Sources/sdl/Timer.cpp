/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Timer.h"

namespace spe
{

namespace sdl
{

    Timer::Timer()
    {
        m_ticks = 0;
        m_pausedTicks = 0;
        m_started = false;
        m_paused = false;
    }

    Timer::~Timer()
    {
    }

    /*
     * Returns how much ticks have passed since the timer was started
     */

    Uint32 Timer::GetTicks() const
    {
        //if the timer is started
        if(m_started)
        {
            //if the timer is paused
            if(m_paused)
            {
                //return difference between paused ticks and ticks when started
                return m_pausedTicks;
            }
            else //if the timer is not paused
            {
                //return difference between current ticks and ticks when started
                return SDL_GetTicks() - m_ticks;
            }
        }

        return 0;
    }


    /*
     * Sarts the timer
     */

    void Timer::Start()
    {
        //unpause timer, doesn't matter if is or isn't already paused
        m_paused = false;

        //start timer
        m_started = true;

        //get ticks
        m_ticks = SDL_GetTicks();
    }

    /*
     * Stops timer
     */

    void Timer::Stop()
    {
        //unpause timer, doesn't matter if is or isn't already paused
        m_paused = false;

        //stop timer
        m_started = false;
    }

    /*
     * Pauses Timer
     */

    void Timer::Pause()
    {
        //if the timer is started and not paused
        if(m_started && !m_paused)
        {
            //pause timer
            m_paused = true;
            //get paused ticks
            m_pausedTicks = SDL_GetTicks() - m_ticks;
        }
    }

    /*
     * Unpauses Timer
     */

    void Timer::Unpause()
    {
        //if timer is paused
        if(m_paused)
        {
            //unpause timer
            m_paused = false;
            m_ticks = SDL_GetTicks() - m_pausedTicks;
            m_pausedTicks = 0;

        }
    }

};

};

