/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Color.h"

#include "Surface.h"

namespace spe
{

namespace sdl
{

    Color::Color(Uint8 r, Uint8 g, Uint8 b) : m_r(r), m_g(g), m_b(b)
    {
    }

    Color::~Color()
    {
    }

    Uint8 Color::GetR() const
    {
        return m_r;
    }

    Uint8 Color::GetG() const
    {
        return m_g;
    }

    Uint8 Color::GetB() const
    {
        return m_b;
    }

    Uint32 Color::MapRGB(sdl::Surface& surface) const
    {
        Uint32 toReturn = 0;
        SDL_Surface* buffer = surface.GetBuffer();

        if(buffer != nullptr)
        {
            toReturn = SDL_MapRGB(buffer->format, m_r, m_g, m_b);
        }

        return toReturn;
    }

    Uint32 Color::MapRGBA(sdl::Surface& surface, Uint32 alpha) const
    {
        Uint32 toReturn = 0;
        SDL_Surface* buffer = surface.GetBuffer();

        if(buffer != nullptr)
        {
            toReturn = SDL_MapRGBA(buffer->format, m_r, m_g, m_b, alpha);
        }

        return toReturn;
    }

};

};
