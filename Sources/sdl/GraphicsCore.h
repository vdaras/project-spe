/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GRAPHICSCORE_H
#define	GRAPHICSCORE_H

#include "Screen.h"
#include "Canvas.h"
#include "TextureCoords.h"
#include "GLColor.h"
#include "Texture.h"
#include "../Rect.h"
#include "Vertex.h"
#include "GLSLExtension.h"
#include "BlendingMethodManager.h"

#include <list>
#include <string>

namespace spe
{
	namespace sdl
	{
		class GraphicsCore : public NonCopyable
		{
		public:

			GraphicsCore()
			{
				mainScreen = nullptr;
				sdlScreen = nullptr;
			}

			~GraphicsCore();

			/**
			 * Via this method screens and frame buffer is initialized.
			 *
			 * @param screenW
			 * @param screenH
			 * @param bpp
			 * @param fullscreen
			 * @param useSdlScreen
			 * @return
			 */
			bool Initialize(int screenW, int screenH, int bpp, bool fullscreen, bool useSdlScreen);

			/**
			 * The main method of graphics core. This renders the specified arguments onto the
			 * screen as a quad polygon.
			 *
			 * @param texture
			 * @param clip
			 * @param color
			 * @param position
			 * @param scale
			 * @param angle in degrees and clockwise
			 * @param flip
			 * @return
			 */
			bool Render(const gl::Texture& texture, const Math::Rect& clip, const gl::Color& color,
				const Math::Vector2F& position, const Math::Vector2F& scale, float angle, bool flip[], gl::BlendingMethodName blend = gl::BLEND_ALPHA);

			/**
			 * Draws a line from start to end of color color.
			 * @param start
			 * @param end
			 * @param color
			 * @param width
			 * @return
			 */
			bool DrawLine(const Math::Vector2F& start, const Math::Vector2F& end, const gl::Color& color, float width = 5);

			/**
			 * Draws a rectangular with the upper left corner at start and the lower right corner at end filled with color.
			 * @param start
			 * @param end
			 * @param color
			 * @return
			 */
			bool DrawFilledRect(const Math::Vector2F& start, const Math::Vector2F& end, const gl::Color& color);

			/**
			 * Draws a rectangular with the upper left corner at start and the lower right corner at end with no filling.
			 * @param start
			 * @param end
			 * @param color
			 * @param width
			 * @return
			 */
			bool DrawLineRect(const Math::Vector2F& start, const Math::Vector2F& end, const gl::Color& color, float width = 1);

			/**
			 * Instruct the GraphicsCore that the rendering process will begin.
			 * @return true in success
			 */
			bool BeginRendering();

			/**
			 * Inform the GraphicsCore that no more objects will be rendered.
			 * @return true in success
			 */
			bool EndRendering();

			/**
			 * Checks if name opengl extension exists.
			 * @param name
			 * @return
			 */
			bool CheckForExtension(const char* name);

			void CreateExtensionList();

			Canvas* GetSdlScreen() const;

			Screen* GetGlScreen() const;
		protected:
			/**
			 * Creates the texture coordinates to map the given portion of the texture onto the quad.
			 * @param tCoords
			 * @param texture
			 * @param clip
			 * @param flip
			 */
			void TextureCoordinates(gl::TextureCoords tCoords[4], const gl::Texture& texture, const Math::Rect& clip, bool flip[]) const;

			/**
			 * Initializes the given vertices to the correct xy.
			 *
			 * @param vertecies
			 * @param position
			 * @param clip
			 * @param scale
			 */
			void Transorm(gl::Vertex vertecies[4], const Math::Vector2F& position, const Math::Rect& clip, const Math::Vector2F& scale) const;

			/**
			 * Called by Initialize to set opengl const before initializing the framebuffer.
			 */
			void SetOpenglConstants();
		private:

			/**
			 * The frame buffer of the engine.
			 */
			Screen* mainScreen;

			/**
			 * If the screen is in opengl mode this canvas is used for sdl functionality.
			 */
			Canvas* sdlScreen;

			/**
			 * The manager tha keeps track of different blending methods.
			 */
			gl::BlendingMethodManager blengingMethods;

			/**
			 * A list with all the names of the supported opengl extensions.
			 */
			std::list<std::string> extensionList;
		};
	};
};

#endif	/* GRAPHICSCORE_H */
