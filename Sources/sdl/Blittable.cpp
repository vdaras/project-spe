/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Blittable.h"

#include "Color.h"

namespace spe
{

namespace sdl
{

    Blittable::Blittable()
    {
    }

    Blittable::~Blittable()
    {
    }

    /*
     * Sets the SDL_Surface's alpha to the desired value
     *
     * @param
     * - alpha: new alpha value
     */

    void Blittable::SetAlpha(Uint8 alpha)
    {
        if(Valid())
        {
            SDL_Surface* buffer = GetBuffer();

            SDL_SetAlpha(buffer, SDL_SRCALPHA, alpha);
        }
    }

    /*
     * Sets the SDL_Surface's colorkey to the desired value
     *
     * @param
     * - colorkey: new colorkey
     */

    void Blittable::SetColorKey(const Color& color)
    {
        if(Valid())
        {
            SDL_Surface* buffer = GetBuffer();

            Uint32 colorkey = SDL_MapRGB(buffer->format, color.GetR(), color.GetG(), color.GetB());
            SDL_SetColorKey(buffer, SDL_SRCCOLORKEY, colorkey);
        }
    }

    /**
     * Blits the blitable surface's buffer onto another surface's buffer (maybe the screen
     * or another blitable).
     *
     * @param
     * - x, y: blit upper-left coordinates
     * - target: destination surface
     * - clip: rectangle which determines which portion of the blitable should be blitted.
     */

    void Blittable::Blit(int x, int y, Surface* target, const SDL_Rect* clip) const
    {
        if(Valid())
        {
            SDL_Rect rect;
            rect.x = x;
            rect.y = y;

            SDL_Rect* noConstClip = const_cast< SDL_Rect* >(clip);

            SDL_BlitSurface(GetBuffer(), noConstClip, target->GetBuffer(), &rect);
        }
    }

};
	
};
