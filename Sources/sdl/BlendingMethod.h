/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef BLENDINGMETHOD_H
#define	BLENDINGMETHOD_H

#include <SDL/SDL_opengl.h>
#include <string>

namespace spe
{

namespace gl
{

    enum BlendingMethodName
    {
        BLEND_UNKNOWN = -1,
        BLEND_ALPHA,
        BLEND_ADD,
        BLEND_LIGHT,
        BLEND_DARK,
        BLEND_FILTER,
        BLEND_NONE,
        BLEND_ALL
    };

    BlendingMethodName GetBlendingMethodName(const std::string& name);

    /**
     * This class represents a opengl blending method.
     */
    class BlendingMethod
    {
    public:

        BlendingMethod() {}

        virtual ~BlendingMethod()
        {
        }

        void Set(GLenum sfactor, GLenum dfactor);

        /**
         * Set the parameters of this blending method as the parameters of
         * glBlendFunc.
         */
        void Enable() const;

    private:
        GLenum sfactor;
        GLenum dfactor;
    };

};

};

#endif	/* BLENDINGMETHOD_H */

