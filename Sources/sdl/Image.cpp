/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Image.h"

#include <SDL/SDL_image.h>

#include "SDLException.h"

namespace spe
{

namespace sdl
{

    /*
     * Constructor opens an image from disk. Throws an exception if an error occurs.
     *
     * @param
     * path: image's location on disk.
     */

    Image::Image(const std::string& path)
    {
        //open image, if an error occurs..
        if(!Open(path))
        {
            //throw exception using the last error
            throw spe::sdl::SdlException();
        }
    }

    Image::~Image()
    {
    }

    /*
     * This routine opens an image from the disk and optimizes the buffer's format
     * for the current video display. If an error occurs during loading or optimization
     * the routine returns failure.
     *
     * @param
     * - path: image's location on disk.
     */

    bool Image::Open(const std::string& path)
    {
        //create a temporary primitive surface and load image
        SDL_Surface* temp = IMG_Load(path.c_str());

        //if loading didn't fail
        if(temp != nullptr)
        {
            //create an optimized primitive surface
            SDL_Surface* optimizedSurface = SDL_DisplayFormatAlpha(temp);
            //free temporary unoptimized primitive surface
            SDL_FreeSurface(temp);

            //if optimization didn't fail
            if(optimizedSurface != nullptr)
            {
                //set the object's buffer to the temporary surface
                SetBuffer(optimizedSurface);

                //return success
                return true;
            }
        }

        //return failure because an error occured
        return false;
    }

};

};
