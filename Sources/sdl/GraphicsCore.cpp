/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "GraphicsCore.h"

#include "../Logger.h"
#include "Canvas.h"
#include "GLColor.h"
#include "Texture.h"
#include "TextureCoords.h"
#include "TextImage.h"
#include "Vertex.h"
#include <SDL/SDL_opengl.h>
#include <list>
#include <algorithm>
#include "../Logger.h"
#include "BlendingMethodManager.h"

namespace spe
{

namespace sdl
{

    GraphicsCore::~GraphicsCore()
    {
        if(sdlScreen != nullptr)
            delete sdlScreen;
        //delete mainScreen;
    }

    bool GraphicsCore::Initialize(int screenW, int screenH, int bpp, bool fullscreen, bool useSdlScreen)
    {
        SetOpenglConstants();

        mainScreen = new Screen(screenW, screenH, bpp, fullscreen, true);

        if(useSdlScreen)
            sdlScreen = new Canvas(screenW, screenH);
        else
            sdlScreen = nullptr;

        //The color of sreen after glClear call.
        glClearColor(0, 0, 0, 0);

        glViewport(0, 0, screenW, screenH);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glOrtho(0, screenW, screenH, 0, 1, -1);

        glEnable(GL_TEXTURE_2D);

        blengingMethods.Initialize();

        CreateExtensionList();

        //intialize shader functionality [future]
        if(!InitializeGLSL())
        {
            LOGERROR << "GLSL could not be initialized!";
        }

        return true;
    }

    bool GraphicsCore::Render(const gl::Texture& texture, const Math::Rect& clip, const gl::Color& color,
                              const Math::Vector2F& position, const Math::Vector2F& scale, float angle,
                              bool flip[2], gl::BlendingMethodName blend /*= "Alpha"*/)
    {
        //revert view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        //load up the specified blending method.
        blengingMethods.EnableMethod(blend);

        //create texture coordinates
        gl::TextureCoords textcoords[4];
        TextureCoordinates(textcoords, texture, clip, flip);

        //create the vertex array
        gl::Vertex vertecies[4];
        Transorm(vertecies, position, clip, scale);

        if(angle > 0.01) glRotatef(angle, 0, 0, 1);

        glTranslated(position.GetX() - vertecies[0].X(), position.GetY() - vertecies[0].Y(), 0);

        glScalef(scale.GetX(), scale.GetY(), 0);

        color.Apply();
        texture.Apply();

        glBegin(GL_QUADS);
        for(int i = 0; i < 4; i++)
        {
            textcoords[i].Apply();
            vertecies[i].Apply();
        }
        glEnd();


        return true;
    }

    bool GraphicsCore::DrawLine(const Math::Vector2F& start, const Math::Vector2F& end, const gl::Color& color, float width)
    {
        //revert view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glBindTexture(GL_TEXTURE_2D, 0);
        glLineWidth(width);
        color.Apply();

        gl::Vertex vertecies[2];

        vertecies[0].Set(start.GetX(), start.GetY());
        vertecies[1].Set(end.GetX(), end.GetY());

        glBegin(GL_LINES);
        for(int i = 0; i < 2; i++)
        {
            vertecies[i].Apply();
        }
        glEnd();

        return true;
    }

    bool GraphicsCore::DrawFilledRect(const Math::Vector2F& start, const Math::Vector2F& end, const gl::Color& color)
    {
        //revert view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glBindTexture(GL_TEXTURE_2D, 0);
        color.Apply();

        gl::Vertex vertecies[4];

        vertecies[0].Set(start.GetX(), start.GetY());
        vertecies[2].Set(end.GetX(), end.GetY());
        vertecies[1].Set(end.GetX(), start.GetY());
        vertecies[3].Set(start.GetX(), end.GetY());

        glBegin(GL_QUADS);
        for(int i = 0; i < 4; i++)
        {
            vertecies[i].Apply();
        }
        glEnd();

        return true;
    }

    bool GraphicsCore::DrawLineRect(const Math::Vector2F& start, const Math::Vector2F& end, const gl::Color& color, float width)
    {
        //revert view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glBindTexture(GL_TEXTURE_2D, 0);
        color.Apply();

        gl::Vertex vertecies[4];
        glLineWidth(width);

        vertecies[0].Set(start.GetX(), start.GetY());
        vertecies[2].Set(end.GetX(), end.GetY());
        vertecies[1].Set(end.GetX(), start.GetY());
        vertecies[3].Set(start.GetX(), end.GetY());

        glBegin(GL_LINE_LOOP);
        for(int i = 0; i < 4; i++)
        {
            vertecies[i].Apply();
        }
        glEnd();

        return true;
    }

    /*void GraphicsCore::GetScreenshot()
    {
        int height = mainScreen->GetHeight();
        int width = mainScreen->GetWidth();

        int stride = width * 4; // length of a line in bytes
        GLubyte* swapline = ( GLubyte * ) malloc( stride );
        GLubyte* pixels = ( GLubyte * ) malloc( stride * height );

        glReadBuffer( GL_FRONT );
        glReadPixels( 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixels );

    //    for( int row = 0; row < height / 2; row++ )
    //    {
    //        memcpy( swapline, pixels + row * stride, stride );
    //        memcpy( pixels + row * stride, pixels + ( height - row - 1 ) * stride, stride );
    //        memcpy( pixels + ( height - row - 1 ) * stride, swapline, stride );
    //    }

        SDL_Surface* image = SDL_CreateRGBSurfaceFrom(pixels, width, height, 32, mainScreen->GetBuffer()->pitch, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000 );

        SDL_SaveBMP( image, "pic.bmp" );
        SDL_FreeSurface( image );
        free(swapline);
        free(pixels);
    }*/

    bool GraphicsCore::BeginRendering()
    {
        mainScreen->Clear();
        return true;
    }

    bool GraphicsCore::EndRendering()
    {
        sdlScreen->Render();
        mainScreen->Flip();
        sdlScreen->FillTransparent();
        glFlush();
        glFinish();
        return true;
    }

    void GraphicsCore::CreateExtensionList()
    {
        //get all available extensions and check if the specified exists
        char* ext = (char*)(glGetString(GL_EXTENSIONS));
        const char* item = strtok(ext, " ");
        while(item != nullptr)
        {
            extensionList.push_back(item);
            item = strtok(nullptr, " ");
        }
        extensionList.sort();
    }

    bool GraphicsCore::CheckForExtension(const char* name)
    {
        return std::binary_search(extensionList.begin(), extensionList.end(), name);
    }

    Canvas* GraphicsCore::GetSdlScreen() const
    {
        return sdlScreen;
    }

    Screen* GraphicsCore::GetGlScreen() const
    {
        return mainScreen;
    }

    void GraphicsCore::TextureCoordinates(gl::TextureCoords tCoords[4], const gl::Texture& texture, const Math::Rect& clip, bool flip[2]) const
    {
        float w = texture.GetWidth();
        float h = texture.GetHeight();

        float x1 = clip.GetLeft() / w;
        float x2 = clip.GetWidth() / w + x1;
        float y1 = clip.GetTop() / h;
        float y2 = clip.GetHeight() / h + y1;

        if(!flip[0])
        {
            tCoords[0].SetX(x1);
            tCoords[2].SetX(x2);
        }
        else
        {
            tCoords[0].SetX(x2);
            tCoords[2].SetX(x1);
        }

        if(!flip[1])
        {
            tCoords[0].SetY(y1);
            tCoords[2].SetY(y2);
        }
        else
        {
            tCoords[0].SetY(y2);
            tCoords[2].SetY(y1);
        }

        tCoords[1].Set(tCoords[2].X(), tCoords[0].Y());

        tCoords[3].Set(tCoords[0].X(), tCoords[2].Y());
    }

    void GraphicsCore::Transorm(gl::Vertex vertecies[4], const Math::Vector2F& position, const Math::Rect& clip, const Math::Vector2F& scale) const
    {
        //divide the two dimensions by two.(it is achieved by shifting one time to the left)
        float halfWidth = clip.GetWidth() >> 1;
        float halfHeight = clip.GetHeight() >> 1;
        vertecies[0].Set(-halfWidth, -halfHeight);
        vertecies[2].Set(halfWidth, halfHeight);
        vertecies[1].Set(vertecies[2].X(), vertecies[0].Y());
        vertecies[3].Set(vertecies[0].X(), vertecies[2].Y());
    }

    void GraphicsCore::SetOpenglConstants()
    {
        /*
         * Set up the attributes of video.
         */
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
        SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    }

};

};
