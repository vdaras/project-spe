/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "BlendingMethod.h"

namespace spe
{

namespace gl
{

    BlendingMethodName GetBlendingMethodName(const std::string& name)
    {
        if(name == "Alpha")
            return BLEND_ALPHA;
        else if(name == "Add")
            return BLEND_ADD;
        else if(name == "Light")
            return BLEND_LIGHT;
        else if(name == "Dark")
            return BLEND_DARK;
        else if(name == "None")
            return BLEND_NONE;
        else
            return BLEND_UNKNOWN;
    }

    void BlendingMethod::Set(GLenum sfactor, GLenum dfactor)
    {
        this->sfactor = sfactor;
        this->dfactor = dfactor;
    }

    void BlendingMethod::Enable() const
    {
        glBlendFunc(sfactor, dfactor);
    }

};

};
