/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef SURFACE_H
#define	SURFACE_H

#include "../NonCopyable.h"

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

namespace spe
{

namespace sdl
{

    class Color;

    /*
     * Class wrapping the SDL_Surface struct. Provides an interface for further
     * surfaces to be derived from (like the screen, images etc.) and some
     * functionality.
     */

    class Surface : public NonCopyable
    {
    private:
        SDL_Surface* m_buffer;

    public:
        Surface();
        virtual ~Surface() = 0;

        /**
         * Converts this surface into a opengl 2d texture.
         *
         * default for both filters is Linear
         *
         * @param minFilter - Filter to use in case of minimization.
         * @param magFilter - Filter to use in case of maximazation.
         * @return The ID of the texture or 0 to describe failure.
         */
        GLuint ConvertToTexture(GLint minFilter = GL_LINEAR,GLint magFilter = GL_LINEAR);

        /**
         * Checks if buffer is valid or nullptr.
         * @return
         */
        bool Valid() const;

        int GetWidth() const;
        int GetHeight() const;

        /**
         * Sets the specified pixel at the given color.
         * @param x
         * @param y
         * @param color
         */
        void SetPixel(int x, int y, const sdl::Color& color);

        /**
         * Fills the given area with the specified color.
         * @param x
         * @param y
         * @param w
         * @param h
         * @param color
         */
        void FillArea(int x, int y, int w, int h, const sdl::Color& color);

        SDL_Surface* GetBuffer() const;
        //SDL_Surface *getMutableBuffer();
    protected:
        void SetBuffer(SDL_Surface* buffer);
        void FreeBuffer();
    };

};
	
};

#endif	/* SURFACE_H */

