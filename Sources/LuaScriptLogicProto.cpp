/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "LuaScriptLogicProto.h"

namespace spe
{

namespace EntitySystem
{

    class AttributeVisitor : public boost::static_visitor<>
    {
    private:
        LuaScriptLogic* m_scriptLogic;
        std::string m_attributeName;

    public:
        AttributeVisitor(LuaScriptLogic* scriptLogic, const std::string& attributeName)
            :
            m_scriptLogic(scriptLogic),
            m_attributeName(attributeName)
        {
        }

        void operator()(int& value)
        {
            m_scriptLogic->PushNumber(m_attributeName, value);
        }

        void operator()(float& value)
        {
            m_scriptLogic->PushNumber(m_attributeName, value);
        }

        void operator()(bool& value)
        {
            m_scriptLogic->PushNumber(m_attributeName, value ? 1 : 0);
        }

        void operator()(std::string& value)
        {
            m_scriptLogic->PushString(m_attributeName, value);
        }

        void operator()(Math::Vector2F& value)
        {
            Math::Vector2F* copy = new Math::Vector2F(value);

            m_scriptLogic->PushObject(m_attributeName, copy, "Math::Vector2Templ< float >*", true);
        }
    };


    LuaScriptLogicProto::LuaScriptLogicProto(const std::string& scriptPath)
        :
        m_scriptPath(scriptPath)
    {
    }


    LuaScriptLogicProto::~LuaScriptLogicProto()
    {
    }


    /**
     * Returns a LuaScriptLogic object based on this prototype.
     */

    ScriptLogic* LuaScriptLogicProto::Clone()
    {
        LuaScriptLogic* aClone = new LuaScriptLogic(m_scriptPath);

        AttributeMap::iterator iter(m_attributes.begin()), end(m_attributes.end());

        for(; iter != end; ++iter)
        {
            AttributeVisitor visitor(aClone, iter->first);

            boost::apply_visitor(visitor, iter->second);
        }

        return aClone;
    }


    void LuaScriptLogicProto::SetParent(Entity* parent)
    {
        ScriptLogic::SetParent(parent);
    }

};

};