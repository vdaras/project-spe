/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "LuaScriptState.h"

//standard library includes

//other includes
#include "swigluarun.h"
#include "Exception.h"
#include "Logger.h"

extern "C"
{
	int luaopen_core(lua_State* L);
	int luaopen_gui(lua_State* L);
	int luaopen_input(lua_State* L);
	int luaopen_vect(lua_State* L);
	int luaopen_printing(lua_State* L);
	int luaopen_res(lua_State* L);
	int luaopen_sdl(lua_State* L);
	int luaopen_audio(lua_State* L);
}

namespace spe
{
	const char* LuaScriptState::METHOD_TABLE[] =
	{
		"Create",
		"Update",
		"OnKeyboardEvent",
		"OnMouseEvent",
		"Pause",
		"Resume",
		"Cleanup",
		"ActionPerformed",
		"OnEntityMessage"
	};

	LuaScriptState::LuaScriptState()
		:
		State(nullptr)
	{
	}

	LuaScriptState::LuaScriptState(EngineCore* engine, const std::string& script)
		:
		State(engine)
	{
		m_context = lua_open();

		int err;

		try
		{
			//load file and check for errors
			if ((err = luaL_loadfile(m_context, script.c_str())) != 0)
			{
				if (err == LUA_ERRSYNTAX)
				{
					LOG(Logger::CHANNEL_SCRIPTING, "Error") << "Syntax error in file: " << script << "\n";
				}
				else if (err == LUA_ERRMEM)
				{
					LOG(Logger::CHANNEL_MAIN, "Error") << "Cannot allocate memory for script: " << script << "\n";
				}
				else if (err == LUA_ERRFILE)
				{
					LOG(Logger::CHANNEL_LOADING, "Error") << "Lua script file missing: " << script << "\n";
				}
			}

			//open libraries for scripting
			luaL_openlibs(m_context);
			luaopen_core(m_context);
			luaopen_input(m_context);
			luaopen_vect(m_context);
			luaopen_printing(m_context);
			luaopen_audio(m_context);
			luaopen_res(m_context);
			luaopen_sdl(m_context);
			luaopen_gui(m_context);
		}
		catch (char const* exc)
		{
			throw spe::Exception(exc);
		}

		//call script to initialize script's global variables
		lua_pcall(m_context, 0, LUA_MULTRET, 0);

		SWIG_NewPointerObj(m_context, this, SWIG_TypeQuery(m_context, "spe::LuaScriptState*"), 0);

		lua_setglobal(m_context, "this");

		CheckMethodDefinitions();
	}

	LuaScriptState::~LuaScriptState()
	{
		lua_close(m_context);
	}

	/**
	 * Calls the Create function residing in the script.
	 */

	void LuaScriptState::Create()
	{
		static int methodIndex = 0;

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			if ((err = lua_pcall(m_context, 0, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Updates the State by calling the corresponding method in the lua script.
	 * The standard Update must occur first (Entity, Rendering and Physics managers).
	 *
	 * @param delta: time elapsed since last call.
	 */

	void LuaScriptState::Update(unsigned delta)
	{
		static int methodIndex = 1;

		State::Update(delta);

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			lua_pushnumber(m_context, delta);

			if ((err = lua_pcall(m_context, 1, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	void LuaScriptState::OnEntityMessage(EntitySystem::Entity* sender, const std::string& msg)
	{
		static int methodIndex = 8;

		//State::OnEntityMessage( sender, msg );

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			SWIG_NewPointerObj(m_context, sender, SWIG_TypeQuery(m_context, "spe::EntitySystem::Entity*"), 0);

			lua_pushstring(m_context, msg.c_str());

			if ((err = lua_pcall(m_context, 2, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Pushes keyboard events to the Entities. Then it forwards keyboard event to the script.
	 *
	 * @param event: a keyboard event occurred.
	 */

	void LuaScriptState::OnKeyboardEvent(KeyboardEvent& event)
	{
		static int methodIndex = 2;

		State::OnKeyboardEvent(event);

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			SWIG_NewPointerObj(m_context, &event, SWIG_TypeQuery(m_context, "spe::KeyboardEvent*"), 0);

			if ((err = lua_pcall(m_context, 1, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Calls regular State Update. Then forwards the mouse event to the script.
	 *
	 * @param event: a mouse event occurred.
	 */

	void LuaScriptState::OnMouseEvent(MouseEvent& event)
	{
		static int methodIndex = 3;

		//State::OnMouseEvent( event );

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			SWIG_NewPointerObj(m_context, &event, SWIG_TypeQuery(m_context, "spe::MouseEvent*"), 0);

			if ((err = lua_pcall(m_context, 1, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Calls the pause function implemented in script.
	 */

	void LuaScriptState::Pause()
	{
		static int methodIndex = 4;

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			if ((err = lua_pcall(m_context, 0, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Calls the resume function implemented in script.
	 */

	void LuaScriptState::Resume()
	{
		static int methodIndex = 5;

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			if ((err = lua_pcall(m_context, 0, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Calls the cleanup function implemented in script.
	 */

	void LuaScriptState::Cleanup()
	{
		static int methodIndex = 6;

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			if ((err = lua_pcall(m_context, 0, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Called when an action is performed on a gui component belonging to this State.
	 *
	 * @param sender: gui component that triggered the action.
	 */

	void LuaScriptState::ActionPerformed(gui::Component* sender)
	{
		static int methodIndex = 7;

		if (m_methodDefinitions[methodIndex])
		{
			int err = 0;

			lua_pushstring(m_context, METHOD_TABLE[methodIndex]);

			lua_gettable(m_context, LUA_GLOBALSINDEX);

			SWIG_NewPointerObj(m_context, sender, SWIG_TypeQuery(m_context, "spe::gui::Component*"), 0);

			if ((err = lua_pcall(m_context, 1, 0, 0)) != 0)
			{
				PrintExecError(methodIndex, err);
			}
		}
	}

	/**
	 * Checks which methods of State are implemented in the script.
	 */

	void LuaScriptState::CheckMethodDefinitions()
	{
		for (int i = 0; i < N_METHODS; ++i)
		{
			lua_getglobal(m_context, METHOD_TABLE[i]);

			m_methodDefinitions[i] = lua_isfunction(m_context, lua_gettop(m_context)) ? true : false;

			lua_setglobal(m_context, METHOD_TABLE[i]);
		}
	}

	/**7
	 * Prints an error occurred when executing a script function.
	 *
	 * @param methodIndex: index (in METHOD_TABLE) of the function that failed to execute.
	 * @param errorCode: error code returned by lua.
	 */

	void LuaScriptState::PrintExecError(int methodIndex, int errorCode)
	{
		LOG(Logger::CHANNEL_SCRIPTING, "Error") << "Could not invoke " << METHOD_TABLE[methodIndex] << " in a State script! Reason: ";

		const char* error = lua_tostring(m_context, lua_gettop(m_context));

		if (errorCode == LUA_ERRRUN)
		{
			LOGCHAINED(Logger::CHANNEL_SCRIPTING, "Error") << " Runtime Error: " << error << '\n';
		}
		else if (errorCode == LUA_ERRMEM)
		{
			LOGCHAINED(Logger::CHANNEL_SCRIPTING, "Error") << " Memory Error: " << error << '\n';
		}
		else
		{
			LOGCHAINED(Logger::CHANNEL_SCRIPTING, "Error") << " Error: " << error << '\n';
		}
	}
};