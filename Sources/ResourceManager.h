/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef RESOURCEMANAGER_H
#define	RESOURCEMANAGER_H

#include "ResourcePool.h"
#include "Singleton.h"

namespace spe
{

/**
 * A singleton class that handles all resources.
 * Each different resource type has its own pool.
 * For each of this pool a GetResource(const string& rname) method should be created.
 */
class ResourceManager
{
    SingletonClass(ResourceManager)
public:
    virtual ~ResourceManager();

    /**
     * Initializes all pools. Sets up default path etc.
     */
    void InitializePools();

    /// a pair of getting image resource.
    Resource<sdl::Image>* GetImage(const string& name);
    Resource<sdl::Image>* GetImage(const string& path, const string& name);

    /// a pair of getting image resource.
    Resource<gl::Texture>* GetTexture(const string& name);
    Resource<gl::Texture>* GetTexture(const string& path, const string& name);

    /// a pair of getting Sound resource.
    Resource<al::Sound>* GetSound(const string& name);
    Resource<al::Sound>* GetSound(const string& path, const string& name);

    /// a pair of getting font resource.
    Resource<sdl::Font>* GetFont(const string& name, int size);
    Resource<sdl::Font>* GetFont(const string& path, const string& name, int size);


    /**
     * Garbage collects all unused resources.
     * @param killEmAll if true important resources will be collected as well if they have no references.
     */

    void GarbageCollect(bool killEmAll = false);

private:

    /**
     * A pool to hold sdl::Images
     */
    ResourcePool<sdl::Image> imagePool;

    /**
     * A pool to hold sdl::Font
     */
    ResourcePool<sdl::Font> fontPool;

    /**
     * A pool to hold gl::Textures
     */
    ResourcePool<gl::Texture> texturePool;

    /**
     * A pool to hold Sounds
     * Because of the nature of sounds all sounds created are set to important.
     */
    ResourcePool<al::Sound> soundPool;
};

};

#endif	/* RESOURCEMANAGER_H */

