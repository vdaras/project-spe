/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef ANIMATIONFRAME_H
#define	ANIMATIONFRAME_H

#include "Rect.h"

namespace spe
{

/**
 * This struct represents a single animation from a spritesheet.
 */
struct AnimationFrame
{
    /**
     * A rect to describe the first frame of an animation sequence.
     * Note that each following frame is described as firstFrame.left + currentFrame*firstFrame.width
     */
    Math::Rect firstFrame;

    /**
     * The total time in milliseconds this animation sequence should overall take.
     */
    int totalTime;

    /**
     * The total number of frames this animation sequence has.
     */
    int numberOfFrames;

    /**
     */
    bool flip[2];
};

};

#endif	/* ANIMATIONFRAME_H */
