/**
 * Project SPE
 *
 * Copyright (C) 2012 Andreas Sfakianakis, Vasilis Daras
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _GLOBALS_H
#define	_GLOBALS_H

#include "Singleton.h"
#include <string>
#include <sstream>
#include <map>

namespace spe
{

/**
 *  This singleton class loads and stores all global values
 *  used through out in the entire engine
 *  via an external .ini file or a xml file
 *  the filename and/or path of this file must be hardcoded
 *  Prior to use, this class should be initialized
 *  To access one global simply get the instance via Globals::GetInstance
 *  and call the needed method
 */
class Globals
{

    SingletonClass(Globals)

public:

    ~Globals()
    {
    }

    void Initialization(bool flag, const char* filename);

    //the file name should be hardcoded
    //The following functions return
    //the global properties of the application
    unsigned long GetScreenWidth();
    unsigned long GetScreenHeight();
    bool GetFullscreen();
    void NormalizePath(std::string& path);
    void SetAppName(std::string appName);
    std::string GetAppName() const;
    void SetFullscreen(bool fullscreen);
    bool IsFullscreen() const;
    void SetScreenHeight(int screenHeight);
    void SetScreenWidth(int screenWidth);
    bool IsNewBuild() const;

    /**
     * Retrieves the value of the name path.
     * @param name
     * @return
     */
    std::string GetPath(const std::string& name);

    /**
     * Retrieves the path to the name file.
     * @param name
     * @return
     */
    std::string GetFile(const std::string& name);

protected:

    //loads a xml script
    bool LoadFromXML(const char* filename);

    std::string GenerateTimestamp()
    {
        std::ostringstream oss;
        oss << __DATE__ << "|" << __TIME__;
        return oss.str();
    }

private:

    /**
     * A map with all paths needed by the engine.
     */
    std::map<std::string, std::string> paths;

    /**
     * A map with all special files needed by the engine.
     */
    std::map<std::string, std::string> files;

    /**
     * application variables
     */
    int screenWidth, screenHeight;

    //the resolution of the game
    bool fullscreen;

    //if true the application runs on fullscreen mode
    bool newBuild;

    std::string appName;
};

};
#endif	/* _GLOBALS_H */

